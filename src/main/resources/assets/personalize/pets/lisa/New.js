base.setTexture(128, 64, "Foxtexture.png");
base.setPositionCorrection(0, 20, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(57, 0);
Shape2.addBox(0, 0, 0, 4, 4, 4);
Shape2.setRotationPoint(-3, -6.5, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(false);

var Shape3 = new Model(74, 1);
Shape3.addBox(0, 0, 0, 1, 1, 1);
Shape3.setRotationPoint(-3, -7.5, 0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(false);

var Shape4 = new Model(80, 1);
Shape4.addBox(0, 0, 0, 1, 1, 1);
Shape4.setRotationPoint(0, -7.5, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(false);

var Shape5 = new Model(61, 11);
Shape5.addBox(0, 0, 0, 2, 3, 2);
Shape5.setRotationPoint(-2, -2.9, 1);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(false);

var Shape6 = new Model(38, 12);
Shape6.addBox(0, 0, 0, 1, 2, 1);
Shape6.setRotationPoint(-3, -2.5, 2);
Shape6.setRotationDegrees(-94.2857142857143, 17.1428571428571, 0);
Shape6.setMirror(false);

var Shape6 = new Model(49, 12);
Shape6.addBox(0, 0, 0, 1, 2, 1);
Shape6.setRotationPoint(0, -2.5, 2);
Shape6.setRotationDegrees(-94.2857142857143, -17.1428571428571, 0);
Shape6.setMirror(false);

var Shape7 = new Model(60, 19);
Shape7.addBox(0, 0, 0, 1, 1, 1);
Shape7.setRotationPoint(-2, -0.5, 1.5);
Shape7.setRotationDegrees(-1.77635683940025E-15, 1.15463194561016E-14, 30);
Shape7.setMirror(false);

var Shape7 = new Model(66, 19);
Shape7.addBox(0, 0, 0, 1, 1, 1);
Shape7.setRotationPoint(-1, 0, 1.5);
Shape7.setRotationDegrees(-1.77635683940025E-15, 1.15463194561016E-14, -18.8799661876584);
Shape7.setMirror(false);

var Shape8 = new Model(87, 32);
Shape8.addBox(0, 0, 0, 1, 1, 2);
Shape8.setRotationPoint(-1.5, -1.5, 2.5);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(false);

var Shape9 = new Model(46, 3);
Shape9.addBox(0, 0, 0, 2, 1, 1);
Shape9.setRotationPoint(-2, -3.5, -0.5);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(false);

var Shape10 = new Model(97, 29);
Shape10.addBox(0, 0, 0, 1, 2, 1);
Shape10.setRotationPoint(-1.5, -2.5, 4);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(false);

var Shape11 = new Model(104, 24);
Shape11.addBox(0, 0, 0, 2, 2, 1);
Shape11.setRotationPoint(-2, -3.5, 5);
Shape11.setRotationDegrees(0, 0, 0);
Shape11.setMirror(false);

var Shape12 = new Model(111, 19);
Shape12.addBox(0, 0, 0, 1, 2, 1);
Shape12.setRotationPoint(-1.5, -4.5, 6);
Shape12.setRotationDegrees(0, 0, 0);
Shape12.setMirror(false);

var GL11 = org.lwjgl.opengl.GL11;
//func_71410_x() -> func_71410_x()


var interval = 60 * 20; // интервал оборота в тиках
var rotationStart = 0;
var rotationEnd = -interval;
var rotationStarted = false;

function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var diffX = 7 / 16; // Исходное расстояние от пета до центра игрока (константа, задано у меня в коде)
    var rotationSpeed = 5.0; // Скорость вращения
    var dist = -0.8; // Новое расстояние от центра игрока до пета (в блоках)

    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);

    if (time > rotationEnd + interval) {
        if (!rotationStarted) {
            rotationStart = time;
            rotationStarted = true;
        }

        var angle = (time - rotationStart) * rotationSpeed;  // Угол поворота
        if (angle < 360) {
            GL11.glTranslatef(diffX, 0, 0); // Ставим в центр игрока
            GL11.glRotatef(angle, 0, 1, 0); // Вращаем
            GL11.glTranslatef(dist, 0, 0); // Отодвигаем в сторону (после поворота)
            GL11.glRotatef(-angle, 0, 1, 0); // Поворачиваем его обратно (уже вокруг себя), что бы смотрел вперёд
        } else {
            GL11.glTranslatef(diffX + dist, 0, 0); // см выше
            rotationEnd = time;
            rotationStarted = false;
        }
    } else {
        GL11.glTranslatef(diffX + dist, 0, 0); // см выше
    }
}
