base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(0, 0);
Shape1.addBox(-5.5, -9.0, -4.0, 11, 18, 8);
Shape1.setRotationPoint(0, 0, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);
var Shape9 = new Model(38, 28);
Shape9.addBox(-5.5, -13.0, -1.0, 11, 2, 2);
Shape9.setRotationPoint(0, 0, 0);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);
var Shape2 = new Model(54, 0);
Shape2.addBox(-4.5, -9.0, -5.0, 4, 3, 1);
Shape2.setRotationPoint(0, 0, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);
var Shape8 = new Model(52, 7);
Shape8.addBox(-1.5, -12.0, -5.0, 3, 2, 3);
Shape8.setRotationPoint(0, 0, 0);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);
var Shape3 = new Model(54, 0);
Shape3.addBox(0.5, -9.0, -5.0, 4, 3, 1);
Shape3.setRotationPoint(0, 0, 0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);
var Shape7 = new Model(0, 28);
Shape7.addBox(-4.5, -10.0, -1.0, 9, 1, 3);
Shape7.setRotationPoint(0, 0, 0);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);
var Shape4 = new Model(0, 27);
Shape4.addBox(-5.5, -10.0, -5.0, 11, 1, 4);
Shape4.setRotationPoint(0, 0, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);
var Shape6 = new Model(0, 29);
Shape6.addBox(-5.5, -10.0, 2.0, 11, 1, 2);
Shape6.setRotationPoint(0, 0, 0);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);
var Shape5 = new Model(0, 26);
Shape5.addBox(-5.5, -11.0, -2.0, 11, 1, 5);
Shape5.setRotationPoint(0, 0, 0);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.005 : math.func_76126_a(time * 0.16) * 0.005; //(math.sin(time * 0.16)) * 0.005;
    var cos = deobf ? math.cos(time * 0.07) : math.func_76134_b(time * 0.07);//math.cos(time * 0.07);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, time + 0.0);
    GL11.glScalef(0.7, 0.7, 0.7);
}
