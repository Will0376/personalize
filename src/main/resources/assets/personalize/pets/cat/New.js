// Date: 06.01.2017 18:26:39
// Personalize model JS exported from Techne - Denr01 Edition

// !!!!!!!!!!!!!!!!!!!!!!!!!!!
// ВАЖНО! Нужно вызывать setScale ДО setPositionCorrection и setOffsetCorrection
// ИНАЧЕ ОБЪЕКТ СМЕСТИТСЯ!!!!!!!!!
base.setScale(0.5);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 20, 0);
base.setOffsetCorrection(0, 0, 0);

var head = new Model(32, 0);
head.addBox(-4, -10, -5, 8, 8, 8);
head.setRotationPoint(0, 3, 1);
head.setRotationDegrees(0, 0, 0);
head.setMirror(true);

var body = new Model(0, 17);
body.addBox(-2, 0, -1, 4, 4, 2);
body.setRotationPoint(0, 1, 0);
body.setRotationDegrees(0, 0, 0);
body.setMirror(true);

var leftHand = new Model(9, 24);
leftHand.addBox(0, -0.5, -1, 2, 4, 2);
leftHand.setRotationPoint(2, 2, 0);
leftHand.setRotationDegrees(0, 0, 0);
leftHand.setMirror(true);

var rightHand = new Model(0, 24);
rightHand.addBox(-2, -0.5, -1, 2, 4, 2);
rightHand.setRotationPoint(-2, 2, 0);
rightHand.setRotationDegrees(0, 0, 0);
rightHand.setMirror(true);

var leftLeg = new Model(28, 17);
leftLeg.addBox(-1, 0, -1, 2, 4, 2);
leftLeg.setRotationPoint(1, 5, 0);
leftLeg.setRotationDegrees(0, 0, 0);
leftLeg.setMirror(true);

var rightLeg = new Model(18, 17);
rightLeg.addBox(-1, 0, -1, 2, 4, 2);
rightLeg.setRotationPoint(-1, 5, 0);
rightLeg.setRotationDegrees(0, 0, 0);
rightLeg.setMirror(true);


/// АНИМАЦИЯ

var GL11 = org.lwjgl.opengl.GL11;
//func_71410_x() -> func_71410_x()


var interval = 60 * 20; // интервал оборота в тиках
var rotationStart = 0;
var rotationEnd = -interval;
var rotationStarted = false;

function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var diffX = 7 / 16; // Исходное расстояние от пета до центра игрока (константа, задано у меня в коде)
    var rotationSpeed = 5.0; // Скорость вращения
    var dist = -0.8; // Новое расстояние от центра игрока до пета (в блоках)

    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);

    if (time > rotationEnd + interval) {
        if (!rotationStarted) {
            rotationStart = time;
            rotationStarted = true;
        }

        var angle = (time - rotationStart) * rotationSpeed;  // Угол поворота
        if (angle < 360) {
            GL11.glTranslatef(diffX, 0, 0); // Ставим в центр игрока
            GL11.glRotatef(angle, 0, 1, 0); // Вращаем
            GL11.glTranslatef(dist, 0, 0); // Отодвигаем в сторону (после поворота)
            GL11.glRotatef(-angle, 0, 1, 0); // Поворачиваем его обратно (уже вокруг себя), что бы смотрел вперёд
        } else {
            GL11.glTranslatef(diffX + dist, 0, 0); // см выше
            rotationEnd = time;
            rotationStarted = false;
        }
    } else {
        GL11.glTranslatef(diffX + dist, 0, 0); // см выше
    }

    // Получаем поворот головы игрока 
    // player -> player
    // field_70759_as -> field_70759_as
    // field_70761_aq -> field_70761_aq
    // field_70125_A -> field_70125_A
    var playerHeadYaw = deobf ? mc.player.rotationYawHead - mc.player.renderYawOffset : mc.field_71439_g.field_70759_as - mc.field_71439_g.field_70761_aq;
    var playerHeadPitch = deobf ? mc.player.rotationPitch : mc.field_71439_g.field_70125_A;
    // Поворачиваем голову модельки
    head.setRotationDegrees(playerHeadPitch, playerHeadYaw, 0);

    var maxLegAngle = 45; // Максимальный угол отклонения ноги
    var rightAngle = 0;
    var leftAngle = 0;
    if (speed > 0) { // Если игрок идёт
        leftAngle = deobf ? math.sin(time) * maxLegAngle : math.func_76126_a(time) * maxLegAngle;//Math.sin(time) * maxLegAngle;
        rightAngle = deobf ? math.sin(time + Math.PI) * maxLegAngle : math.func_76126_a(time + Math.PI) * maxLegAngle;//Math.sin(time + Math.PI) * maxLegAngle;
    }

    // Поворачиваем ноги
    leftLeg.setRotationDegrees(leftAngle, 0, 0);
    rightLeg.setRotationDegrees(rightAngle, 0, 0);
}
