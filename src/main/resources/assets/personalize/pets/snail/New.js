base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(16, 21);
Shape1.addBox(0.0, 0.0, 0.0, 4, 3, 8);
Shape1.setRotationPoint(-2.0, 0.0, -3.0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);
var Shape2 = new Model(0, 27);
Shape2.addBox(0.0, 0.0, 0.0, 3, 2, 3);
Shape2.setRotationPoint(-1.5, 1.0, -5.0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);
var Shape3 = new Model(54, 28);
Shape3.addBox(-1.0, -0.5, 0.0, 2, 1, 3);
Shape3.setRotationPoint(0.0, 2.5, 7.0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);
var Shape4 = new Model(0, 11);
Shape4.addBox(-0.5, -5.0, -0.5, 1, 5, 1);
Shape4.setRotationPoint(0.7, 1.0, -4.0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);
var Shape5 = new Model(24, 0);
Shape5.addBox(0.0, 0.0, 0.0, 5, 9, 8);
Shape5.setRotationPoint(-2.5, -7.3, -1.0);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);
var Shape6 = new Model(40, 26);
Shape6.addBox(-1.5, 0.0, 0.0, 3, 2, 4);
Shape6.setRotationPoint(0.0, 1.0, 4.0);
Shape3.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);
var Shape7 = new Model(0, 11);
Shape6.addBox(-0.5, -5.0, -0.5, 1, 5, 1);
Shape6.setRotationPoint(-0.7, 1.0, -4.0);
Shape3.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
}
