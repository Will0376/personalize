base.setTexture(64, 32, "Hattexture.png");
base.setPositionCorrection(0, 5, 0);
base.setOffsetCorrection(0, 0, 0);

var slime = new Model(0, 0);
slime.addBox(-4.0, -4.0, -4.0, 8, 8, 8);
var small = new Model(0, 16);
small.addBox(-3.0, -3.0, -3.0, 6, 6, 6);
var mouth = new Model(32, 8);
mouth.addBox(0.0, 1.0, -3.4, 1, 1, 1);
var leftEye = new Model(32, 0);
leftEye.addBox(1.4, -2.0, -3.4, 2, 2, 2);
var rightEye = new Model(32, 0);
rightEye.addBox(-3.4, -2.0, -3.4, 2, 2, 2);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = MathHelper.sin(time * 0.16) * 0.02;
    var scale = MathHelper.sin(time * 0.2) * 0.05 + 1.5;
    GL11.glTranslatef(0.6, -0.5 + sin, 0.4);
    GL11.glScalef(scale, scale + sin * 3.0, scale);
}
