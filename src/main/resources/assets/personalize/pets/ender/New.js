base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(32, 2);
Shape1.addBox(0.0, 0.0, 0.0, 3, 4, 1);
Shape1.setRotationPoint(0, 0, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);
var Shape2 = new Model(32, 0);
Shape2.addBox(-0.5, 0.0, -0.5, 1, 6, 1);
Shape2.setRotationPoint(2.5, 4.0, 0.5);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);
var Shape3 = new Model(32, 0);
Shape3.addBox(-0.5, 0.0, -0.5, 1, 6, 1);
Shape3.setRotationPoint(0.5, 4.0, 0.5);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);
var Shape4 = new Model(0, 0);
Shape4.addBox(-4.0, -8.0, -4.0, 8, 8, 8);
Shape4.setRotationPoint(1.5, 0.0, 0.5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);
var Shape5 = new Model(48, 0);
Shape5.addBox(0.0, 0.0, 0.0, 3, 3, 3);
Shape5.setRotationPoint(0.0, 2.0, -5.0);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);
var Shape6 = new Model(42, 0);
Shape6.addBox(0.0, 0.0, 0.0, 1, 5, 1);
Shape6.setRotationPoint(-1.0, 0.0, 0.3);
Shape6.setRotationDegrees(-0.8726646, 0.0, 0.0);
Shape6.setMirror(true);
var Shape7 = new Model(42, 0);
Shape7.addBox(0.0, 0.0, 0.0, 1, 5, 1);
Shape7.setRotationPoint(3.0, 0.0, 0.3);
Shape7.setRotationDegrees(-0.8726646, 0.0, 0.0);
Shape7.setMirror(true);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02; //math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
}
