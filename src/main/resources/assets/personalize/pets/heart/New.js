base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 25, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(23, 0);
Shape1.addBox(-0.5, -17.0, -0.5, 1, 8, 1);
Shape1.setRotationPoint(0.0, 0, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(false);
var Shape2 = new Model(18, 0);
Shape2.addBox(-1.5, -18.0, -0.5, 1, 8, 1);
Shape2.setRotationPoint(0.0, 0, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(false);
var Shape3 = new Model(13, 0);
Shape3.addBox(-2.5, -18.0, -0.5, 1, 7, 1);
Shape3.setRotationPoint(0.0, 0, 0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(false);
var Shape4 = new Model(8, 0);
Shape4.addBox(-3.5, -17.0, -0.5, 1, 5, 1);
Shape4.setRotationPoint(0.0, 0, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(false);
var Shape5 = new Model(3, 0);
Shape5.addBox(-4.5, -16.0, -0.5, 1, 3, 1);
Shape5.setRotationPoint(0.0, 0, 0);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(false);
var Shape6 = new Model(28, 0);
Shape6.addBox(0.5, -18.0, -0.5, 1, 8, 1);
Shape6.setRotationPoint(0.0, 0, 0);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(false);
var Shape7 = new Model(33, 0);
Shape7.addBox(1.5, -18.0, -0.5, 1, 7, 1);
Shape7.setRotationPoint(0.0, 0, 0);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(false);
var Shape8 = new Model(38, 0);
Shape8.addBox(2.5, -17.0, -0.5, 1, 5, 1);
Shape8.setRotationPoint(0.0, 0, 0);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(false);
var Shape9 = new Model(43, 0);
Shape9.addBox(3.5, -16.0, -0.5, 1, 3, 1);
Shape9.setRotationPoint(0.0, 0, 0);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(false);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02; //math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
}
