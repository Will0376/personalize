base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 10, 0);
base.setOffsetCorrection(0, 0, 0);

var body = new Model(0, 0);
body.addBox(0.0, 0.0, 0.0, 9, 13, 7);
body.setRotationPoint(-4.5, 0.0, -4.0);
body.setRotationDegrees(0, 0, 0);
body.setMirror(false);
var leftHand = new Model(0, 0);
leftHand.addBox(0.0, -0.5, -0.5, 1, 5, 1);
leftHand.setRotationPoint(4.5, 9.5, -0.5);
leftHand.setRotationDegrees(0, 0, 0);
leftHand.setMirror(false);
var rightHand = new Model(0, 0);
rightHand.addBox(-1.0, -0.5, -0.5, 1, 5, 1);
rightHand.setRotationPoint(-4.5, 9.5, -0.5);
rightHand.setRotationDegrees(0, 0, 0);
rightHand.setMirror(false);
var rightLeg = new Model(0, 0);
rightLeg.addBox(-0.5, 0.0, -0.5, 1, 3, 1);
rightLeg.setRotationPoint(-2.0, 13.0, -0.5);
rightLeg.setRotationDegrees(0, 0, 0);
rightLeg.setMirror(false);
var leftLeg = new Model(0, 0);
leftLeg.addBox(-0.5, 0.0, -0.5, 1, 3, 1);
leftLeg.setRotationPoint(2.0, 13.0, -0.5);
leftLeg.setRotationDegrees(0, 0, 0);
leftLeg.setMirror(false);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;

    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var sin2 = deobf ? math.sin(time * 0.26) * 0.05 : math.func_76126_a(time * 0.26) * 0.05;//math.sin(time * 0.26) * 0.05;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    var handRotation = 0.0;
    if (time % 70.0 > 50.0) {
        handRotation = deobf ? math.cos(time % 70.0 * 0.15 + 0.4) * 2.0 : math.func_76134_b(time % 70.0 * 0.15 + 0.4) * 2.0;//math.cos(time % 70.0 * 0.15 + 0.4) * 2.0;
    }
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);

    this.rightLeg.setRotationRadians(sin2 * 3.0, 0, 0);
    this.rightLeg.setRotationRadians(0, sin * 6.0, 0);
    this.leftLeg.setRotationRadians(-sin2 * 3.0, 0, 0);
    this.leftLeg.setRotationRadians(0, 0, sin * 5.0);
    this.leftHand.setRotationRadians(cos * 0.1, 0, 0);
    this.leftHand.setRotationRadians(0, -cos * sin * 2.0, 0);
    this.leftHand.setRotationRadians(0, 0, cos * sin * 4.0 + handRotation);
    this.rightHand.setRotationRadians(-this.leftHand.rotationX, 0, 0);
    this.rightHand.setRotationRadians(0, -this.leftHand.rotationY, 0);
    this.rightHand.setRotationRadians(0, 0, cos * sin * 4.0 - handRotation);
}
