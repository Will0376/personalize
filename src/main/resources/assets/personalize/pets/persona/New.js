base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 15, 0);
base.setOffsetCorrection(0, 0, 0);

var body = new Model(0, 0);
body.addBox(0.0, 0.0, 0.0, 8, 8, 5);
var Shape1 = new Model(0, 0);
Shape1.addBox(0.0, 0.0, 0.0, 1, 5, 1);
var Shape2 = new Model(0, 0);
Shape2.addBox(0.0, 0.0, 0.0, 1, 5, 1);
var Shape3 = new Model(0, 0);
Shape3.addBox(0.0, 0.0, 0.0, 1, 2, 1);
var Shape4 = new Model(0, 0);
Shape4.addBox(0.0, 0.0, 0.0, 1, 5, 1);
var Shape5 = new Model(0, 0);
Shape5.addBox(0.0, 0.0, 0.0, 1, 3, 1);
var Shape6 = new Model(0, 0);
Shape6.addBox(0.0, 0.0, 0.0, 1, 3, 1);
var Shape7 = new Model(0, 0);
Shape7.addBox(0.0, 0.0, 0.0, 1, 3, 1);
var Shape8 = new Model(0, 0);
Shape8.addBox(0.0, 0.0, 0.0, 1, 3, 1);
var Shape9 = new Model(0, 0);
Shape9.addBox(0.0, 0.0, 0.0, 1, 3, 1);
var Shape10 = new Model(0, 0);
Shape10.addBox(0.0, 0.0, 0.0, 1, 3, 1);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.6, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
}
