base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 13, 0);
base.setOffsetCorrection(0, 0, 0);

var head = new Model(0, 0);
head.addBox(-4.0, -8.0, -4.0, 8, 8, 8);
head.setRotationPoint(0.0, 0.0, 1.0);
head.setRotationDegrees(0.0, 0.0, 0.0);
var body = new Model(32, 0);
body.addBox(0.0, 0.0, 0.0, 4, 4, 2);
body.setRotationPoint(-2.0, 0.0, 0.0);
body.setRotationDegrees(0.0, 0.0, 0.0);
var rightLegF = new Model(0, 16);
rightLegF.addBox(-1.0, 0.0, -2.0, 2, 2, 2);
rightLegF.setRotationPoint(-1.0, 4.0, 0.0);
rightLegF.setRotationDegrees(0.0, 0.0, 0.0);
var rightLegB = new Model(0, 20);
rightLegB.addBox(-1.0, 0.0, -1.0, 2, 2, 2);
rightLegB.setRotationPoint(-1.0, 4.0, 2.0);
rightLegB.setRotationDegrees(0.0, 0.0, 0.0);
var leftLegF = new Model(0, 24);
leftLegF.addBox(-1.0, 0.0, -1.0, 2, 2, 2);
leftLegF.setRotationPoint(1.0, 4.0, 0.0);
leftLegF.setRotationDegrees(0.0, 0.0, 0.0);
var leftLegB = new Model(0, 28);
leftLegB.addBox(-1.0, 0.0, 0.0, 2, 2, 2);
leftLegB.setRotationPoint(1.0, 4.0, 2.0);
leftLegB.setRotationDegrees(0.0, 0.0, 0.0);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
//  this.head.rotationX = modelPlayer.field_78116_c.field_78795_f / 2.0;
//  this.head.rotationY = modelPlayer.field_78116_c.field_78796_g + 0.2 - cos * 0.1;
//  this.head.rotationZ = modelPlayer.field_78116_c.field_78808_h;
//  this.leftLegF.rotationX = modelPlayer.field_78124_i.field_78795_f + sin2 / 3.0;
//  this.leftLegB.rotationX = modelPlayer.field_78123_h.field_78795_f + sin3 / 3.0;
//  this.rightLegB.rotationX = modelPlayer.field_78124_i.field_78795_f + sin2 / 3.0;
//  this.rightLegF.rotationX = modelPlayer.field_78123_h.field_78795_f + sin3 / 3.0;
}
