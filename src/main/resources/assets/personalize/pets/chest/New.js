base.setScale(0.4);
base.setTexture(128, 64, "Newtexture.png");
base.setPositionCorrection(0, 13, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(0, 19);
Shape1.addBox(-7.0, 0.0, -7.0, 14, 10, 14);
Shape1.setRotationPoint(0, 0, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);
var Shape2 = new Model(70, 41);
Shape2.addBox(-6.5, -2.0, -6.5, 13, 2, 0);
Shape2.setRotationPoint(0, 0, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);
var Shape3 = new Model(75, 31);
Shape3.addBox(6.5, -2.0, -6.5, 0, 2, 8);
Shape3.setRotationPoint(0, 0, 0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);
var Shape4 = new Model(65, 33);
Shape4.addBox(-6.5, -2.0, -6.5, 0, 2, 6);
Shape4.setRotationPoint(0, 0, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);
var Shape5 = new Model(0, 0);
Shape5.addBox(-7.0, -5.0, -14.0, 14, 5, 14);
Shape5.setRotationPoint(0, 0, 7.0);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);
var Shape6 = new Model(14, 57);
Shape6.addBox(-6.5, 0.0, -13.5, 13, 2, 0);
Shape6.setRotationPoint(0, 0, 7.0);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);
var Shape7 = new Model(17, 45);
Shape7.addBox(6.5, 0.0, -13.5, 0, 2, 10);
Shape7.setRotationPoint(0, 0, 7.0);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);
var Shape8 = new Model(7, 48);
Shape8.addBox(-6.5, 0.0, -13.5, 0, 2, 7);
Shape8.setRotationPoint(0, 0, 7.0);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);
var Shape9 = new Model(0, 0);
Shape9.addBox(-1.0, -2.0, -15.0, 2, 4, 1);
Shape9.setRotationPoint(0, 0, 7.0);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(1.5, 0.0, 1.0, 0.0);
    var handRotation = 0.0;
    if (time % 70.0 > 50.0) {
        handRotation = deobf ? math.cos(time % 70.0 * 0.267) * 0.5 - 0.5 : math.func_76134_b(time % 70.0 * 0.267) * 0.5 - 0.5;//math.cos(time % 70.0 * 0.267) * 0.5 - 0.5;
    }
    this.Shape5.setRotationRadians(handRotation, 0, 0);
    this.Shape6.setRotationRadians(handRotation, 0, 0);
    this.Shape7.setRotationRadians(handRotation, 0, 0);
    this.Shape8.setRotationRadians(handRotation, 0, 0);
    this.Shape9.setRotationRadians(handRotation, 0, 0);
}
