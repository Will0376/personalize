base.setScale(0.5);
base.setTexture(128, 64, "Newtexture.png");
base.setPositionCorrection(0, 13, 0);
base.setOffsetCorrection(0, 0, 0);

var Bill0 = new Model(0, 0);
Bill0.addBox(0.0, 0.0, 0.0, 8, 10, 8);
Bill0.setRotationPoint(-4.0, 0.0, -4.0);
Bill0.setMirror(true);
var Bill1 = new Model(0, 28);
Bill1.addBox(0.0, 0.0, 0.0, 10, 2, 10);
Bill1.setRotationPoint(-5.0, 10.0, -5.0);
Bill1.setMirror(true);
var Bill2 = new Model(0, 40);
Bill2.addBox(0.0, 0.0, 0.0, 10, 2, 10);
Bill2.setRotationPoint(-5.0, -2.0, -5.0);
Bill2.setMirror(true);
var Bill3 = new Model(0, 18);
Bill3.addBox(0.0, 0.0, 0.0, 8, 2, 8);
Bill3.setRotationPoint(-4.0, -3.0, -4.0);
Bill3.setMirror(true);
var Bill4 = new Model(0, 0);
Bill4.addBox(0.0, 0.0, 0.0, 2, 2, 2);
Bill4.setRotationPoint(-1.0, -5.0, -1.0);
Bill4.setMirror(true);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02; // sin -> sin
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(time * 1.5, 0.0, 1.0, 0.0);
}
