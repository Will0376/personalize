base.setScale(0.6);
base.setTexture(64, 32, "bookgf1.png");
base.setPositionCorrection(0, 20, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(2, 2);
Shape1.addBox(-4.0, -1.5, -5.0, 8, 1, 8);
Shape1.setRotationPoint(0.0, 0.0, 0.0);
Shape1.setRotationDegrees(0.0, 0.0, 0.0);
var Shape2 = new Model(0, 11);
Shape2.addBox(-4.0, 0.5, -5.0, 8, 1, 10);
Shape2.setRotationPoint(0.0, 0.0, 0.0);
Shape2.setRotationDegrees(0.0, 0.0, 0.0);
var Shape3 = new Model(0, 22);
Shape3.addBox(-3.5, -0.5, -4.5, 7, 1, 9);
Shape3.setRotationPoint(0.0, 0.0, 0.0);
Shape3.setRotationDegrees(0.0, 0.0, 0.0);
var Shape4 = new Model(39, 17);
Shape4.addBox(-4.5, -1.0, -5.0, 1, 2, 10);
Shape4.setRotationPoint(0.0, 0.0, 0.0);
Shape4.setRotationDegrees(0.0, 0.0, 0.0);
var Shape5 = new Model(50, 7);
Shape5.addBox(-4.6, -1.1, -4.0, 1, 1, 1);
Shape5.setRotationPoint(0.0, 0.0, 0.0);
Shape5.setRotationDegrees(0.0, 0.0, 0.0);
var Shape6 = new Model(50, 7);
Shape6.addBox(-4.6, 0.1, -4.0, 1, 1, 1);
Shape6.setRotationPoint(0.0, 0.0, 0.0);
Shape6.setRotationDegrees(0.0, 0.0, 0.0);
var Shape7 = new Model(50, 6);
Shape7.addBox(-4.7, -1.0, -4.0, 1, 2, 1);
Shape7.setRotationPoint(0.0, 0.0, 0.0);
Shape7.setRotationDegrees(0.0, 0.0, 0.0);
var Shape8 = new Model(50, 7);
Shape8.addBox(-4.6, -1.1, 3.0, 1, 1, 1);
Shape8.setRotationPoint(0.0, 0.0, 0.0);
Shape8.setRotationDegrees(0.0, 0.0, 0.0);
var Shape9 = new Model(50, 7);
Shape9.addBox(-4.6, 0.1, 3.0, 1, 1, 1);
Shape9.setRotationPoint(0.0, 0.0, 0.0);
Shape9.setRotationDegrees(0.0, 0.0, 0.0);
var Shape10 = new Model(50, 6);
Shape10.addBox(-4.7, -1.0, 3.0, 1, 2, 1);
Shape10.setRotationPoint(0.0, 0.0, 0.0);
Shape10.setRotationDegrees(0.0, 0.0, 0.0);
var Shape11 = new Model(8, 0);
Shape11.addBox(-4.0, -1.5, 3.0, 6, 1, 2);
Shape11.setRotationPoint(0.0, 0.0, 0.0);
Shape11.setRotationDegrees(0.0, 0.0, 0.0);
var Shape12 = new Model(41, 7);
Shape12.addBox(-1.7, -1.4, 3.5, 2, 1, 1);
Shape12.setRotationPoint(0.0, 0.0, 0.0);
Shape12.setRotationDegrees(0.0, 0.7853982, 0.0);
var Shape13 = new Model(11, 27);
Shape13.addBox(-1.2, 0.0, -0.2, 3, 0, 2);
Shape13.setRotationPoint(0.0, 0.0, 0.0);
Shape13.setRotationDegrees(0.0, 0.9599311, 0.0);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;

    var sin = deobf ? math.sin(time * 0.1) * 0.04 : math.func_76126_a(time * 0.1) * 0.04;//math.sin(time * 0.1) * 0.04;
    var anim = time / 1.5;
    var anim2 = -time / 1.5;
    GL11.glTranslatef(0.6 + sin, -0.5 + sin, 0.5 + sin);
    GL11.glRotatef(anim * 4.0, 0.0, 0.6, time + 0.0);
    GL11.glRotatef(90.0 + anim, 1.0, 0.0, 0.0);
    GL11.glRotatef(90.0 + anim2, 0.0, 1.0, 0.0);
    GL11.glRotatef(90.0 + anim, 0.0, 0.0, 1.0);
    //GL11.glScalef(1.2, 1.2, 1.2);
}
