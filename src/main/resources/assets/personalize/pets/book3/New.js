base.setScale(0.6);
base.setTexture(64, 32, "bookgf3.png");
base.setPositionCorrection(0, 20, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(0, 0);
Shape1.addBox(-4.0, -1.5, -5.0, 8, 1, 10);
var Shape2 = new Model(0, 11);
Shape2.addBox(-4.0, 0.5, -5.0, 8, 1, 10);
var Shape3 = new Model(0, 22);
Shape3.addBox(-3.5, -0.5, -4.5, 7, 1, 9);
var Shape4 = new Model(39, 17);
Shape4.addBox(-4.5, -1.033333, -5.0, 1, 2, 10);
var Shape5 = new Model(50, 7);
Shape5.addBox(-4.6, -1.1, -4.0, 1, 1, 1);
var Shape6 = new Model(50, 7);
Shape6.addBox(-4.6, 0.1, -4.0, 1, 1, 1);
var Shape7 = new Model(50, 6);
Shape7.addBox(-4.7, -1.0, -4.0, 1, 2, 1);
var Shape8 = new Model(50, 7);
Shape8.addBox(-4.6, -1.1, 3.0, 1, 1, 1);
var Shape9 = new Model(50, 7);
Shape9.addBox(-4.6, 0.1, 3.0, 1, 1, 1);
var Shape10 = new Model(50, 6);
Shape10.addBox(-4.733333, -1.033333, 3.0, 1, 2, 1);
var Shape11 = new Model(16, 24);
Shape11.addBox(2.0, 1.5, 0.0, 2, 3, 0);
var Shape12 = new Model(16, 24);
Shape12.addBox(2.0, -2.5, 0.0, 2, 3, 0);
var Shape13 = new Model(16, 24);
Shape13.addBox(0.8, -3.2, 0.0, 2, 3, 0);
var Shape14 = new Model(16, 24);
Shape14.addBox(-0.8, -5.4, 0.0, 3, 3, 0);
var Shape15 = new Model(16, 24);
Shape15.addBox(-0.2, -4.4, 0.0, 3, 3, 0);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.1) * 0.04 : math.func_76126_a(time * 0.1) * 0.04;//math.sin(time * 0.1) * 0.04;
    var anim = time / 1.5;
    var anim2 = -time / 1.5;
    GL11.glTranslatef(0.6 + sin, -0.5 + sin, 0.5 + sin);
    GL11.glRotatef(anim * 4.0, 0.0, 0.6, time + 0.0);
    GL11.glRotatef(90.0 + anim, 1.0, 0.0, 0.0);
    GL11.glRotatef(90.0 + anim2, 0.0, 1.0, 0.0);
    GL11.glRotatef(90.0 + anim, 0.0, 0.0, 1.0);
    //GL11.glScalef(1.2, 1.2, 1.2);
}
