base.setScale(0.5);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 15, 0);
base.setOffsetCorrection(0, 0, 0);

var body1 = new Model(0, 0);
body1.addBox(-5.0, -6.0, -2.005, 10, 12, 6);
var bodyeye = new Model(0, 16);
bodyeye.addBox(-4.5, -4.5, -2.5, 9, 9, 2);
var body2 = new Model(0, 0);
body2.addBox(-6.0, -5.0, -2.0, 12, 10, 6);
var body3 = new Model(0, 0);
body3.addBox(-4.5, -4.5, 2.5, 9, 9, 2);
var handr2 = new Model(0, 0);
handr2.addBox(-5.4, 1.533333, 0.0, 2, 6, 2);
var handr1 = new Model(0, 0);
handr1.addBox(-3.733333, 4.466667, 0.005, 2, 4, 2);
var rleg1 = new Model(0, 0);
rleg1.addBox(-0.4, 5.4, 0.005, 2, 3, 2);
var rleg2 = new Model(0, 0);
rleg2.addBox(-4.266667, 5.266667, 0.0, 2, 3, 2);
var lleg1 = new Model(0, 0);
lleg1.addBox(-0.6666667, 5.133333, 0.005, 2, 3, 2);
var lleg2 = new Model(0, 0);
lleg2.addBox(3.4, 4.4, 0.0, 2, 3, 2);
var handl1 = new Model(0, 0);
handl1.addBox(3.466667, 2.25, 0.005, 2, 6, 2);
var handl2 = new Model(0, 0);
handl2.addBox(0.9666666, 4.866667, 0.0, 2, 4, 2);
var earl1 = new Model(0, 1);
earl1.addBox(-0.9666666, -10.53333, 0.0, 2, 4, 2);
var earl2 = new Model(0, 0);
earl2.addBox(0.3333333, -10.53333, 0.005, 1, 5, 2);
var earr1 = new Model(0, 0);
earr1.addBox(-1.933333, -10.26667, 0.0, 2, 4, 2);
var earr2 = new Model(2, 0);
earr2.addBox(-3.0, -9.866667, 0.005, 1, 4, 2);
var bloonrope1 = new Model(32, 0);
bloonrope1.addBox(7.666667, -3.266667, 0.5, 1, 5, 1);
var bloonrope2 = new Model(32, 0);
bloonrope2.addBox(8.0, -9.866667, 0.5, 1, 3, 1);
var bloonrope3 = new Model(32, 0);
bloonrope3.addBox(8.466666, -6.4, 0.5, 1, 3, 1);
var bloonrope4 = new Model(32, 0);
bloonrope4.addBox(8.2, -4.266667, 0.5, 1, 3, 1);
var bloonrope5 = new Model(32, 0);
bloonrope5.addBox(6.7, 4.066667, 0.5, 1, 5, 1);
var doot1 = new Model(0, 27);
doot1.addBox(1.466667, 1.6, -6.0, 1, 1, 4);
var doot2 = new Model(6, 27);
doot2.addBox(1.0, 3.933333, -6.333333, 2, 1, 2);
var doot3 = new Model(14, 27);
doot3.addBox(1.005, 6.933333, -1.666667, 2, 1, 1);
var hat1 = new Model(33, 26);
hat1.addBox(-2.0, -7.5, -1.0, 4, 2, 4);
var hat2 = new Model(21, 27);
hat2.addBox(-1.5, -9.0, -0.5, 3, 2, 3);
var hat3 = new Model(24, 23);
hat3.addBox(-1.0, -10.5, 0.0, 2, 2, 2);
var hat4 = new Model(33, 26);
hat4.addBox(-0.5, -11.5, 0.5, 1, 1, 1);
var hattop = new Model(26, 21);
hattop.addBox(-5.0, -12.5, 0.0, 2, 2, 0);
bloon.setRotationPoint(9.0, -14.0, 1.0);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.6, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
}
