base.setScale(0.4);
// Date: 05.01.2017 7:38:52
// Personalize model JS exported from Techne - Denr01 Edition
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 12, 0);
base.setOffsetCorrection(0, 0, 0);

var head = new Model(0, 0);
head.addBox(-4, -4, -8, 8, 8, 8);
head.setRotationPoint(0, 12, -6);
head.setRotationDegrees(0, 0, 0);
head.setMirror(true);
var body = new Model(28, 8);
body.addBox(-5, -10, -7, 10, 16, 8);
body.setRotationPoint(0, 11, 3);
body.setRotationDegrees(90, 0, 0);
head.setMirror(true);
var leg1 = new Model(0, 16);
leg1.addBox(-2, 0, -2, 4, 6, 4);
leg1.setRotationPoint(-3, 18, 7);
leg1.setRotationDegrees(0, 0, 0);
head.setMirror(true);
var leg2 = new Model(0, 16);
leg2.addBox(-2, 0, -2, 4, 6, 4);
leg2.setRotationPoint(3, 18, 7);
leg2.setRotationDegrees(0, 0, 0);
head.setMirror(true);
var leg3 = new Model(0, 16);
leg3.addBox(-2, 18, -2, 4, 6, 4);
leg3.setRotationPoint(-3, 0, -5);
leg3.setRotationDegrees(0, 0, 0);
head.setMirror(true);
var leg4 = new Model(0, 16);
leg4.addBox(-2, 0, -2, 4, 6, 4);
leg4.setRotationPoint(3, 18, -5);
leg4.setRotationDegrees(0, 0, 0);
head.setMirror(true);

var GL11 = org.lwjgl.opengl.GL11;

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var sin = deobf ? math.sin(time * 0.16) * 0.02 : math.func_76126_a(time * 0.16) * 0.02;//math.sin(time * 0.16) * 0.02;
    var cos = deobf ? math.cos(time * 0.16) : math.func_76134_b(time * 0.16);//math.cos(time * 0.16);
    GL11.glTranslatef(0.8, -0.5 + sin, 0.3);
    GL11.glRotatef(cos * 4.0, 0.0, 0.6, 0.0);
}
