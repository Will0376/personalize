// Date: 29.12.2016 20:32:26
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");

var Shape1 = new Model(0, 0);
Shape1.addBox(0, 0, 0, 2, 1, 1);
Shape1.setRotationPoint(1, -2, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape2 = new Model(0, 0);
Shape2.addBox(0, 0, 0, 2, 1, 1);
Shape2.setRotationPoint(1, 1, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 0);
Shape3.addBox(0, 0, 0, 1, 2, 1);
Shape3.setRotationPoint(0, -1, 0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 2, 1);
Shape4.setRotationPoint(3, -1, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(14, 0);
Shape5.addBox(0, 0, 0, 2, 2, 0);
Shape5.setRotationPoint(1, -1, 0.5);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 1, 1);
Shape6.setRotationPoint(4, 1, 1);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
