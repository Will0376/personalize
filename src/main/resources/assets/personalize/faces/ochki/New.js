// Date: 09.01.2017 15:48:50
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -3, 5);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(0, 16);
Shape2.addBox(0, 0, 0, 3, 1, 1);
Shape2.setRotationPoint(1, 4, -4.3);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(11, 22);
Shape3.addBox(0, 0, 0, 3, 1, 1);
Shape3.setRotationPoint(-4, 4, -4.3);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(11, 19);
Shape4.addBox(0, 0, 0, 2, 1, 1);
Shape4.setRotationPoint(-1, 3, -4.3);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 28);
Shape5.addBox(0, 0, 0, 1, 1, 1);
Shape5.setRotationPoint(-4, 3, -4.3);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 25);
Shape6.addBox(0, 0, 0, 1, 1, 1);
Shape6.setRotationPoint(3, 3, -4.3);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape7 = new Model(5, 27);
Shape7.addBox(0, 0, 0, 1, 1, 4);
Shape7.setRotationPoint(3.3, 3, -4.3);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape8 = new Model(0, 19);
Shape8.addBox(0, 0, 0, 1, 1, 4);
Shape8.setRotationPoint(-4.3, 3, -4.3);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);

var Shape9 = new Model(0, 3);
Shape9.addBox(0, 0, 0, 2, 1, 0);
Shape9.setRotationPoint(1, 3, -4.1);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);

var Shape10 = new Model(0, 5);
Shape10.addBox(0, 0, 0, 2, 1, 0);
Shape10.setRotationPoint(-3, 3, -4.1);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
