// Date: 05.01.2017 6:09:40
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "HUDtexture.png");
base.setPositionCorrection(0, -20, 6.4);
base.setOffsetCorrection(0, 0, 0);

var Band = new Model(16, 5);
Band.addBox(-4.5, 0, -0.5, 9, 1, 1);
Band.setRotationPoint(0, 15, 0);
Band.setRotationDegrees(-5.32907051820075E-15, 0, 0);
Band.setMirror(true);

var BandLeft = new Model(16, 8);
BandLeft.addBox(0, -1, -1, 1, 2, 2);
BandLeft.setRotationPoint(-5, 17, 0);
BandLeft.setRotationDegrees(45, 0, 0);
BandLeft.setMirror(true);

var Shape1 = new Model(0, 13);
Shape1.addBox(0, 0, -6, 0, 1, 6);
Shape1.setRotationPoint(5, 17.5, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape1 = new Model(1, 22);
Shape1.addBox(-1, -1, 0, 1, 1, 4);
Shape1.setRotationPoint(5, 18, -5);
Shape1.setRotationDegrees(0, -90, -60);
Shape1.setMirror(true);

var Shape2 = new Model(1, 28);
Shape2.addBox(-4, 0, 0, 4, 3, 0);
Shape2.setRotationPoint(4, 18, -5.5);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var BandLeft = new Model(0, 9);
BandLeft.addBox(0, -0.5, -0.5, 1, 1, 1);
BandLeft.setRotationPoint(4.5, 17, 0);
BandLeft.setRotationDegrees(45, 0, 0);
BandLeft.setMirror(true);

var BandLeft = new Model(16, 8);
BandLeft.addBox(0, -1, -1, 1, 2, 2);
BandLeft.setRotationPoint(4, 17, 0);
BandLeft.setRotationDegrees(45, 0, 0);
BandLeft.setMirror(true);

var BandLeft = new Model(0, 9);
BandLeft.addBox(0, -0.5, -0.5, 1, 1, 1);
BandLeft.setRotationPoint(-5.5, 17, 0);
BandLeft.setRotationDegrees(45, 0, 0);
BandLeft.setMirror(true);

var BandLeft = new Model(24, 8);
BandLeft.addBox(0, -0.5, -0.5, 0, 1, 6);
BandLeft.setRotationPoint(-5, 17.5, 0);
BandLeft.setRotationDegrees(-122.686390532544, 0, 0);
BandLeft.setMirror(true);

var BandLeft = new Model(31, 26);
BandLeft.addBox(0, -0.5, -0.5, 0, 1, 3);
BandLeft.setRotationPoint(-4.5, 22, -6.5);
BandLeft.setRotationDegrees(0, 90, 0);
BandLeft.setMirror(true);

var BandLeft = new Model(28, 16);
BandLeft.addBox(0, -0.5, -4.5, 0, 1, 4);
BandLeft.setRotationPoint(-5, 22, -2);
BandLeft.setRotationDegrees(0, 0, 0);
BandLeft.setMirror(true);

var BandLeft = new Model(26, 28);
BandLeft.addBox(-0.5, -0.5, 2.5, 1, 1, 1);
BandLeft.setRotationPoint(-4.5, 22, -6.5);
BandLeft.setRotationDegrees(0, 90, 45);
BandLeft.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
