base.setTexture(64, 32, "WolfMasktexture.png");
base.setPositionCorrection(0, -19, 5);
base.setOffsetCorrection(0, 0, 0);

var head0 = new Model(14, 0);
head0.addBox(-4, -8, -4, 6, 6, 1);
head0.setRotationPoint(1, 25, -1.5);
head0.setRotationDegrees(0, 0, 0);
head0.setMirror(true);
var head1 = new Model(0, 0);
head1.addBox(-4, -8, -4, 3, 3, 4);
head1.setRotationPoint(2.5, 28, -5.5);
head1.setRotationDegrees(0, 0, 0);
head1.setMirror(true);
var head2 = new Model(0, 7);
head2.addBox(-4, -8, -4, 9, 1, 9);
head2.setRotationPoint(-0.5, 27, -0.5);
head2.setRotationDegrees(0, 0, 0);
head2.setMirror(true);
