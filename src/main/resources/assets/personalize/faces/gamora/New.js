// Date: 07.01.2017 3:57:03
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -4, 5);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(0, 23);
Shape2.addBox(0, 0, 0, 8, 8, 1);
Shape2.setRotationPoint(-4, 0, -5);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 12);
Shape3.addBox(0, 0, 0, 0, 1, 9);
Shape3.setRotationPoint(4, 3, -5);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape3 = new Model(0, 0);
Shape3.addBox(0, 0, 0, 0, 1, 9);
Shape3.setRotationPoint(-4, 3, -5);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(19, 10);
Shape4.addBox(0, 0, 0, 8, 1, 0);
Shape4.setRotationPoint(-4, 3, 4);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
