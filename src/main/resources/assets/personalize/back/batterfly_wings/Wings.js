base.setScale(0.6);

base.setTexture(64, 32, "Wingstexture.png");
base.setPositionCorrection(0, 0, -2.8);
base.setOffsetCorrection(0, 0, 0);

var Left = new Model(0, -30);
Left.addBox(1.5, -10.0, 6.0, 0, 32, 30);
Left.setRotationPoint(0, 0, 0);
Left.setRotationDegrees(0, -30, 0);
Left.setMirror(true);
var Right = new Model(0, -30);
Right.addBox(-1.5, -10.0, 6.0, 0, 32, 30);
Right.setRotationPoint(0, 0, 0);
Right.setRotationDegrees(0, 30, 0);
Right.setMirror(true);
//var LightFix = new Model(0, 0);
//LightFix.addBox(1.0, 1.0, 1.0, 1, 1, 0);
//LightFix.setRotationPoint(0, 0, 0);
//LightFix.setRotationDegrees(0, 0, 0);
//LightFix.setMirror(true);

//func_71410_x() -> func_71410_x()


function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;
    var angle = deobf ? math.cos(time * 0.05) * 8.0 : math.func_76134_b(time * 0.05) * 8.0;

    if (armorInventory.get(2) != null) {
        base.setOffsetCorrection(0, 0, 0.05);
    }
    Right.setRotationDegrees(0, -20 + angle, 0);
    Left.setRotationDegrees(0, 20 - angle, 0);
}
