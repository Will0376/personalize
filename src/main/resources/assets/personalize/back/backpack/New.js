// Date: 05.01.2017 7:21:11
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 64, "Newtexture.png");
base.setPositionCorrection(0, 0, -2);
base.setOffsetCorrection(0, 0, 0);

var SleepingMat = new Model(0, 33);
SleepingMat.addBox(0, 0, 0, 12, 4, 4);
SleepingMat.setRotationPoint(-6, -4, 5);
SleepingMat.setRotationDegrees(0, 0, 0);
SleepingMat.setMirror(true);

var Flap1 = new Model(33, 0);
Flap1.addBox(0, 0, 0, 9, 1, 5);
Flap1.setRotationPoint(-4.5, 0, 2);
Flap1.setRotationDegrees(0, 0, 0);
Flap1.setMirror(true);

var Flap2 = new Model(33, 7);
Flap2.addBox(0, 0, 0, 9, 4, 1);
Flap2.setRotationPoint(-4.5, 0, 7);
Flap2.setRotationDegrees(0, 0, 0);
Flap2.setMirror(true);

var MainBag = new Model(28, 50);
MainBag.addBox(0, 0, 0, 8, 9, 5);
MainBag.setRotationPoint(-4, 1, 2);
MainBag.setRotationDegrees(0, 0, 0);
MainBag.setMirror(true);

var TieDown1 = new Model(0, 0);
TieDown1.addBox(0, 0, 0, 1, 4, 0);
TieDown1.setRotationPoint(-0.5, 4, 8);
TieDown1.setRotationDegrees(-14, 0, 0);
TieDown1.setMirror(true);

var TieDown2 = new Model(0, 0);
TieDown2.addBox(0, 0, 0, 1, 7, 0);
TieDown2.setRotationPoint(2, 0, 9);
TieDown2.setRotationDegrees(-13, 0, 0);
TieDown2.setMirror(true);

var TieDown3 = new Model(0, 0);
TieDown3.addBox(0, 0, 0, 1, 7, 0);
TieDown3.setRotationPoint(-3, 0, 9);
TieDown3.setRotationDegrees(-13, 0, 0);
TieDown3.setMirror(true);

var TieDown4 = new Model(0, 0);
TieDown4.addBox(0, 0, 0, 1, 4, 0);
TieDown4.setRotationPoint(-3, -4, 5);
TieDown4.setRotationDegrees(-13, 0, 0);
TieDown4.setMirror(true);

var TieDown5 = new Model(0, 0);
TieDown5.addBox(0, 0, 0, 1, 4, 0);
TieDown5.setRotationPoint(2, -4, 5);
TieDown5.setRotationDegrees(-13, 0, 0);
TieDown5.setMirror(true);

var Buckle1 = new Model(0, 10);
Buckle1.addBox(0, 0, 0, 2, 1, 1);
Buckle1.setRotationPoint(-1, 6, 7);
Buckle1.setRotationDegrees(0, 0, 0);
Buckle1.setMirror(true);

var Buckle2 = new Model(0, 10);
Buckle2.addBox(0, 0, 0, 2, 1, 1);
Buckle2.setRotationPoint(-3.5, 6.5, 7);
Buckle2.setRotationDegrees(0, 0, 0);
Buckle2.setMirror(true);

var Buckle3 = new Model(0, 10);
Buckle3.addBox(0, 0, 0, 2, 1, 1);
Buckle3.setRotationPoint(1.5, 6.5, 7);
Buckle3.setRotationDegrees(0, 0, 0);
Buckle3.setMirror(true);

var Bag1 = new Model(0, 50);
Bag1.addBox(-3, 0, 0, 3, 4, 3);
Bag1.setRotationPoint(-3, 6, 7);
Bag1.setRotationDegrees(0, -60, 0);
Bag1.setMirror(true);

var Bag2 = new Model(0, 50);
Bag2.addBox(0, 0, 0, 3, 4, 3);
Bag2.setRotationPoint(3, 6, 7);
Bag2.setRotationDegrees(0, 60, 0);
Bag2.setMirror(true);

var Pouch2 = new Model(0, 57);
Pouch2.addBox(-2, 0, -3, 2, 4, 3);
Pouch2.setRotationPoint(4, 1, 3);
Pouch2.setRotationDegrees(0, 180, 0);
Pouch2.setMirror(true);

var Roll = new Model(0, 42);
Roll.addBox(0, 0, 0, 10, 3, 4);
Roll.setRotationPoint(-5, 10, 3.5);
Roll.setRotationDegrees(0, 0, 0);
Roll.setMirror(true);

var LeftStrap1 = new Model(4, 0);
LeftStrap1.addBox(0, 0, 0, 1, 6, 1);
LeftStrap1.setRotationPoint(3.6, -0.5, 3);
LeftStrap1.setRotationDegrees(-90, 0, 0);
LeftStrap1.setMirror(true);

var LeftStrap2 = new Model(10, 0);
LeftStrap2.addBox(-1, 0, 0, 1, 8, 1);
LeftStrap2.setRotationPoint(4.6, 0.5, -3);
LeftStrap2.setRotationDegrees(0, 0, 2);
LeftStrap2.setMirror(true);

var LeftStrap3 = new Model(4, 0);
LeftStrap3.addBox(0, 0, 0, 1, 6, 1);
LeftStrap3.setRotationPoint(3.4, 8, 3);
LeftStrap3.setRotationDegrees(-90, 0, 0);
LeftStrap3.setMirror(true);

var RightStrap1 = new Model(4, 0);
RightStrap1.addBox(0, 0, 0, 1, 6, 1);
RightStrap1.setRotationPoint(-4.6, -0.5, 3);
RightStrap1.setRotationDegrees(-90, 0, 0);
RightStrap1.setMirror(true);

var RightStrap2 = new Model(10, 0);
RightStrap2.addBox(0, 0, 0, 1, 8, 1);
RightStrap2.setRotationPoint(-4.6, 0.5, -3);
RightStrap2.setRotationDegrees(0, 0, -2);
RightStrap2.setMirror(true);

var RightStrap3 = new Model(4, 0);
RightStrap3.addBox(0, 0, 0, 1, 6, 1);
RightStrap3.setRotationPoint(-4.4, 8, 3);
RightStrap3.setRotationDegrees(-90, 0, 0);
RightStrap3.setMirror(true);

var RollStrap1 = new Model(14, 55);
RollStrap1.addBox(0, 0, 0, 1, 4, 5);
RollStrap1.setRotationPoint(-2, 9.5, 3);
RollStrap1.setRotationDegrees(0, 0, 0);
RollStrap1.setMirror(true);

var RollStrap2 = new Model(14, 55);
RollStrap2.addBox(0, 0, 0, 1, 4, 5);
RollStrap2.setRotationPoint(1, 9.5, 3);
RollStrap2.setRotationDegrees(0, 0, 0);
RollStrap2.setMirror(true);

var PotBase1 = new Model(38, 39);
PotBase1.addBox(0, -1, -2, 1, 2, 4);
PotBase1.setRotationPoint(-5, 3, 5);
PotBase1.setRotationDegrees(0, 0, 0);
PotBase1.setMirror(true);

var PotBase2 = new Model(38, 39);
PotBase2.addBox(0, -1, -2, 1, 2, 4);
PotBase2.setRotationPoint(-5, 3, 5);
PotBase2.setRotationDegrees(-60, 0, 0);
PotBase2.setMirror(true);

var PotBase3 = new Model(38, 39);
PotBase3.addBox(0, -1, -2, 1, 2, 4);
PotBase3.setRotationPoint(-5, 3, 5);
PotBase3.setRotationDegrees(-120, 0, 0);
PotBase3.setMirror(true);

var PotRim1 = new Model(38, 33);
PotRim1.addBox(0, 0, 0, 2, 3, 1);
PotRim1.setRotationPoint(-6.5, -0.2, 4.8);
PotRim1.setRotationDegrees(-57, 0, 0);
PotRim1.setMirror(true);

var PotRim2 = new Model(38, 33);
PotRim2.addBox(0, 0, 0, 2, 3, 1);
PotRim2.setRotationPoint(-6.5, 3.5, 7);
PotRim2.setRotationDegrees(-60, 0, 0);
PotRim2.setMirror(true);

var PotRim3 = new Model(38, 33);
PotRim3.addBox(0, 0, 0, 2, 3, 1);
PotRim3.setRotationPoint(-6.5, 1.4, 2.3);
PotRim3.setRotationDegrees(0, 0, 0);
PotRim3.setMirror(true);

var PotRim4 = new Model(38, 33);
PotRim4.addBox(0, 0, 0, 2, 3, 1);
PotRim4.setRotationPoint(-6.5, 1.4, 6.4);
PotRim4.setRotationDegrees(2, 0, 0);
PotRim4.setMirror(true);

var PotRim5 = new Model(38, 33);
PotRim5.addBox(0, 0, 0, 2, 3, 1);
PotRim5.setRotationPoint(-6.5, 5, 5.4);
PotRim5.setRotationDegrees(-120, 0, 0);
PotRim5.setMirror(true);

var PotRim6 = new Model(38, 33);
PotRim6.addBox(0, 0, 0, 2, 3, 1);
PotRim6.setRotationPoint(-6.5, 1.3, 7.4);
PotRim6.setRotationDegrees(-120, 0, 0);
PotRim6.setMirror(true);

var PotHandle = new Model(38, 33);
PotHandle.addBox(-0.7, -6.5, -0.5, 1, 4, 1);
PotHandle.setRotationPoint(-5, 3, 5);
PotHandle.setRotationDegrees(-30, -4, -11);
PotHandle.setMirror(true);

var HerbStalk1 = new Model(15, 6);
HerbStalk1.addBox(0, 0, 0, 3, 1, 1);
HerbStalk1.setRotationPoint(0, 12.5, 5);
HerbStalk1.setRotationDegrees(0, 0, 28);
HerbStalk1.setMirror(true);

var HerbStalk2 = new Model(15, 6);
HerbStalk2.addBox(0, 0, 0, 2, 1, 1);
HerbStalk2.setRotationPoint(-2, 12.4, 5);
HerbStalk2.setRotationDegrees(0, 0, 0);
HerbStalk2.setMirror(true);

var HerbStalk3 = new Model(15, 6);
HerbStalk3.addBox(0, 0, 0, 2, 1, 1);
HerbStalk3.setRotationPoint(-2, 12.4, 6);
HerbStalk3.setRotationDegrees(0, 180, 18);
HerbStalk3.setMirror(false);

var HerbLeaf1 = new Model(15, 5);
HerbLeaf1.addBox(0, 0, 0, 3, 0, 1);
HerbLeaf1.setRotationPoint(2, 14, 5);
HerbLeaf1.setRotationDegrees(0, 43, 53);
HerbLeaf1.setMirror(true);

var HerbLeaf2 = new Model(15, 2);
HerbLeaf2.addBox(0, 0, -1, 5, 0, 1);
HerbLeaf2.setRotationPoint(2, 14, 6);
HerbLeaf2.setRotationDegrees(-4, -26, 72);
HerbLeaf2.setMirror(true);

var HerbLeaf3 = new Model(15, 4);
HerbLeaf3.addBox(0, 0, 0, 4, 0, 1);
HerbLeaf3.setRotationPoint(1, 13.5, 5);
HerbLeaf3.setRotationDegrees(-4, 29, 76);
HerbLeaf3.setMirror(true);

var HerbLeaf4 = new Model(15, 3);
HerbLeaf4.addBox(0, 0, 0, 4, 0, 1);
HerbLeaf4.setRotationPoint(2.5, 14, 5);
HerbLeaf4.setRotationDegrees(-4, -28, 89);
HerbLeaf4.setMirror(true);

var Garlic1 = new Model(0, 7);
Garlic1.addBox(0, 0, 0, 1, 1, 1);
Garlic1.setRotationPoint(4, 6, 3);
Garlic1.setRotationDegrees(0, 7, 0);
Garlic1.setMirror(true);

var Garlic2 = new Model(0, 7);
Garlic2.addBox(0, 0, 0, 1, 1, 1);
Garlic2.setRotationPoint(4.3, 7, 2.5);
Garlic2.setRotationDegrees(0, -34, 0);
Garlic2.setMirror(true);

var Garlic3 = new Model(0, 7);
Garlic3.addBox(0, 0, 0, 1, 1, 1);
Garlic3.setRotationPoint(4.8, 8, 3);
Garlic3.setRotationDegrees(0, -14, 0);
Garlic3.setMirror(true);

var Garlic4 = new Model(0, 7);
Garlic4.addBox(0, 0, 0, 1, 1, 1);
Garlic4.setRotationPoint(4, 9, 2.7);
Garlic4.setRotationDegrees(0, -14, 0);
Garlic4.setMirror(true);

var Garlic5 = new Model(0, 7);
Garlic5.addBox(0, 0, 0, 1, 1, 1);
Garlic5.setRotationPoint(6, 9.5, 2);
Garlic5.setRotationDegrees(0, -56, 0);
Garlic5.setMirror(true);

var Garlic6 = new Model(0, 7);
Garlic6.addBox(0, 0, 0, 1, 1, 1);
Garlic6.setRotationPoint(5, 9, 3.5);
Garlic6.setRotationDegrees(0, 18, 0);
Garlic6.setMirror(true);

var Garlic7 = new Model(0, 7);
Garlic7.addBox(0, 0, 0, 1, 1, 1);
Garlic7.setRotationPoint(5, 10, 3);
Garlic7.setRotationDegrees(0, 57, 0);
Garlic7.setMirror(true);

var Garlic8 = new Model(0, 7);
Garlic8.addBox(0, 0, 0, 1, 1, 1);
Garlic8.setRotationPoint(6, 10, 3.3);
Garlic8.setRotationDegrees(0, -19, 0);
Garlic8.setMirror(true);

var Garlic9 = new Model(0, 7);
Garlic9.addBox(0, 0, 0, 1, 1, 1);
Garlic9.setRotationPoint(5, 11, 3);
Garlic9.setRotationDegrees(0, -14, 0);
Garlic9.setMirror(true);

var GarlicTieDown = new Model(0, 0);
GarlicTieDown.addBox(0, 0, 0, 1, 2, 0);
GarlicTieDown.setRotationPoint(4.5, 5, 4);
GarlicTieDown.setRotationDegrees(14, 104, 0);
GarlicTieDown.setMirror(true);

var WaterJug = new Model(0, 27);
WaterJug.addBox(0, 0, 0, 5, 3, 3);
WaterJug.setRotationPoint(-2.5, 7.5, 8);
WaterJug.setRotationDegrees(-23, 0, 0);
WaterJug.setMirror(true);

var JugSpout = new Model(7, 29);
JugSpout.addBox(0, 0, 0, 1, 1, 1);
JugSpout.setRotationPoint(-2.5, 9.5, 9.9);
JugSpout.setRotationDegrees(-23, -27, 12);
JugSpout.setMirror(true);

var JugLid = new Model(0, 23);
JugLid.addBox(0, 0, 0, 1, 2, 2);
JugLid.setRotationPoint(-3, 8.2, 8.3);
JugLid.setRotationDegrees(-23, 0, 0);
JugLid.setMirror(true);

var JugBase = new Model(0, 23);
JugBase.addBox(0, 0, 0, 1, 2, 2);
JugBase.setRotationPoint(2, 8.2, 8.3);
JugBase.setRotationDegrees(-23, 0, 0);
JugBase.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;
}
*/
