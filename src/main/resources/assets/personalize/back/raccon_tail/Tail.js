// Date: 05.01.2017 6:53:38
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(128, 32, "Tailtexture.png");
base.setPositionCorrection(0, 12, 0);
base.setOffsetCorrection(0, 0, 0);

var tailBase = new Model(12, 16);
tailBase.addBox(-1, -1, 0, 2, 2, 2);
tailBase.setRotationPoint(0, 0, 0);

var tail1 = new Model(0, 16);
tail1.addBox(-1.5, -1.5, 0, 3, 3, 3);
tail1.setRotationPoint(0, 0, 1);
tail1.setRotationDegrees(-40, 0, 0);

var tail2 = new Model(0, 0);
tail2.addBox(-2, -2, 0, 4, 4, 12);
tail2.setRotationPoint(0, 0, 2);
tail2.setRotationDegrees(-30, 0, 0);

var tailTip = new Model(0, 22);
tailTip.addBox(-1.5, -1.5, 0, 3, 3, 1);
tailTip.setRotationPoint(0, 6, 12);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;
}
*/
