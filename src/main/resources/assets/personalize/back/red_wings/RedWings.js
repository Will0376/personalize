// Date: 05.01.2017 6:53:38
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(128, 32, "RedWingstexture.png");
base.setPositionCorrection(0, 0, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(0, 10);
Shape1.addBox(0, 0, 0, 10, 10, 0);
Shape1.setRotationPoint(0, 0, 0);
Shape1.setRotationDegrees(0, -30, 0);
Shape1.setMirror(true);

var Shape2 = new Model(0, 0);
Shape2.addBox(-10, 0, 0, 10, 10, 0);
Shape2.setRotationPoint(0, 0, 0);
Shape2.setRotationDegrees(0, 30, 0);
Shape2.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;
}
*/
