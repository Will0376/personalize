base.setTexture(64, 32, "White Bear Earstexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape10 = new Model(4, 15);
Shape10.addBox(0, 0, 0, 10, 1, 1);
Shape10.setRotationPoint(-5, 15, -1);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);
var Shape11 = new Model(4, 15);
Shape11.addBox(0, 0, 0, 1, 6, 1);
Shape11.setRotationPoint(-5, 15, -1);
Shape11.setRotationDegrees(0, 0, 0);
Shape11.setMirror(true);
var Shape12 = new Model(4, 15);
Shape12.addBox(0, 0, 0, 1, 6, 1);
Shape12.setRotationPoint(4, 15, -1);
Shape12.setRotationDegrees(0, 0, 0);
Shape12.setMirror(true);
var Shape23 = new Model(35, 7);
Shape23.addBox(0, 0, 0, 3, 2, 1);
Shape23.setRotationPoint(-4.5, 13.4, -1.1);
Shape23.setRotationDegrees(0, 0, 0);
Shape23.setMirror(true);
var Shape24 = new Model(46, 7);
Shape24.addBox(0, 0, 0, 3, 2, 1);
Shape24.setRotationPoint(1.5, 13.4, -1.1);
Shape24.setRotationDegrees(0, 0, 0);
Shape24.setMirror(true);
var Shape35 = new Model(46, 3);
Shape35.addBox(0, 0, 0, 2, 1, 1);
Shape35.setRotationPoint(2, 12.4, -1.1);
Shape35.setRotationDegrees(0, 0, 0);
Shape35.setMirror(true);
var Shape36 = new Model(35, 3);
Shape36.addBox(0, 0, 0, 2, 1, 1);
Shape36.setRotationPoint(-4, 12.4, -1.1);
Shape36.setRotationDegrees(0, 0, 0);
Shape36.setMirror(true);
