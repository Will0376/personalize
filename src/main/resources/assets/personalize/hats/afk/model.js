base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape10 = new Model(0, 0);
Shape10.addBox(0, 0, 0, 4, 1, 1);
Shape10.setRotationPoint(3, 13, -2);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);
var Shape11 = new Model(0, 0);
Shape11.addBox(0, 0, 0, 4, 1, 1);
Shape11.setRotationPoint(0, 10, 0);
Shape11.setRotationDegrees(0, 0, 0);
Shape11.setMirror(true);
var Shape12 = new Model(0, 0);
Shape12.addBox(0, 0, 0, 4, 1, 1);
Shape12.setRotationPoint(0, 10, 0);
Shape12.setRotationDegrees(0, 0, -28);
Shape12.setMirror(true);
var Shape13 = new Model(0, 0);
Shape13.addBox(0, 0, 0, 4, 1, 1);
Shape13.setRotationPoint(0, 8, 0);
Shape13.setRotationDegrees(0, 0, 0);
Shape13.setMirror(true);
var Shape14 = new Model(0, 0);
Shape14.addBox(0, 0, 0, 4, 1, 1);
Shape14.setRotationPoint(3, 11, -2);
Shape14.setRotationDegrees(0, 0, 0);
Shape14.setMirror(true);
var Shape15 = new Model(0, 0);
Shape15.addBox(0, 0, 0, 4, 1, 1);
Shape15.setRotationPoint(3, 13, -2);
Shape15.setRotationDegrees(0, 0, -28);
Shape15.setMirror(true);
var Shape16 = new Model(0, 0);
Shape16.addBox(0, 0, 0, 4, 1, 1);
Shape16.setRotationPoint(-3, 7, -4);
Shape16.setRotationDegrees(0, 0, -28);
Shape16.setMirror(true);
var Shape17 = new Model(0, 0);
Shape17.addBox(0, 0, 0, 4, 1, 1);
Shape17.setRotationPoint(-3, 7, -4);
Shape17.setRotationDegrees(0, 0, 0);
Shape17.setMirror(true);
var Shape18 = new Model(0, 0);
Shape18.addBox(0, 0, 0, 4, 1, 1);
Shape18.setRotationPoint(-3, 5, -4);
Shape18.setRotationDegrees(0, 0, 0);
Shape18.setMirror(true);
