base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var top0 = new Model(19, 19);
top0.addBox(-4, 15, -4, 8, 5, 8);
top0.setRotationPoint(0, -5, 0);
top0.setRotationDegrees(0, 0, 0);
top0.setMirror(true);
var top1 = new Model(0, 6);
top1.addBox(-4, 15, -4, 9, 1, 9);
top1.setRotationPoint(-0.5, 0, -0.5);
top1.setRotationDegrees(0, 0, 0);
top1.setMirror(true);
