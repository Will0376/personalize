base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var shape1 = new Model(0, 21);
shape1.addBox(-3.5, 17, -9.5, 7, 2, 1);
shape1.setRotationPoint(0, 0, 0);
shape1.setRotationDegrees(0, 0, 0);
shape1.setMirror(true);
var shape2 = new Model(0, 15);
shape2.addBox(-4.5, 17, -8.5, 9, 2, 4);
shape2.setRotationPoint(0, 0, 0);
shape2.setRotationDegrees(0, 0, 0);
shape2.setMirror(true);
var shape3 = new Model(0, 0);
shape3.addBox(-4.5, 13, -4.5, 9, 6, 9);
shape3.setRotationPoint(0, 0, 0);
shape3.setRotationDegrees(0, 0, 0);
shape3.setMirror(true);
