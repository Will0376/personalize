base.setTexture(64, 64, "Newtexture.png");
base.setPositionCorrection(0, -14, 0);
base.setOffsetCorrection(0, 0, 0);

var shape1 = new Model(0, 0);
shape1.addBox(0, 0, 0, 10, 10, 10);
shape1.setRotationPoint(-5, 14, -5);
shape1.setRotationDegrees(0, 0, 0);
shape1.setMirror(true);
