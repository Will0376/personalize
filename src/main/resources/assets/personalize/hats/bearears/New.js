base.setTexture(16, 8, "Newtexture.png");
base.setPositionCorrection(0, 10, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(0, 0);
Shape1.addBox(-6.0, -11.0, -1.0, 4, 4, 1);
Shape1.setRotationPoint(0.0, 0, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(false);
var Shape2 = new Model(0, 0);
Shape2.addBox(2.0, -11.0, -1.0, 4, 4, 1);
Shape2.setRotationPoint(0.0, 0, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(false);
