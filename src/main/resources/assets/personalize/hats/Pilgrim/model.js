base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var top0 = new Model(31, 11);
top0.addBox(-3.5, -16, -3.5, 7, 9, 7);
top0.setRotationPoint(0, 22, 0);
top0.setRotationDegrees(0, 0, 0);
top0.setMirror(true);
var bottom1 = new Model(1, 13);
bottom1.addBox(-4.5, -10, -4.5, 1, 1, 7);
bottom1.setRotationPoint(9, 25, 1);
bottom1.setRotationDegrees(0, 0, 0);
bottom1.setMirror(true);
var bottom2 = new Model(0, 0);
bottom2.addBox(-4.5, -10, -4.5, 9, 1, 9);
bottom2.setRotationPoint(0, 25, 0);
bottom2.setRotationDegrees(0, 0, 0);
bottom2.setMirror(true);
var bottom3 = new Model(19, 13);
bottom3.addBox(-4.5, -10, -4.5, 3, 1, 1);
bottom3.setRotationPoint(3, 21, 0);
bottom3.setRotationDegrees(0, 0, 0);
bottom3.setMirror(true);
var bottom4 = new Model(1, 18);
bottom4.addBox(-4.5, -10, -4.5, 7, 1, 1);
bottom4.setRotationPoint(1, 25, 9);
bottom4.setRotationDegrees(0, 0, 0);
bottom4.setMirror(true);
var bottom5 = new Model(1, 13);
bottom5.addBox(-4.5, -10, -4.5, 1, 1, 7);
bottom5.setRotationPoint(-1, 25, 1);
bottom5.setRotationDegrees(0, 0, 0);
bottom5.setMirror(true);
var bottom6 = new Model(1, 18);
bottom6.addBox(-4.5, -10, -4.5, 7, 1, 1);
bottom6.setRotationPoint(1, 25, -1);
bottom6.setRotationDegrees(0, 0, 0);
bottom6.setMirror(true);
var bottom7 = new Model(19, 13);
bottom7.addBox(-4.5, -10, -4.5, 1, 1, 1);
bottom7.setRotationPoint(5, 22, 0);
bottom7.setRotationDegrees(0, 0, 0);
bottom7.setMirror(true);
var bottom8 = new Model(19, 13);
bottom8.addBox(-4.5, -10, -4.5, 3, 1, 1);
bottom8.setRotationPoint(3, 23, 0);
bottom8.setRotationDegrees(0, 0, 0);
bottom8.setMirror(true);
var bottom9 = new Model(19, 13);
bottom9.addBox(-4.5, -10, -4.5, 1, 1, 1);
bottom9.setRotationPoint(3, 22, 0);
bottom9.setRotationDegrees(0, 0, 0);
bottom9.setMirror(true);
var bottom10 = new Model(1, 23);
bottom10.addBox(-4.5, -10, -4.5, 1, 1, 7);
bottom10.setRotationPoint(7.2, 22, 0.8);
bottom10.setRotationDegrees(0, 0, 0);
bottom10.setMirror(true);
var bottom11 = new Model(21, 13);
bottom11.addBox(-4.5, -10, -4.5, 1, 1, 1);
bottom11.setRotationPoint(3, 22, 0);
bottom11.setRotationDegrees(0, 0, 0);
bottom11.setMirror(true);
var bottom12 = new Model(1, 23);
bottom12.addBox(-4.5, -10, -4.5, 4, 1, 1);
bottom12.setRotationPoint(1, 22, 0.8);
bottom12.setRotationDegrees(0, 0, 0);
bottom12.setMirror(true);
var bottom13 = new Model(1, 23);
bottom13.addBox(-4.5, -10, -4.5, 7, 1, 1);
bottom13.setRotationPoint(1.2, 22, 7.2);
bottom13.setRotationDegrees(0, 0, 0);
bottom13.setMirror(true);
var bottom14 = new Model(1, 23);
bottom14.addBox(-4.5, -10, -4.5, 1, 1, 1);
bottom14.setRotationPoint(0.8, 22, 7.2);
bottom14.setRotationDegrees(0, 0, 0);
bottom14.setMirror(true);
var bottom15 = new Model(1, 23);
bottom15.addBox(-4.5, -10, -4.5, 2, 1, 1);
bottom15.setRotationPoint(6, 22, 0.8);
bottom15.setRotationDegrees(0, 0, 0);
bottom15.setMirror(true);
var bottom16 = new Model(1, 23);
bottom16.addBox(-4.5, -10, -4.5, 1, 1, 7);
bottom16.setRotationPoint(7.2, 22, 0.8);
bottom16.setRotationDegrees(0, 0, 0);
bottom16.setMirror(true);
var bottom17 = new Model(1, 23);
bottom17.addBox(-4.5, -10, -4.5, 1, 1, 7);
bottom17.setRotationPoint(0.8, 22, 0.8);
bottom17.setRotationDegrees(0, 0, 0);
bottom17.setMirror(true);
