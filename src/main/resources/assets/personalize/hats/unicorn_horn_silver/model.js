base.setTexture(64, 32, "Silver Unicorn Horntexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape10 = new Model(10, 0);
Shape10.addBox(0, 0, 0, 1, 1, 7);
Shape10.setRotationPoint(-0.5, 13.1, -9.1);
Shape10.setRotationDegrees(-25, 0, 0);
Shape10.setMirror(true);
var Shape11 = new Model(10, 0);
Shape11.addBox(0, 0, 0, 1, 1, 7);
Shape11.setRotationPoint(-0.6, 13.1, -9.1);
Shape11.setRotationDegrees(-30, -6, 0);
Shape11.setMirror(true);
var Shape12 = new Model(10, 0);
Shape12.addBox(0, 0, 0, 1, 1, 7);
Shape12.setRotationPoint(-0.5, 13.1, -9.1);
Shape12.setRotationDegrees(-33, 0, 0);
Shape12.setMirror(true);
var Shape13 = new Model(10, 0);
Shape13.addBox(0, 0, 0, 1, 1, 7);
Shape13.setRotationPoint(-0.4, 13.1, -9.1);
Shape13.setRotationDegrees(-30, 6, 0);
Shape13.setMirror(true);
