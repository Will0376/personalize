// Date: 09.01.2017 15:49:13
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 1, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(0, 0);
Shape2.addBox(0, 0, 0, 8, 1, 8);
Shape2.setRotationPoint(-4, -1, -4);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 0);
Shape3.addBox(0, 0, 0, 10, 9, 1);
Shape3.setRotationPoint(-5, -1, 4);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 9, 9);
Shape4.setRotationPoint(-5, -1, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 9, 9);
Shape5.setRotationPoint(4, -1, -5);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 8, 1, 1);
Shape6.setRotationPoint(-4, -1, -5);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape8 = new Model(0, 0);
Shape8.addBox(0, 0, 0, 1, 2, 1);
Shape8.setRotationPoint(-2, 5, -5);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);

var Shape8 = new Model(0, 0);
Shape8.addBox(0, 0, 0, 1, 2, 1);
Shape8.setRotationPoint(1, 5, -5);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);

var Shape9 = new Model(0, 0);
Shape9.addBox(0, 0, 0, 2, 1, 1);
Shape9.setRotationPoint(-1, 5, -5);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(0, 0, 0, 3, 1, 2);
Shape7.setRotationPoint(-6, -2, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(0, 0, 0, 3, 1, 2);
Shape7.setRotationPoint(3, -2, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape1 = new Model(0, 0);
Shape1.addBox(0, 0, 0, 1, 2, 2);
Shape1.setRotationPoint(5, -2, -5);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape1 = new Model(0, 0);
Shape1.addBox(0, 0, 0, 1, 2, 2);
Shape1.setRotationPoint(-6, -2, -5);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
