// Date: 07.01.2017 3:56:23
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 64, "Newtexture.png");
base.setPositionCorrection(0, 1, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(0, 51);
Shape2.addBox(0, 0, 0, 1, 8, 1);
Shape2.setRotationPoint(-4, 0, -5);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 61);
Shape3.addBox(0, 0, 0, 6, 2, 1);
Shape3.setRotationPoint(-3, 0, -5);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape2 = new Model(0, 41);
Shape2.addBox(0, 0, 0, 1, 8, 1);
Shape2.setRotationPoint(3, 0, -5);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 38);
Shape3.addBox(0, 0, 0, 6, 1, 1);
Shape3.setRotationPoint(-3, 7, -5);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(5, 47);
Shape4.addBox(0, 0, 0, 1, 1, 1);
Shape4.setRotationPoint(2, 2, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape4 = new Model(5, 50);
Shape4.addBox(0, 0, 0, 1, 1, 1);
Shape4.setRotationPoint(-3, 2, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape4 = new Model(5, 41);
Shape4.addBox(-3, 0, 0, 1, 1, 1);
Shape4.setRotationPoint(0, 6, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape4 = new Model(5, 44);
Shape4.addBox(2, 0, 0, 1, 1, 1);
Shape4.setRotationPoint(0, 6, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(15, 38);
Shape5.addBox(0, 0, 0, 1, 8, 9);
Shape5.setRotationPoint(4, 0, -5);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(36, 38);
Shape6.addBox(0, 0, 0, 10, 8, 1);
Shape6.setRotationPoint(-5, 0, 4);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape7 = new Model(41, 19);
Shape7.addBox(0, 0, 0, 1, 8, 10);
Shape7.setRotationPoint(-5, 0, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape8 = new Model(0, 26);
Shape8.addBox(0, 0, 0, 10, 1, 10);
Shape8.setRotationPoint(-5, -1, -5);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);

var Shape1 = new Model(0, 4);
Shape1.addBox(0, 0, 0, 2, 2, 1);
Shape1.setRotationPoint(-5, -3, -5);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape1 = new Model(0, 0);
Shape1.addBox(0, 0, 0, 2, 2, 1);
Shape1.setRotationPoint(3, -3, -5);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
