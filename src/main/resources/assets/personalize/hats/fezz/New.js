//base.setScale(0.5);
// Date: 05.01.2017 7:38:52
// Personalize model JS exported from Techne - Denr01 Edition
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var one = new Model(0, 0);
one.addBox(0, 0, 0, 3, 3, 3);
one.setRotationPoint(-2, 13, -1);
one.setRotationDegrees(0, 0, 0);
one.setMirror(true);
var two = new Model(12, 0);
two.addBox(0, 0, 0, 1, 3, 1);
two.setRotationPoint(-2.1, 12.8, 1.05);
two.setRotationDegrees(0, 0, 0.017453292);
two.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
