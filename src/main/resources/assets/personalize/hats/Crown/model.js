base.setTexture(64, 32, "Hattexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var shape1 = new Model(0, 0);
shape1.addBox(0, 0, 0, 10, 4, 1);
shape1.setRotationPoint(-5, 14, 4);
shape1.setRotationDegrees(0, 0, 0);
shape1.setMirror(true);
var shape2 = new Model(0, 0);
shape2.addBox(0, 0, 0, 10, 4, 1);
shape2.setRotationPoint(-5, 14, 5);
shape2.setRotationDegrees(0, 90, 0);
shape2.setMirror(true);
var shape3 = new Model(0, 0);
shape3.addBox(0, 0, 0, 10, 4, 1);
shape3.setRotationPoint(-5, 14, -5);
shape3.setRotationDegrees(0, 0, 0);
shape3.setMirror(true);
var shape4 = new Model(0, 0);
shape4.addBox(0, 0, 0, 10, 4, 1);
shape4.setRotationPoint(4, 14, 5);
shape4.setRotationDegrees(0, 90, 0);
shape4.setMirror(true);
var shape5 = new Model(0, 0);
shape5.addBox(0, 0, 0, 8, 1, 8);
shape5.setRotationPoint(-4, 15, -4);
shape5.setRotationDegrees(0, 0, 0);
shape5.setMirror(true);
var shape6 = new Model(0, 0);
shape6.addBox(0, 0, 0, 1, 1, 1);
shape6.setRotationPoint(4, 13, 4);
shape6.setRotationDegrees(0, 0, 0);
shape6.setMirror(true);
var shape7 = new Model(0, 0);
shape7.addBox(0, 0, 0, 1, 1, 1);
shape7.setRotationPoint(-5, 13, 4);
shape7.setRotationDegrees(0.017453292, 0, 0);
shape7.setMirror(true);
var shape8 = new Model(0, 0);
shape8.addBox(0, 0, 0, 1, 1, 1);
shape8.setRotationPoint(1, 13, 4);
shape8.setRotationDegrees(0, 0, 0);
shape8.setMirror(true);
var shape9 = new Model(0, 0);
shape9.addBox(0, 0, 0, 1, 1, 1);
shape9.setRotationPoint(-2, 13, 4);
shape9.setRotationDegrees(0, 0, 0);
shape9.setMirror(true);
var shape10 = new Model(0, 0);
shape10.addBox(0, 0, 0, 1, 1, 1);
shape10.setRotationPoint(4, 13, 1);
shape10.setRotationDegrees(0, 0, 0);
shape10.setMirror(true);
var shape11 = new Model(0, 0);
shape11.addBox(0, 0, 0, 1, 1, 1);
shape11.setRotationPoint(4, 13, -2);
shape11.setRotationDegrees(0, 0, 0);
shape11.setMirror(true);
var shape12 = new Model(0, 0);
shape12.addBox(0, 0, 0, 1, 1, 1);
shape12.setRotationPoint(4, 13, -5);
shape12.setRotationDegrees(0, 0, 0);
shape12.setMirror(true);
var shape13 = new Model(0, 0);
shape13.addBox(0, 0, 0, 1, 1, 1);
shape13.setRotationPoint(-5, 13, 1);
shape13.setRotationDegrees(0, 0, 0);
shape13.setMirror(true);
var shape14 = new Model(0, 0);
shape14.addBox(0, 0, 0, 1, 1, 1);
shape14.setRotationPoint(-5, 13, -2);
shape14.setRotationDegrees(0, 0, 0);
shape14.setMirror(true);
var shape15 = new Model(0, 0);
shape15.addBox(0, 0, 0, 1, 1, 1);
shape15.setRotationPoint(-5, 13, -5);
shape15.setRotationDegrees(0, 0, 0);
shape15.setMirror(true);
var shape16 = new Model(0, 0);
shape16.addBox(0, 0, 0, 1, 1, 1);
shape16.setRotationPoint(-2, 13, -5);
shape16.setRotationDegrees(0, 0, 0);
shape16.setMirror(true);
var shape17 = new Model(0, 0);
shape17.addBox(0, 0, 0, 1, 1, 1);
shape17.setRotationPoint(1, 13, -5);
shape17.setRotationDegrees(0, 0, 0);
shape17.setMirror(true);
