base.setScale(1.0);
base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 11, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape1 = new Model(0, 11);
Shape1.addBox(-6.0, -8.0, -6.0, 12, 1, 12);
Shape1.setRotationPoint(0, 0, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(false);
var Shape2 = new Model(0, 0);
Shape2.addBox(-4.0, -11.0, -4.0, 8, 3, 8);
Shape2.setRotationPoint(0, 0, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(false);
