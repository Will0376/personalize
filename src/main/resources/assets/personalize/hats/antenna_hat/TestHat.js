// Date: 29.12.2016 20:31:48
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "TestHattexture.png");

var Shape1 = new Model(0, 28);
Shape1.addBox(0, 0, 0, 8, 3, 1);
Shape1.setRotationPoint(-4, -1, 4);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape2 = new Model(0, 13);
Shape2.addBox(0, 0, 0, 8, 1, 8);
Shape2.setRotationPoint(-4, 0, -4);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(13, 0);
Shape3.addBox(0, 0, 0, 1, 3, 10);
Shape3.setRotationPoint(4, -1, -5);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(41, 0);
Shape4.addBox(0, 0, 0, 1, 3, 10);
Shape4.setRotationPoint(-5, -1, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 23);
Shape5.addBox(0, 0, 0, 8, 3, 1);
Shape5.setRotationPoint(-4, -1, -5);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(-1, 0, -1, 2, 11, 2);
Shape6.setRotationPoint(0, -11, 0);
Shape6.setRotationDegrees(0, 45, 0);
Shape6.setMirror(true);

function animate(time, speed, distance, mc, armorInventory, deobf) {
    var math = net.minecraft.util.math.MathHelper;

    var rotationSpeed = 0.2 + speed * 0.01; // скорость вращения = 0.2 + скорость движения перса * 0.01
    var angle = time * rotationSpeed; // угол (будет постоянно увеличиваться, как и время)
    Shape6.setRotationRadians(0, angle, 0); //вращаем вокруг оси Y
}

