// Date: 13.01.2017 16:53:59
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 0, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(0, 0);
Shape2.addBox(-1, 0, 4, 2, 8, 1);
Shape2.setRotationPoint(0, 0, 0);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 0);
Shape3.addBox(4, 0, -1, 1, 8, 2);
Shape3.setRotationPoint(0, 0, 0);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(-5, 0, -1, 1, 8, 2);
Shape4.setRotationPoint(0, 0, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(-4, 0, 5, 8, 1, 1);
Shape5.setRotationPoint(0, 0, -1);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(5, 0, -4, 1, 1, 8);
Shape6.setRotationPoint(-1, 0, 0);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(-5, 0, -5, 1, 1, 8);
Shape7.setRotationPoint(0, 0, 1);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape8 = new Model(0, 0);
Shape8.addBox(-3, 4, 4, 6, 1, 1);
Shape8.setRotationPoint(0, 0, 0);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);

var Shape9 = new Model(0, 0);
Shape9.addBox(-4, 7, 4, 8, 1, 1);
Shape9.setRotationPoint(0, 0, 0);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);

var Shape10 = new Model(0, 0);
Shape10.addBox(2, 0, 0, 1, 3, 1);
Shape10.setRotationPoint(0, 0, 4);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);

var Shape11 = new Model(0, 0);
Shape11.addBox(-3, 0, 1, 1, 3, 1);
Shape11.setRotationPoint(0, 0, 3);
Shape11.setRotationDegrees(0, 0, 0);
Shape11.setMirror(true);

var Shape10 = new Model(0, 0);
Shape10.addBox(4, 0, 0, 1, 3, 1);
Shape10.setRotationPoint(0, 0, 2);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);

var Shape10 = new Model(0, 0);
Shape10.addBox(4, 0, 0, 1, 3, 1);
Shape10.setRotationPoint(0, 0, -3);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);

var Shape12 = new Model(0, 0);
Shape12.addBox(0, 0, 0, 1, 1, 8);
Shape12.setRotationPoint(4, 7, -4);
Shape12.setRotationDegrees(0, 0, 0);
Shape12.setMirror(true);

var Shape13 = new Model(0, 0);
Shape13.addBox(0, 3, -4, 1, 1, 6);
Shape13.setRotationPoint(4, 1, 1);
Shape13.setRotationDegrees(0, 0, 0);
Shape13.setMirror(true);

var Shape10 = new Model(0, 0);
Shape10.addBox(-5, 0, -3, 1, 3, 1);
Shape10.setRotationPoint(0, 0, 0);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);

var Shape10 = new Model(0, 0);
Shape10.addBox(0, 0, 0, 1, 3, 1);
Shape10.setRotationPoint(-5, 0, 2);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);

var Shape14 = new Model(0, 0);
Shape14.addBox(0, 7, -4, 1, 1, 8);
Shape14.setRotationPoint(-5, 0, 0);
Shape14.setRotationDegrees(0, 0, 0);
Shape14.setMirror(true);

var Shape13 = new Model(0, 0);
Shape13.addBox(-5, 5, -3, 1, 1, 6);
Shape13.setRotationPoint(0, -1, 0);
Shape13.setRotationDegrees(0, 0, 0);
Shape13.setMirror(true);

var Shape15 = new Model(0, 0);
Shape15.addBox(4, 0.5, 4, 1, 7, 1);
Shape15.setRotationPoint(0, 0, 0);
Shape15.setRotationDegrees(0, 0, 0);
Shape15.setMirror(true);

var Shape16 = new Model(0, 0);
Shape16.addBox(0, 0.5, 3, 1, 7, 1);
Shape16.setRotationPoint(-5, 0, 1);
Shape16.setRotationDegrees(0, 0, 0);
Shape16.setMirror(true);

var Shape17 = new Model(0, 0);
Shape17.addBox(-5, 4, 2, 1, 1, 1);
Shape17.setRotationPoint(0, -1, 1);
Shape17.setRotationDegrees(0, 0, 0);
Shape17.setMirror(true);

var Shape18 = new Model(0, 0);
Shape18.addBox(-5, 4, -3, 1, 1, 1);
Shape18.setRotationPoint(0, -1, -1);
Shape18.setRotationDegrees(0, 0, 0);
Shape18.setMirror(true);

var Shape19 = new Model(0, 0);
Shape19.addBox(0, 0, 0, 1, 1, 1);
Shape19.setRotationPoint(-4, 3, 4);
Shape19.setRotationDegrees(0, 0, 0);
Shape19.setMirror(true);

var Shape20 = new Model(0, 0);
Shape20.addBox(0, 0, 0, 1, 1, 1);
Shape20.setRotationPoint(3, 3, 4);
Shape20.setRotationDegrees(0, 0, 0);
Shape20.setMirror(true);

var Shape21 = new Model(0, 0);
Shape21.addBox(0, 0, 0, 1, 1, 1);
Shape21.setRotationPoint(4, 3, 3);
Shape21.setRotationDegrees(0, 0, 0);
Shape21.setMirror(true);

var Shape22 = new Model(0, 0);
Shape22.addBox(0, 0, 0, 1, 1, 1);
Shape22.setRotationPoint(4, 3, -4);
Shape22.setRotationDegrees(0, 0, 0);
Shape22.setMirror(true);

var Shape24 = new Model(0, 0);
Shape24.addBox(-1, 0, 0, 8, 1, 1);
Shape24.setRotationPoint(-3, 0, -5);
Shape24.setRotationDegrees(5.55111512312578E-15, 0, 0);
Shape24.setMirror(true);

var Shape25 = new Model(0, 0);
Shape25.addBox(-1, -0.5, 0, 8, 1, 8);
Shape25.setRotationPoint(-3, 0, -4);
Shape25.setRotationDegrees(0, 0, 0);
Shape25.setMirror(true);

var Shape26 = new Model(0, 0);
Shape26.addBox(0, 0, 0, 1, 1, 1);
Shape26.setRotationPoint(3, 1, -5);
Shape26.setRotationDegrees(0, 0, 0);
Shape26.setMirror(true);

var Shape27 = new Model(0, 0);
Shape27.addBox(0, 0, 0, 1, 2, 1);
Shape27.setRotationPoint(3.5, 2, -5);
Shape27.setRotationDegrees(0, 0, 0);
Shape27.setMirror(true);

var Shape28 = new Model(0, 0);
Shape28.addBox(0, 0, 0, 1, 1, 1);
Shape28.setRotationPoint(3, 3, -5);
Shape28.setRotationDegrees(0, 0, 0);
Shape28.setMirror(true);

var Shape29 = new Model(0, 0);
Shape29.addBox(2, 3.3, -5, 1, 1, 1);
Shape29.setRotationPoint(0, 0, 0);
Shape29.setRotationDegrees(0, 0, 0);
Shape29.setMirror(true);

var Shape30 = new Model(0, 0);
Shape30.addBox(1, 3.3, -5, 1, 1, 1);
Shape30.setRotationPoint(0, 0, 0);
Shape30.setRotationDegrees(0, 0, 0);
Shape30.setMirror(true);

var Shape31 = new Model(0, 0);
Shape31.addBox(-1, 2, 0, 1, 1, 1);
Shape31.setRotationPoint(1, 1, -5);
Shape31.setRotationDegrees(0, 0, 0);
Shape31.setMirror(true);

var Shape32 = new Model(0, 0);
Shape32.addBox(0, -2, 0, 1, 1, 1);
Shape32.setRotationPoint(-4, 3, -5);
Shape32.setRotationDegrees(0, 0, 0);
Shape32.setMirror(true);

var Shape33 = new Model(0, 0);
Shape33.addBox(-11.5, -1, 0, 1, 2, 1);
Shape33.setRotationPoint(7, 3, -5);
Shape33.setRotationDegrees(0, 0, 0);
Shape33.setMirror(true);

var Shape34 = new Model(0, 0);
Shape34.addBox(-4, 3, -5, 1, 1, 1);
Shape34.setRotationPoint(0, 0, 0);
Shape34.setRotationDegrees(0, 0, 0);
Shape34.setMirror(true);

var Shape35 = new Model(0, 0);
Shape35.addBox(-3, 3.3, -5, 1, 1, 1);
Shape35.setRotationPoint(0, 0, 0);
Shape35.setRotationDegrees(0, 0, 0);
Shape35.setMirror(true);

var Shape36 = new Model(0, 0);
Shape36.addBox(-2, 3.3, -5, 1, 1, 1);
Shape36.setRotationPoint(0, 0, 0);
Shape36.setRotationDegrees(0, 0, 0);
Shape36.setMirror(true);

var Shape37 = new Model(0, 0);
Shape37.addBox(-1, 3, -5, 1, 1, 1);
Shape37.setRotationPoint(0, 0, 0);
Shape37.setRotationDegrees(0, 0, 0);
Shape37.setMirror(true);

var Shape38 = new Model(0, 0);
Shape38.addBox(-0.5, 2, -5, 1, 1, 1);
Shape38.setRotationPoint(0, 0, 0);
Shape38.setRotationDegrees(0, 0, 0);
Shape38.setMirror(true);

var Shape39 = new Model(0, 0);
Shape39.addBox(-0.5, 1, -5, 1, 1, 1);
Shape39.setRotationPoint(0, 0, 0);
Shape39.setRotationDegrees(0, 0, 0);
Shape39.setMirror(true);

var Shape40 = new Model(0, 0);
Shape40.addBox(0, 0.5, -5, 1, 1, 1);
Shape40.setRotationPoint(0, 0, 0);
Shape40.setRotationDegrees(0, 0, 0);
Shape40.setMirror(true);

var Shape41 = new Model(0, 0);
Shape41.addBox(-1, 1.46666666666667, -5, 1, 1, 1);
Shape41.setRotationPoint(0, -1, 0);
Shape41.setRotationDegrees(0, 0, 0);
Shape41.setMirror(true);

var Shape42 = new Model(0, 0);
Shape42.addBox(-4, 6, -5, 1, 1, 1);
Shape42.setRotationPoint(0, 0, 0);
Shape42.setRotationDegrees(0, 0, 0);
Shape42.setMirror(true);

var Shape43 = new Model(0, 0);
Shape43.addBox(3, 6, -5, 1, 1, 1);
Shape43.setRotationPoint(0, 0, 0);
Shape43.setRotationDegrees(0, 0, 0);
Shape43.setMirror(true);

var Shape44 = new Model(0, 0);
Shape44.addBox(2, 6.4, -5, 1, 1, 1);
Shape44.setRotationPoint(0, 0, 0);
Shape44.setRotationDegrees(0, 0, 0);
Shape44.setMirror(true);

var Shape45 = new Model(0, 0);
Shape45.addBox(-3, 6.4, -5, 1, 1, 1);
Shape45.setRotationPoint(0, 0, 0);
Shape45.setRotationDegrees(0, 0, 0);
Shape45.setMirror(true);

var Shape46 = new Model(0, 0);
Shape46.addBox(0.6, 6.8, -5, 2, 1, 1);
Shape46.setRotationPoint(0, 0, 0);
Shape46.setRotationDegrees(0, 0, 0);
Shape46.setMirror(true);

var Shape47 = new Model(0, 0);
Shape47.addBox(-1, 7, -5, 2, 1, 1);
Shape47.setRotationPoint(0, 0, 0);
Shape47.setRotationDegrees(0, 0, 0);
Shape47.setMirror(true);

var Shape49 = new Model(0, 0);
Shape49.addBox(-2.6, 6.8, -5, 2, 1, 1);
Shape49.setRotationPoint(0, 0, 0);
Shape49.setRotationDegrees(0, 0, 0);
Shape49.setMirror(true);

var Shape23 = new Model(0, 0);
Shape23.addBox(3.7, 4, -5, 1, 3, 1);
Shape23.setRotationPoint(0, 0, 0);
Shape23.setRotationDegrees(0, 0, 0);
Shape23.setMirror(true);

var Shape48 = new Model(0, 0);
Shape48.addBox(-4.7, 4, -5, 1, 3, 1);
Shape48.setRotationPoint(0, 0, 0);
Shape48.setRotationDegrees(0, 0, 0);
Shape48.setMirror(true);

var Shape50 = new Model(0, 0);
Shape50.addBox(-0.3, 1, 0, 1, 2, 1);
Shape50.setRotationPoint(4, 0, -5);
Shape50.setRotationDegrees(0, 0, 0);
Shape50.setMirror(true);

var Shape51 = new Model(0, 0);
Shape51.addBox(-4.7, 1, -5, 1, 2, 1);
Shape51.setRotationPoint(0, 0, 0);
Shape51.setRotationDegrees(0, 0, 0);
Shape51.setMirror(true);

var Shape52 = new Model(0, 0);
Shape52.addBox(-4, 7, -5, 1, 1, 1);
Shape52.setRotationPoint(0, 0, 0);
Shape52.setRotationDegrees(0, 0, 0);
Shape52.setMirror(true);

var Shape53 = new Model(0, 0);
Shape53.addBox(3, 7, -5, 1, 1, 1);
Shape53.setRotationPoint(0, 0, 0);
Shape53.setRotationDegrees(0, 0, 0);
Shape53.setMirror(true);

var Shape54 = new Model(47, 0);
Shape54.addBox(0, 4, -4.5, 1, 3, 1);
Shape54.setRotationPoint(1, 0, 0);
Shape54.setRotationDegrees(0, 0, 0);
Shape54.setMirror(true);

var Shape55 = new Model(59, 0);
Shape55.addBox(2.5, 4, -4.5, 1, 3, 1);
Shape55.setRotationPoint(0, 0, 0);
Shape55.setRotationDegrees(0, 0, 0);
Shape55.setMirror(true);

var Shape56 = new Model(45, 0);
Shape56.addBox(-2, 4, -4.5, 1, 3, 1);
Shape56.setRotationPoint(0, 0, 0);
Shape56.setRotationDegrees(0, 0, 0);
Shape56.setMirror(true);

var Shape57 = new Model(56, 0);
Shape57.addBox(-3.5, 4, -4.5, 1, 3, 1);
Shape57.setRotationPoint(0, 0, 0);
Shape57.setRotationDegrees(0, 0, 0);
Shape57.setMirror(true);

var Shape58 = new Model(51, 0);
Shape58.addBox(-0.5, 4, -4.5, 1, 3, 1);
Shape58.setRotationPoint(0, 0, 0);
Shape58.setRotationDegrees(0, 0, 0);
Shape58.setMirror(true);

var Shape59 = new Model(45, 20);
Shape59.addBox(-4, 0.5, -4.1, 8, 7, 0);
Shape59.setRotationPoint(0, 0, 0);
Shape59.setRotationDegrees(0, 0, 0);
Shape59.setMirror(true);

var Shape60 = new Model(0, 0);
Shape60.addBox(-0.5, 7.3, -5, 1, 1, 1);
Shape60.setRotationPoint(0, 0, 0);
Shape60.setRotationDegrees(0, 0, 0);
Shape60.setMirror(true);

var Shape1 = new Model(50, 8);
Shape1.addBox(0, 0, -4.4, 1, 1, 1);
Shape1.setRotationPoint(1.5, 1.5, 0);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape61 = new Model(46, 5);
Shape61.addBox(-2.5, 1.7, -4.5, 1, 1, 1);
Shape61.setRotationPoint(0, 0, 0);
Shape61.setRotationDegrees(0, 0, 0);
Shape61.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
