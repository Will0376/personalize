base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape10 = new Model(0, 16);
Shape10.addBox(4, 18, -8, 2, 3, 8);
Shape10.setRotationPoint(0, 0, 0);
Shape10.setRotationDegrees(0, 0, 0);
Shape10.setMirror(true);
var Shape11 = new Model(0, 0);
Shape11.addBox(-4, 5, -13, 8, 8, 8);
Shape11.setRotationPoint(0, 11, 0);
Shape11.setRotationDegrees(0, 0, 0);
Shape11.setMirror(true);
var Shape12 = new Model(0, 16);
Shape12.addBox(-6, 18, -8, 2, 3, 8);
Shape12.setRotationPoint(0, 0, 0);
Shape12.setRotationDegrees(0, 0, 0);
Shape12.setMirror(true);
