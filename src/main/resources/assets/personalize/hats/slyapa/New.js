// Date: 06.01.2017 20:36:27
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 2, 0);
base.setOffsetCorrection(0, 0, 0);

var shlyap1 = new Model(0, 19);
shlyap1.addBox(-4, -2, -5, 6, 3, 6);
shlyap1.setRotationPoint(1, -3, 2);
shlyap1.setRotationDegrees(0, 0, 0);
shlyap1.setMirror(true);

var Shape1 = new Model(0, 14);
Shape1.addBox(0, 0, 0, 10, 2, 1);
Shape1.setRotationPoint(-5, -1, -5);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape2 = new Model(40, 8);
Shape2.addBox(0, 0, 0, 1, 2, 8);
Shape2.setRotationPoint(4, -1, -4);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape1 = new Model(40, 4);
Shape1.addBox(0, 0, 0, 10, 2, 1);
Shape1.setRotationPoint(-5, -1, 4);
Shape1.setRotationDegrees(0, 0, 0);
Shape1.setMirror(true);

var Shape2 = new Model(21, 3);
Shape2.addBox(0, 0, 0, 1, 2, 8);
Shape2.setRotationPoint(-5, -1, -4);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(26, 23);
Shape3.addBox(0, 0, 0, 8, 1, 8);
Shape3.setRotationPoint(-4, -2, -4);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
