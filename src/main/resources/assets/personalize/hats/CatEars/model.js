base.setTexture(64, 32, "Hattexture.png");
base.setPositionCorrection(0, 10, 0);
base.setOffsetCorrection(0, 0, 0);

var leftEarBottom = new Model(0, 0);
leftEarBottom.addBox(0, 0, 0, 1, 1, 1);
leftEarBottom.setRotationPoint(4, -8, 0);
leftEarBottom.setMirror(true);

var leftEarRearTop = new Model(0, 16);
leftEarRearTop.addBox(0, -3, 1, 1, 1, 1);
leftEarRearTop.setRotationPoint(4, -8, 0);
leftEarRearTop.setMirror(true);

var leftEarRearLayer1 = new Model(0, 14);
leftEarRearLayer1.addBox(-1, -2, 1, 2, 1, 1);
leftEarRearLayer1.setRotationPoint(4, -8, 0);
leftEarRearLayer1.setMirror(true);

var leftEarRearBottom = new Model(0, 12);
leftEarRearBottom.addBox(-2, -1, 1, 3, 1, 1);
leftEarRearBottom.setRotationPoint(4, -8, 0);
leftEarRearBottom.setMirror(true);

var leftEarLayer1 = new Model(0, 2);
leftEarLayer1.addBox(-3, -1, 0, 5, 1, 1);
leftEarLayer1.setRotationPoint(4, -8, 0);
leftEarLayer1.setMirror(true);

var leftEarTop = new Model(0, 8);
leftEarTop.addBox(0, -4, 0, 1, 1, 1);
leftEarTop.setRotationPoint(4, -8, 0);
leftEarTop.setMirror(true);

var leftEarLayer3 = new Model(0, 6);
leftEarLayer3.addBox(-1, -3, 0, 3, 1, 1);
leftEarLayer3.setRotationPoint(4, -8, 0);
leftEarLayer3.setMirror(true);

var leftEarLayer2 = new Model(0, 4);
leftEarLayer2.addBox(-2, -2, 0, 4, 1, 1);
leftEarLayer2.setRotationPoint(4, -8, 0);
leftEarLayer2.setMirror(true);

var rightEarBottom = new Model(13, 0);
rightEarBottom.addBox(-1, 0, 0, 1, 1, 1);
rightEarBottom.setRotationPoint(-4, -8, 0);
rightEarBottom.setMirror(true);

var rightEarLayer1 = new Model(13, 2);
rightEarLayer1.addBox(-2, -1, 0, 5, 1, 1);
rightEarLayer1.setRotationPoint(-4, -8, 0);
rightEarLayer1.setMirror(true);

var rightEarRearTop = new Model(13, 16);
rightEarRearTop.addBox(-1, -3, 1, 1, 1, 1);
rightEarRearTop.setRotationPoint(-4, -8, 0);
rightEarRearTop.setMirror(true);

var rightEarRearLayer1 = new Model(13, 14);
rightEarRearLayer1.addBox(-1, -2, 1, 2, 1, 1);
rightEarRearLayer1.setRotationPoint(-4, -8, 0);
rightEarRearLayer1.setMirror(true);

var rightEarRearBottom = new Model(13, 12);
rightEarRearBottom.addBox(-1, -1, 1, 3, 1, 1);
rightEarRearBottom.setRotationPoint(-4, -8, 0);
rightEarRearBottom.setMirror(true);

var rightEarLayer2 = new Model(13, 4);
rightEarLayer2.addBox(-2, -2, 0, 4, 1, 1);
rightEarLayer2.setRotationPoint(-4, -8, 0);
rightEarLayer2.setMirror(true);

var rightEarTop = new Model(13, 8);
rightEarTop.addBox(-1, -4, 0, 1, 1, 1);
rightEarTop.setRotationPoint(-4, -8, 0);
rightEarTop.setMirror(true);

var rightEarLayer3 = new Model(13, 6);
rightEarLayer3.addBox(-2, -3, 0, 3, 1, 1);
rightEarLayer3.setRotationPoint(-4, -8, 0);
rightEarLayer3.setMirror(true);
