base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -13, 0);
base.setOffsetCorrection(0, 0, 0);

var shape1 = new Model(0, 0);
shape1.addBox(-8, 5, -8, 16, 16, 16);
shape1.setRotationPoint(0, 0, 0);
shape1.setRotationDegrees(0, 0, 0);
shape1.setMirror(true);
