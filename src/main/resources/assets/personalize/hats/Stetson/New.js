// Date: 06.01.2017 1:50:39
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexsture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var HatBase = new Model(7, 18);
HatBase.addBox(0, 0, 0, 12, 1, 12);
HatBase.setRotationPoint(-6, 17, -6);
HatBase.setRotationDegrees(0, 0, 0);
HatBase.setMirror(true);

var HatTop = new Model(11, 2);
HatTop.addBox(0, 0, 0, 10, 1, 10);
HatTop.setRotationPoint(-5, 16, -5);
HatTop.setRotationDegrees(0, 0, 0);
HatTop.setMirror(true);

var HatTopR = new Model(21, 19);
HatTopR.addBox(0, -1, 0, 5, 3, 9);
HatTopR.setRotationPoint(-4, 14.1, -4.5);
HatTopR.setRotationDegrees(-3.01980662698043E-14, 0, 15);
HatTopR.setMirror(true);

var HatBrimL = new Model(4, 17);
HatBrimL.addBox(0, 0, 0, 1, 2, 12);
HatBrimL.setRotationPoint(6.7, 15.8, -6);
HatBrimL.setRotationDegrees(0, 0, 45);
HatBrimL.setMirror(false);

var HatBrimR = new Model(7, 17);
HatBrimR.addBox(0, 0, 0, 1, 2, 12);
HatBrimR.setRotationPoint(-7.4, 16.5, -6);
HatBrimR.setRotationDegrees(0, 0, -45);
HatBrimR.setMirror(true);

var HatTopL = new Model(5, 17);
HatTopL.addBox(-4, -1, 0, 5, 3, 9);
HatTopL.setRotationPoint(3, 14.3, -4.5);
HatTopL.setRotationDegrees(-3.01980662698043E-14, 0, -15);
HatTopL.setMirror(false);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
