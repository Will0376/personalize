base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, -15, 0);
base.setOffsetCorrection(0, 0, 0);

var shape1 = new Model(36, 20);
shape1.addBox(0.5, -18, -2.5, 4, 8, 4);
shape1.setRotationPoint(0, 24, 0);
shape1.setRotationDegrees(0, 0, 0);
shape1.setMirror(true);
var shape2 = new Model(0, 15);
shape2.addBox(-4.5, -10, -4.5, 9, 8, 9);
shape2.setRotationPoint(0, 24, 0);
shape2.setRotationDegrees(0, 0, 0);
shape2.setMirror(true);
var shape3 = new Model(36, 20);
shape3.addBox(-4.5, -18, -2.5, 4, 8, 4);
shape3.setRotationPoint(0, 24, 0);
shape3.setRotationDegrees(0, 0, 0);
shape3.setMirror(true);
