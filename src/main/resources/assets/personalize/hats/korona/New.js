// Date: 06.01.2017 22:51:21
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");
base.setPositionCorrection(0, 2, 0);
base.setOffsetCorrection(0, 0, 0);

var Shape2 = new Model(0, 0);
Shape2.addBox(-2, 0, 0, 10, 1, 1);
Shape2.setRotationPoint(-3, 0, -5);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape2 = new Model(0, 0);
Shape2.addBox(0, 0, 0, 10, 1, 1);
Shape2.setRotationPoint(-5, 0, 4);
Shape2.setRotationDegrees(0, 0, 0);
Shape2.setMirror(true);

var Shape3 = new Model(0, 0);
Shape3.addBox(0, 0, 0, 1, 1, 8);
Shape3.setRotationPoint(-5, 0, -4);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape3 = new Model(0, 0);
Shape3.addBox(0, 0, 0, 1, 1, 8);
Shape3.setRotationPoint(4, 0, -4);
Shape3.setRotationDegrees(0, 0, 0);
Shape3.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 5, 1);
Shape4.setRotationPoint(-5, -5, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(-4, -4, -5);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(-3, -1, 0, 1, 3, 1);
Shape6.setRotationPoint(0, -2, -5);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(0, 0, 0, 1, 5, 1);
Shape7.setRotationPoint(-2, -5, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(0, 0, 0, 1, 6, 1);
Shape7.setRotationPoint(-1, -6, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(0, 0, 0, 1, 6, 1);
Shape7.setRotationPoint(0, -6, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape7 = new Model(0, 0);
Shape7.addBox(0, 0, 0, 1, 5, 1);
Shape7.setRotationPoint(1, -5, -5);
Shape7.setRotationDegrees(0, 0, 0);
Shape7.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(2, -3, 4);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(3, -4, -5);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 5, 1);
Shape4.setRotationPoint(4, -5, -5);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(4, -4, -4);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(4, -3, -3);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(-5, -4, -4);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(-5, -3, -3);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(4, -3, -2);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(-5, -3, -2);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(4, -4, -1);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(4, -4, 0);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 3, 1);
Shape5.setRotationPoint(4, -3, 1);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 3, 1);
Shape5.setRotationPoint(4, -3, 2);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(4, -4, 3);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 5, 1);
Shape4.setRotationPoint(4, -5, 4);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 4, 1);
Shape4.setRotationPoint(-5, -4, -1);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 4, 1);
Shape4.setRotationPoint(-5, -4, 0);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 3, 1);
Shape5.setRotationPoint(-5, -3, 1);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 3, 1);
Shape5.setRotationPoint(-5, -3, 2);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(-5, -4, 3);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape4 = new Model(0, 0);
Shape4.addBox(0, 0, 0, 1, 5, 1);
Shape4.setRotationPoint(-5, -5, 4);
Shape4.setRotationDegrees(0, 0, 0);
Shape4.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(-4, -4, 4);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape5 = new Model(0, 0);
Shape5.addBox(0, 0, 0, 1, 4, 1);
Shape5.setRotationPoint(3, -4, 4);
Shape5.setRotationDegrees(0, 0, 0);
Shape5.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(-3, -3, 4);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(1, -3, 4);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(-2, -3, 4);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(2, -3, -5);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 4, 1);
Shape6.setRotationPoint(0, -4, 4.01333333333333);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 4, 1);
Shape6.setRotationPoint(-1, -4, 4);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape6 = new Model(0, 0);
Shape6.addBox(0, 0, 0, 1, 3, 1);
Shape6.setRotationPoint(2, -3, -5);
Shape6.setRotationDegrees(0, 0, 0);
Shape6.setMirror(true);

var Shape8 = new Model(0, 28);
Shape8.addBox(0, 0, 0, 2, 3, 1);
Shape8.setRotationPoint(-1, -4, -6);
Shape8.setRotationDegrees(0, 0, 0);
Shape8.setMirror(true);

var Shape9 = new Model(0, 20);
Shape9.addBox(0, 0, 0, 1, 2, 1);
Shape9.setRotationPoint(3, -2, -6);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);

var Shape9 = new Model(0, 24);
Shape9.addBox(0, 0, 0, 1, 2, 1);
Shape9.setRotationPoint(-4, -2, -6);
Shape9.setRotationDegrees(0, 0, 0);
Shape9.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
