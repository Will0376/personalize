// Date: 29.12.2016 20:32:44
// Personalize model JS exported from Techne - Denr01 Edition

base.setTexture(64, 32, "Newtexture.png");

var top1 = new Model(0, 6);
top1.addBox(-4, 0, -4, 3, 2, 2);
top1.setRotationPoint(5.5, 0.5, -2);
top1.setRotationDegrees(0, 0, 0);
top1.setMirror(true);

var top2 = new Model(0, 16);
top2.addBox(-4, 0, -4, 8, 1, 8);
top2.setRotationPoint(0, 0, 0);
top2.setRotationDegrees(0, 0, 0);
top2.setMirror(true);

var top3 = new Model(5, 13);
top3.addBox(-4, 0, -4, 1, 2, 8);
top3.setRotationPoint(7, 1, 0);
top3.setRotationDegrees(0, 0, 0);
top3.setMirror(true);

var top4 = new Model(0, 6);
top4.addBox(-4, 0, -4, 8, 3, 2);
top4.setRotationPoint(0, -0.5, -2);
top4.setRotationDegrees(0, 0, 0);
top4.setMirror(true);

var top5 = new Model(0, 6);
top5.addBox(-4, 0, -4, 3, 2, 2);
top5.setRotationPoint(-0.5, 0.5, -2);
top5.setRotationDegrees(0, 0, 0);
top5.setMirror(true);

var top6 = new Model(7, 23);
top6.addBox(-4, 0, -4, 8, 3, 1);
top6.setRotationPoint(0, 1, 8);
top6.setRotationDegrees(0, 0, 0);
top6.setMirror(true);

var top7 = new Model(5, 16);
top7.addBox(-4, 0, -4, 1, 2, 8);
top7.setRotationPoint(0, 1, 0);
top7.setRotationDegrees(0, 0, 0);
top7.setMirror(true);

var top8 = new Model(0, 0);
top8.addBox(-4, 0, -4, 2, 3, 6);
top8.setRotationPoint(8, 1.5, 1);
top8.setRotationDegrees(0, 0, 0);
top8.setMirror(true);

var top9 = new Model(6, 6);
top9.addBox(-4, 0, -4, 2, 1, 1);
top9.setRotationPoint(8, 4.5, 5);
top9.setRotationDegrees(0, 0, 0);
top9.setMirror(true);

var top10 = new Model(6, 6);
top10.addBox(-4, 0, -4, 2, 2, 3);
top10.setRotationPoint(8, 4.5, 2.5);
top10.setRotationDegrees(0, 0, 0);
top10.setMirror(true);

var top11 = new Model(6, 6);
top11.addBox(-4, 0, -4, 2, 1, 1);
top11.setRotationPoint(8, 4.5, 2);
top11.setRotationDegrees(0, 0, 0);
top11.setMirror(true);

var top12 = new Model(0, 0);
top12.addBox(-4, 0, -4, 2, 3, 6);
top12.setRotationPoint(-2, 1.5, 1);
top12.setRotationDegrees(0, 0, 0);
top12.setMirror(true);

var top13 = new Model(6, 6);
top13.addBox(-4, 0, -4, 2, 1, 1);
top13.setRotationPoint(-2, 4.5, 2);
top13.setRotationDegrees(0, 0, 0);
top13.setMirror(true);

var top14 = new Model(6, 6);
top14.addBox(-4, 0, -4, 2, 2, 3);
top14.setRotationPoint(-2, 4.5, 2.5);
top14.setRotationDegrees(0, 0, 0);
top14.setMirror(true);

var top15 = new Model(6, 6);
top15.addBox(-4, 0, -4, 2, 1, 1);
top15.setRotationPoint(-2, 4.5, 5);
top15.setRotationDegrees(0, 0, 0);
top15.setMirror(true);

var top16 = new Model(7, 23);
top16.addBox(-4, 0, -4, 8, 3, 1);
top16.setRotationPoint(0, 1, 7);
top16.setRotationDegrees(0, 0, 0);
top16.setMirror(true);

/*
function animate(time, speed, distance, mc, armorInventory, deobf) {     var math = net.minecraft.util.math.MathHelper;

}
*/
