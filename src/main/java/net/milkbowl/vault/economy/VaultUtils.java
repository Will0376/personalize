package net.milkbowl.vault.economy;

import com.google.common.base.Preconditions;
import net.minecraft.entity.player.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;


@GradleSideOnly(GradleSide.SERVER)
public final class VaultUtils {
	private static Economy economy;

	public static boolean depositPlayer(EntityPlayer player, double amount) {
		Player bukkitPlayer = getBukkitPlayer(player);
		if (bukkitPlayer != null) {
			Economy economy = getEconomy();
			return economy.depositPlayer(bukkitPlayer, amount).transactionSuccess();
		} else {
			return false;
		}
	}

	public static boolean withdrawPlayer(EntityPlayer player, double amount) {
		Player bukkitPlayer = getBukkitPlayer(player);
		if (bukkitPlayer != null) {
			Economy economy = getEconomy();
			return economy.withdrawPlayer(bukkitPlayer, amount).transactionSuccess();
		} else {
			return false;
		}
	}

	public static Economy getEconomy() {
		if (economy == null) {
			RegisteredServiceProvider economyProvider = Bukkit.getServicesManager().getRegistration(Economy.class);
			if (economyProvider != null) {
				economy = (Economy) economyProvider.getProvider();
			}
		}

		return Preconditions.checkNotNull(economy, "VaultEconomy not found!");
	}

	public static Player getBukkitPlayer(EntityPlayer player) {
		return Bukkit.getPlayer(player.getUniqueID());
	}
}
