package net.milkbowl.vault.economy;


import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;


@GradleSideOnly(GradleSide.SERVER)
public class EconomyResponse {
	public final double amount;
	public final double balance;
	public final ResponseType type;
	public final String errorMessage;

	public EconomyResponse(double amount, double balance, ResponseType type, String errorMessage) {
		this.amount = amount;
		this.balance = balance;
		this.type = type;
		this.errorMessage = errorMessage;
	}

	public boolean transactionSuccess() {
		switch (this.type) {
			case SUCCESS:
				return true;
			default:
				return false;
		}
	}

	public enum ResponseType {
		SUCCESS(1),
		FAILURE(2),
		NOT_IMPLEMENTED(3);

		private final int id;

		ResponseType(int id) {
			this.id = id;
		}

		int getId() {
			return this.id;
		}
	}
}
