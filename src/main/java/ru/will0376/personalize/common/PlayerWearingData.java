package ru.will0376.personalize.common;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

//оба
public class PlayerWearingData implements Serializable {
	public String PlayerName;
	public Set<String> Wearing;

	public static PlayerWearingData createEmpty(String playerName) {
		PlayerWearingData data = new PlayerWearingData();
		data.PlayerName = playerName;
		data.Wearing = new HashSet<>();
		return data;
	}

	public boolean isWearing(String modelID) {
		return this.Wearing.contains(modelID);
	}

	public boolean equals(Object obj) {
		return obj instanceof PlayerWearingData && ((PlayerWearingData) obj).PlayerName.equals(this.PlayerName);
	}

}
