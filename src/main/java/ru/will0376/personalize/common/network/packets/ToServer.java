package ru.will0376.personalize.common.network.packets;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.personalize.Personalize;

//оба
public class ToServer implements IMessageHandler<ToServer, IMessage>, IMessage {
	PacketType type;
	String string;

	public ToServer() {
	}

	public ToServer(String text, PacketType type) {
		try {
			this.type = type;
			switch (type) {
				case BUY_MODEL:
				case PUT_ON_MODEL:
				case REQUEST_PLAYER_DATA:
					string = text;
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		int tmp = buf.readInt();
		type = PacketType.values()[tmp];
		string = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(type.ordinal());
		ByteBufUtils.writeUTF8String(buf, string);
	}

	@Override
	public IMessage onMessage(ToServer message, MessageContext ctx) {
		if (FMLCommonHandler.instance().getSide().isServer()) Invoke.server(() -> logic(message, ctx));
		return null;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public void logic(ToServer message, MessageContext ctx) {
		try {
			if (message.type == PacketType.BUY_MODEL) {
				if (Personalize.debug)
					System.out.println(String.format("player %s trying buy model: %s", ctx.getServerHandler().player.getName(), message.string));
				Personalize.get().buyModel(ctx.getServerHandler().player, message.string);
			} else if (message.type == PacketType.PUT_ON_MODEL) {
				Personalize.get().putOnModel(ctx.getServerHandler().player, message.string);
			} else if (message.type == PacketType.REQUEST_PLAYER_DATA) {
				EntityPlayerMP subject = (EntityPlayerMP) FMLCommonHandler.instance()
						.getMinecraftServerInstance().getEntityWorld().getPlayerEntityByName(message.string);
				Personalize.get().sendOtherWearing(ctx.getServerHandler().player, subject);
			} else {
				System.err.println("Invalid packet with type:" + type + "\nand string: " + message.string + "\nby: " + ctx.getServerHandler().player.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
