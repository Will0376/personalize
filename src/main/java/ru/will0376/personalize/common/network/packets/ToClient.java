package ru.will0376.personalize.common.network.packets;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.will0376.personalize.client.ModelManager;
import ru.will0376.personalize.common.ModelData;
import ru.will0376.personalize.common.ModelType;
import ru.will0376.personalize.common.PlayerWearingData;

import java.util.Arrays;
import java.util.HashSet;

//оба
public class ToClient implements IMessageHandler<ToClient, IMessage>, IMessage {
	PacketType type;
	ModelData modelData;
	String bought;
	PlayerWearingData wearingData;

	public ToClient() {
	}

	public ToClient(Object obj, PacketType type) {
		try {
			this.type = type;
			switch (type) {
				case MODEL_SET: {//ModelSet
					modelData = (ModelData) obj;
				}
				break;
				case PLAYER_BOUGHT: {//PlayerBought
					bought = (String) obj;
				}
				break;
				case PLAYER_WEARING: {//PlayerWearing
					wearingData = (PlayerWearingData) obj;
				}
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeVarShort(buf, type.ordinal());
		if (type == PacketType.MODEL_SET) {
			ByteBufUtils.writeUTF8String(buf, modelData.getID());
			ByteBufUtils.writeUTF8String(buf, modelData.getName() == null ? "" : modelData.getName());
			buf.writeLong(modelData.getPrice());
			ByteBufUtils.writeUTF8String(buf, modelData.getPermission() == null ? "" : modelData.getPermission());
			ByteBufUtils.writeUTF8String(buf, modelData.getScriptLocation() == null ? "" : modelData.getScriptLocation());
			ByteBufUtils.writeUTF8String(buf, modelData.getDescription() == null ? "" : modelData.getDescription());
			buf.writeInt(modelData.getType().ordinal());
		} else if (type == PacketType.PLAYER_BOUGHT) {
			ByteBufUtils.writeUTF8String(buf, bought);
		} else if (type == PacketType.PLAYER_WEARING) {
			ByteBufUtils.writeUTF8String(buf, wearingData.PlayerName);
			ByteBufUtils.writeUTF8String(buf, String.join("|", wearingData.Wearing));
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		type = PacketType.values()[ByteBufUtils.readVarShort(buf)];
		if (type == PacketType.MODEL_SET) {
			modelData = new ModelData(
					ByteBufUtils.readUTF8String(buf),
					ByteBufUtils.readUTF8String(buf),
					buf.readLong(),
					ByteBufUtils.readUTF8String(buf),
					ByteBufUtils.readUTF8String(buf),
					ByteBufUtils.readUTF8String(buf),
					ModelType.values()[buf.readInt()]
			);
		} else if (type == PacketType.PLAYER_BOUGHT) {
			bought = ByteBufUtils.readUTF8String(buf);
		} else if (type == PacketType.PLAYER_WEARING) {
			wearingData = new PlayerWearingData();
			wearingData.PlayerName = ByteBufUtils.readUTF8String(buf);

			wearingData.Wearing = new HashSet<>(Arrays.asList(ByteBufUtils.readUTF8String(buf).split("\\|")));
		}
	}


	@Override
	public IMessage onMessage(ToClient message, MessageContext ctx) {
		try {
			switch (message.type) {
				case MODEL_SET:
					ModelManager.get().updateModel(message.modelData);
					break;
				case MODEL_RESET:
					ModelManager.get().resetModels();
					break;
				case PLAYER_BOUGHT:
					ModelManager.get().addBought(message.bought);
					break;
				case PLAYER_RESET_BOUGHT:
					ModelManager.get().resetBought();
					break;
				case PLAYER_WEARING:
					ModelManager.get().addWearingData(message.wearingData);
					break;
				case PLAYER_RESET_WEARING:
					ModelManager.get().resetWearingData();
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}