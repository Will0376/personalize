package ru.will0376.personalize.common;

import java.io.Serializable;

//оба
public class ModelData implements Serializable {
	public String ID;
	public String Name;
	public long Price;
	public String Permission;
	public String ScriptLocation;
	public String Description;
	public ModelType Type;

	public ModelData(String ID, String name, long price, String permission, String scriptLocation, String description, ModelType type) {
		this.ID = ID;
		this.Name = name == null ? "" : name;
		this.Price = price;
		this.Permission = permission == null ? "" : permission;
		this.ScriptLocation = scriptLocation;
		this.Description = description;
		this.Type = type;
	}

	public boolean equals(Object obj) {
		return obj instanceof ModelData && ((ModelData) obj).ID.equals(this.ID);
	}

	public String getID() {
		return ID;
	}

	public String getName() {
		return Name;
	}

	public long getPrice() {
		return Price;
	}

	public String getPermission() {
		return Permission;
	}

	public String getScriptLocation() {
		return ScriptLocation;
	}

	public String getDescription() {
		return Description;
	}

	public ModelType getType() {
		return Type;
	}

	public void setType(ModelType type) {
		Type = type;
	}
}
