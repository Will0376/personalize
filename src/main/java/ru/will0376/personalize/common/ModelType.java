package ru.will0376.personalize.common;

import java.io.Serializable;

//клиент
public enum ModelType implements Serializable {
	Hat("Hat"),
	Face("Face"),
	Back("Back"),
	Pet("Pet");

	ModelType(String s) {
	}

	public static ModelType getByString(String s) {
		for (ModelType value : ModelType.values()) {
			if (value.name().equals(s)) return value;
		}
		return null;
	}
}
