package ru.will0376.personalize.server;

import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.util.HashSet;
import java.util.Set;


@GradleSideOnly(GradleSide.SERVER)
public class PlayerModelData {
	public Set<String> Bought = new HashSet<>();
	public Set<String> Wearing = new HashSet<>();
}
