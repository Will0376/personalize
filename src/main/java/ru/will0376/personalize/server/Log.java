package ru.will0376.personalize.server;

import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;


@GradleSideOnly(GradleSide.SERVER)
public class Log {
	public static void print(String color, String prefix, String message, Object... args) {
		for (int i = 0; i < args.length; ++i) {
			args[i] = color + args[i] + color;
		}

		System.out.println(String.format("%s[PERSONALIZE/%s] %s", color, prefix, String.format(message, args)));
	}

	public static void info(String message, Object... args) {
		print("\u00a79", "INFO", message, args);
	}

	public static void warning(String message, Object... args) {
		print("\u00a7e", "WARNING", message, args);
	}

	public static void error(String message, Object... args) {
		print("\u00a74", "ERROR", message, args);
	}

	public static void error(Throwable t) {
		error(t.getMessage());
	}
}
