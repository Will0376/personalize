package ru.will0376.personalize.server;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.personalize.Personalize;

import javax.annotation.Nullable;
import java.util.List;


@GradleSideOnly(GradleSide.SERVER)
public class PersonalizationCommand extends CommandBase {

	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return sender instanceof EntityPlayer && (server.getServer().isSinglePlayer() || sender.canUseCommand(4, "personalize.cmd"));
	}

	@Override
	public String getName() {
		return "personalize";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/personalize - Personalize`s Commands";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length != 0 && args[0].equalsIgnoreCase("reload")) {
			if (args[0].equals("reload")) {
				Personalize.get().managerServer.LoadModels();
				sender.sendMessage(new TextComponentString("Модели перезагружены!"));
			}
		} else {
			sender.sendMessage(new TextComponentString("Use /personalize reload"));
		}
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		return getListOfStringsMatchingLastWord(args, "personalize");
	}
}
