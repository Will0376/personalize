package ru.will0376.personalize.server;

import net.minecraft.entity.player.EntityPlayerMP;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.personalize.Personalize;
import ru.will0376.personalize.common.ModelData;
import ru.will0376.personalize.common.ModelType;

import java.io.File;
import java.util.*;



@GradleSideOnly(GradleSide.SERVER)
public class ModelManagerServer {
	private final Personalize personalize;
	private final FileManager fileManager;
	private final FileManager playersFolder;
	private final Set<ModelData> models = new HashSet<>();
	private final Map<String, PlayerModelData> map = new HashMap<>();

	public ModelManagerServer(Personalize owner, FileManager rootFolder) {
		this.personalize = owner;
		this.fileManager = rootFolder;
		this.playersFolder = new FileManager(new File(rootFolder.getFolder(), "players"));
	}

	public void LoadModels() {
		this.models.clear();
		File modelsFolder = new File(this.fileManager.getFolder(), "models");
		ModelType[] values;
		int length = (values = ModelType.values()).length;

		for (int i = 0; i < length; ++i) {
			ModelType type = values[i];
			File typeFolder = new File(modelsFolder, type.toString());
			if (typeFolder.exists()) {
				File[] listFiles;
				int length2 = (listFiles = typeFolder.listFiles()).length;

				for (int j = 0; j < length2; ++j) {
					File file = listFiles[j];

					try {
						if (file.getName().endsWith(".json")) {
							ModelData modelData = (ModelData) this.fileManager.ReadFile(ModelData.class, file);
							//modelData.getType() = type;
							modelData.setType(type);
							this.models.add(modelData);
							Log.info("Model loaded successfully: %s/%s", modelData.getType(), modelData.getID());
						}
					} catch (Exception var12) {
						Log.error("Error reading model file: %s", file.getName());
						Log.error(var12);
					}
				}
			} else {
				typeFolder.mkdirs();
			}
		}

	}

	public PlayerModelData ReloadPlayerData(final EntityPlayerMP ply) {
		PlayerModelData data;
		try {
			Log.info("Loading player %s model data...", ply.getName());
			data = (PlayerModelData) this.playersFolder.ReadFile(PlayerModelData.class, ply.getName());
		} catch (Exception var4) {
			data = new PlayerModelData();
			Log.error("Error reading %s's model data file!", ply.getName());
			Log.error(var4);
		}

		this.map.remove(ply.getName());
		this.map.put(ply.getName(), data);
		data.Wearing.removeIf(wearingModel -> {
			Optional<ModelData> modelData = ModelManagerServer.this.GetModelData(wearingModel);
			return !modelData.isPresent() || !ModelManagerServer.this.personalize.isBoughtBy(ply, modelData.get());
		});
		return data;
	}

	public PlayerModelData GetPlayerData(EntityPlayerMP ply) {
		try {
			PlayerModelData data = this.map.get(ply.getName());
			if (data != null) {
				return data;
			}
		} catch (Exception var3) {
			Log.error("Model data for player %s not set! Reloading...", ply.getName());
			Log.error(var3);
		}

		return this.ReloadPlayerData(ply);
	}

	public void UpdatePlayerData(PlayerModelData data, EntityPlayerMP ply) {
		try {
			this.playersFolder.WriteFile(ply.getName(), data);
		} catch (Exception var4) {
			Log.error("Error writing player %s model data to file!", ply.getName());
			Log.error(var4);
		}

	}

	public Optional<ModelData> GetModelData(String ID) {
		for (ModelData model : this.models) {
			if (model.getID().equals(ID)) return Optional.of(model);
		}
		return Optional.empty();
	}

	public Set<ModelData> GetModels() {
		return this.models;
	}
}
