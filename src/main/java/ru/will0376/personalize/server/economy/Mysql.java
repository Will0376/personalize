package ru.will0376.personalize.server.economy;

import net.minecraft.entity.player.EntityPlayer;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.personalize.Personalize;

import java.sql.*;
import java.util.HashMap;

@GradleSideOnly(GradleSide.SERVER)
public class Mysql implements IEco {
	private static final HashMap<String, Float> cache = new HashMap<>();

	public static Connection connect() {
		Connection dbConnection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			dbConnection = DriverManager.getConnection(Personalize.config.getHost(), Personalize.config.getUser(), Personalize.config.getPass());
			return dbConnection;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean setToBD(String player, float count) {
		try {
			connect().prepareStatement(("Insert into %table% (%nickC%, %balanceC%)" +
					" VALUES(\"%player%\", \"%count%\")")
					.replace("%table%", Personalize.config.getTableName())
					.replace("%nickC%", Personalize.config.getNick())
					.replace("%balanceC%", Personalize.config.getBalance())
					.replace("%player%", player)
					.replace("%count%", String.valueOf(count))
			).executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static float getBalanceFromNick(String nick) {
		try {
			PreparedStatement ps = connect().prepareStatement("select * from %table% where %player%=\"%nick%\""
					.replace("%table%", Personalize.config.getTableName())
					.replace("%player%", Personalize.config.getNick())
					.replace("%nick%", nick)
			);
			ResultSet rs = ps.executeQuery();
			float ret = 0;
			while (rs.next()) {
				ret = rs.getFloat(Personalize.config.getBalance());
			}
			rs.close();
			return ret;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public float getBalance(String nick) {
		if (cache.containsKey(nick))
			return cache.get(nick);
		Float put = cache.put(nick, getBalanceFromNick(nick));
		return put;
	}

	@Override
	public float getBalance(EntityPlayer player) {
		return getBalance(player.getName());
	}

	@Override
	public float setBalance(String nick, float input) {
		setToBD(nick, input);
		Float put = cache.put(nick, input);
		return put;
	}

	@Override
	public float setBalance(EntityPlayer player, float input) {
		return setBalance(player.getName(), input);
	}
}
