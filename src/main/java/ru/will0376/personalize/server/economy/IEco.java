package ru.will0376.personalize.server.economy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.util.concurrent.atomic.AtomicReference;

@GradleSideOnly(GradleSide.SERVER)
public interface IEco {
	float getBalance(String nick);

	float getBalance(EntityPlayer player);

	float setBalance(String nick, float input);

	float setBalance(EntityPlayer player, float input);

	default void changeBalance(String playerName, float input) {
		setBalance(playerName, getBalance(playerName) + input);
	}

	default void changeBalance(EntityPlayer player, float input) {
		setBalance(player, getBalance(player) + input);
	}

	default boolean canBuy(String nick, float input) {
		return getBalance(nick) >= input;
	}

	default boolean canBuy(EntityPlayer player, float input) {
		return getBalance(player) >= input;
	}

	default EntityPlayer getPlayerByNick(String nick) {
		AtomicReference<EntityPlayer> player = new AtomicReference<>();
		FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers().forEach(e -> {
			if (e.getName().equals(nick)) player.set(e);
		});
		return player.get();
	}
}
