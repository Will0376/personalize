package ru.will0376.personalize.server.economy;

import net.milkbowl.vault.economy.VaultUtils;
import net.minecraft.entity.player.EntityPlayer;
import org.bukkit.Bukkit;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.personalize.server.Log;

@GradleSideOnly(GradleSide.SERVER)
public class VaultEco implements IEco {
	@Override
	public float getBalance(String nick) {
		return (float) VaultUtils.getEconomy().getBalance(nick);
	}

	@Override
	public float getBalance(EntityPlayer player) {
		return (float) VaultUtils.getEconomy().getBalance(Bukkit.getOfflinePlayer(player.getUniqueID()));
	}

	@Override
	public float setBalance(String nick, float input) {
		Log.error("Do not use Vault#setBalance!");
		return -1;
	}

	@Override
	public float setBalance(EntityPlayer player, float input) {
		Log.error("Do not use Vault#setBalance!");
		return -1;
	}

	@Override
	public void changeBalance(String playerName, float input) {
		if (input < 0)
			input = -input;
		VaultUtils.getEconomy().withdrawPlayer(playerName, input);
	}

	@Override
	public void changeBalance(EntityPlayer player, float input) {
		if (input < 0)
			input = -input;
		VaultUtils.getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(player.getUniqueID()), input);
	}
}
