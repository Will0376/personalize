package ru.will0376.personalize.server.economy;

import net.minecraft.entity.player.EntityPlayer;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.service.economy.EconomyService;
import org.spongepowered.api.service.economy.account.UniqueAccount;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.math.BigDecimal;
import java.util.Optional;

@GradleSideOnly(GradleSide.SERVER)
public class SpongeEco implements IEco {
	EconomyService economyService;

	public SpongeEco() {
		Optional<EconomyService> serviceOpt = Sponge.getServiceManager().provide(EconomyService.class);
		if (!serviceOpt.isPresent()) {
			throw new NullPointerException("Sponge not found!");
		}
		economyService = serviceOpt.get();
	}

	@Override
	public float getBalance(String nick) {
		return getBalance(getPlayerByNick(nick));
	}

	@Override
	public float getBalance(EntityPlayer player) {
		Optional<UniqueAccount> uOpt = economyService.getOrCreateAccount(player.getUniqueID());
		if (uOpt.isPresent()) {
			UniqueAccount acc = uOpt.get();
			BigDecimal balance = acc.getBalance(economyService.getDefaultCurrency());
			return balance.floatValue();
		}
		System.err.println("Player not found");
		return 0;
	}

	@Override
	public float setBalance(String nick, float input) {
		return setBalance(getPlayerByNick(nick), input);
	}

	@Override
	public float setBalance(EntityPlayer player, float input) {
		Optional<UniqueAccount> uOpt = economyService.getOrCreateAccount(player.getUniqueID());
		if (uOpt.isPresent()) {
			UniqueAccount acc = uOpt.get();
			acc.setBalance(economyService.getDefaultCurrency(),
					BigDecimal.valueOf(input),
					Cause.of(EventContext.empty(), "Personalize buy"));
		}
		return getBalance(player);
	}

}
