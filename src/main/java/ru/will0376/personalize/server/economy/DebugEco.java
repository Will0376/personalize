package ru.will0376.personalize.server.economy;

import net.minecraft.entity.player.EntityPlayer;

import java.util.HashMap;

public class DebugEco implements IEco {
	public static HashMap<String, Float> debugmap = new HashMap<>();

	@Override
	public float getBalance(String nick) {
		if (debugmap.containsKey(nick)) return debugmap.get(nick);
		return 99999;
	}

	@Override
	public float getBalance(EntityPlayer player) {
		if (debugmap.containsKey(player.getName())) return debugmap.get(player.getName());
		return 99999;
	}

	@Override
	public float setBalance(String nick, float input) {
		debugmap.put(nick, input);
		return getBalance(nick);
	}

	@Override
	public float setBalance(EntityPlayer player, float input) {
		debugmap.put(player.getName(), input);
		return getBalance(player);
	}

	@Override
	public boolean canBuy(String nick, float input) {
		return true;
	}

	@Override
	public boolean canBuy(EntityPlayer player, float input) {
		return true;
	}
}
