package ru.will0376.personalize.server.economy;

import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.personalize.Personalize;
import ru.will0376.personalize.server.Log;

@GradleSideOnly(GradleSide.SERVER)
public class SetEco {

	public SetEco() {
		IEco iEco;
		try {
			switch (Personalize.config.getEco()) {
				case 0:
					iEco = new VaultEco();
					Log.info("Set Vault Eco");
					break;
				case 1:
					iEco = new Mysql();
					Log.info("Set Mysql Eco");
					break;
				case 2:
					iEco = new SpongeEco();
					Log.info("Set SpongeApi Eco");
					break;
				case 3:
					iEco = new DebugEco();
					Log.info("Set Debug Eco");
					Log.warning("The mod works in debug mode!");
					break;
				default:
					iEco = new VaultEco();
					Log.warning("Set Default: Vault Eco!");
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			iEco = new VaultEco();
		}
		Personalize.eco = iEco;
	}
}
