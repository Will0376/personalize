package ru.will0376.personalize.server;

import net.minecraftforge.common.config.Configuration;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.io.File;

@GradleSideOnly(GradleSide.SERVER)
public class Config {
	private final String DB = "DataBase";
	private final Configuration configuration;
	private int eco;
	private String host;
	private String user;
	private String pass;
	private String tableName;
	private String balance;
	private String nick;

	public Config(File file) {
		this.configuration = new Configuration(file);
	}

	public void launch() {
		load();
		setCfg();
		save();
	}

	private void load() {
		this.configuration.load();
	}

	private void save() {
		this.configuration.save();
	}

	private void setCfg() {
		eco = configuration.getInt("Eco", "General", 0, 0, 3, "0 -> VaultEco(bukkit); 1 -> Mysql(all); 2 -> Sponge; 3 -> Debug");
		host = configuration.getString("Host", DB, "jdbc:mysql://127.0.0.1:3306/test?serverTimezone=UTC", "ip:port/database?serverTimezone=UTC");
		user = configuration.getString("User", DB, "root", "User");
		pass = configuration.getString("Pass", DB, "pass", "Password");
		tableName = configuration.getString("Table", DB, "table", "Name of table");
		balance = configuration.getString("Balance", DB, "Balance", "column of nalance");
		nick = configuration.getString("Nick", DB, "nick", "column of nick");
	}

	public int getEco() {
		return eco;
	}

	public String getHost() {
		return host;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public String getTableName() {
		return tableName;
	}

	public String getBalance() {
		return balance;
	}

	public String getNick() {
		return nick;
	}
}
