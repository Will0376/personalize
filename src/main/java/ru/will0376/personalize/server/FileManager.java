package ru.will0376.personalize.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


@GradleSideOnly(GradleSide.SERVER)
public class FileManager {
	public static String extension = ".json";
	File dataFolder;
	Gson serializer = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();

	public FileManager(File dataDir) {
		if (!dataDir.exists()) {
			dataDir.mkdirs();
		}

		this.dataFolder = dataDir;
	}

	public File getFolder() {
		return this.dataFolder;
	}

	public File getFile(String fileName) {
		return new File(this.dataFolder, fileName + extension);
	}

	public File getFile(String directory, String fileName) {
		File dir = new File(this.dataFolder, directory);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		return new File(dir, fileName + extension);
	}

	public void WriteFile(File file, Object content) throws IOException {
		FileWriter fw = new FileWriter(file);
		this.serializer.toJson(content, fw);
		fw.flush();
		fw.close();
	}

	public void WriteFile(String fileName, Object content) throws IOException {
		this.WriteFile(this.getFile(fileName), content);
	}

	public void WriteFile(String dir, String fileName, Object content) throws IOException {
		this.WriteFile(this.getFile(dir, fileName), content);
	}

	public Object ReadFile(Class clazz, File file) throws Exception {
		if (file.exists()) {
			FileReader fr = new FileReader(file);

			Object var4;
			try {
				var4 = this.serializer.fromJson(fr, clazz);
			} finally {
				fr.close();
			}

			return var4;
		} else {
			return clazz.newInstance();
		}
	}

	public Object ReadFile(Class clazz, String fileName) throws Exception {
		return this.ReadFile(clazz, this.getFile(fileName));
	}

	public Object ReadFile(Class clazz, String dir, String fileName) throws Exception {
		return this.ReadFile(clazz, this.getFile(dir, fileName));
	}

	public Object ReadOrWriteDefault(Class clazz, File file) throws Exception {
		Object instance = this.ReadFile(clazz, file);
		if (!file.exists()) {
			this.WriteFile(file, instance);
		}

		return instance;
	}

	public Object ReadOrWriteDefault(Class clazz, String fileName) throws Exception {
		return this.ReadOrWriteDefault(clazz, this.getFile(fileName));
	}

	public Object ReadOrWriteDefault(Class clazz, String dir, String fileName) throws Exception {
		return this.ReadOrWriteDefault(clazz, this.getFile(dir, fileName));
	}
}
