package ru.will0376.personalize.client;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import ru.will0376.personalize.client.js.JSModelBase;
import ru.will0376.personalize.common.ModelData;
import ru.will0376.personalize.common.PlayerWearingData;

import java.util.*;

import static ru.will0376.personalize.Personalize.debug;

//клиент
public class ModelManager {
	private static PlayerWearingData emptyWearing = PlayerWearingData.createEmpty("");
	private static ModelManager instance;
	Set<String> tryingOn = new HashSet<>();
	private Set<JSModelBase> models = new HashSet<>();
	private Set<String> boughtModels = new HashSet<>();
	private Map<String, PlayerWearingData> playerWearings = new HashMap<>();

	private ModelManager() {
	}

	public static ModelManager get() {
		if (instance == null)
			instance = new ModelManager();
		return instance;
	}

	public void updateModels(Set<ModelData> modelSet) {
		if (debug) System.out.println(modelSet);
		synchronized (this.models) {
			this.models.clear();

			for (ModelData o : modelSet) {
				this.models.add(new JSModelBase(o));
			}
		}
	}

	public void updateModel(ModelData modeldata) {
		if (debug) System.out.println("add model: " + modeldata.getID());

		models.add(new JSModelBase(modeldata));
	}

	public void resetModels() {
		if (debug) System.out.println("Reset models");


		models.clear();
	}

	public Set<? extends JSModelBase> getModels() {
		return this.models;
	}

	public void addWearingData(PlayerWearingData data) {
		this.playerWearings.put(data.PlayerName, data);
	}

	public void resetWearingData() {
		if (debug) System.out.println("Reset wearing data");
		playerWearings.clear();
	}

	public void setBought(Set<String> bought) {
		this.boughtModels = bought;
	}

	public void addBought(String str) {
		boughtModels.add(str);
	}

	public void resetBought() {
		if (debug) System.out.println("Reset bought");

		boughtModels.clear();
	}

	public JSModelBase getByID(String id) {
		synchronized (this.models) {
			Iterator<JSModelBase> var3 = this.models.iterator();

			JSModelBase model;
			do {
				if (!var3.hasNext()) {
					return null;
				}

				model = var3.next();
			} while (!model.ModelData.getID().equals(id));

			return model;
		}
	}

	private PlayerWearingData getWearingData(String playerName) {
		PlayerWearingData data = this.playerWearings.get(playerName);
		return data == null ? emptyWearing : data;
	}

	public boolean isWearingOrTryingOn(Entity ent, String ID) {
		return ent == Minecraft.getMinecraft().player && !this.tryingOn.isEmpty() ? this.tryingOn.contains(ID) : this.isWearing(ent, ID);
	}

	public boolean isWearing(Entity ent, String ID) {
		return ent instanceof EntityPlayer && this.getWearingData(ent.getName()).isWearing(ID);
	}

	public boolean isBought(String ID) {
		return this.boughtModels.contains(ID);
	}

	public void TryOn(final ModelData model) {
		if (model != null) {
			if (this.tryingOn.isEmpty()) {
				this.tryingOn.addAll(getWearingData(Minecraft.getMinecraft().player.getName()).Wearing);
			}

			this.tryingOn.removeIf(modelID -> {
				JSModelBase modelbase = ModelManager.this.getByID(modelID);
				return modelbase != null && modelbase.ModelData.getType() == model.getType();
			});
			this.tryingOn.add(model.getID());
		}

	}

	public void StopTryingOn() {
		this.tryingOn.clear();
	}
}
