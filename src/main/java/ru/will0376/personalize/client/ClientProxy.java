package ru.will0376.personalize.client;

import api.player.model.ModelPlayerAPI;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.personalize.BaseProxy;
import ru.will0376.personalize.Personalize;
import ru.will0376.personalize.client.gui.PersonalizeGui;

//клиент
@GradleSideOnly(GradleSide.CLIENT)
public class ClientProxy extends BaseProxy {
	private PersonalizeGui gui;
	private PlayerTracker tracker;

	public void Init(Personalize mod) {
		ModelPlayerAPI.register("Personalize", RenderPlayerHandler.class);
		gui = new PersonalizeGui();
		MinecraftForge.EVENT_BUS.register(gui);
		FMLCommonHandler.instance().bus().register(gui);
		tracker = new PlayerTracker();
		FMLCommonHandler.instance().bus().register(this.tracker);
	}
}
