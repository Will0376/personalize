package ru.will0376.personalize.client;

import api.player.model.ModelPlayerAPI;
import api.player.model.ModelPlayerBase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.util.NonNullList;
import org.lwjgl.opengl.GL11;
import ru.will0376.personalize.client.js.JSModelBase;

import java.util.ArrayList;
import java.util.Set;

//клиент
public class RenderPlayerHandler extends ModelPlayerBase {
	ModelManager modelManager = ModelManager.get();
	float sneakDiffY = 0.09F;

	public RenderPlayerHandler(ModelPlayerAPI api) {
		super(api);
	}

	public static ArrayList<ItemStack> getCollectionFromIteralbe(Iterable<ItemStack> itr) {
		ArrayList<ItemStack> cltn = new ArrayList<>();
		for (ItemStack t : itr)
			cltn.add(t);
		return cltn;
	}

	public void render(Entity ent, float distance, float speed, float time, float yaw, float pitch, float scale) {
		super.render(ent, distance, speed, time, yaw, pitch, scale);
		if (this.modelPlayer != null) {
			boolean blend = GL11.glIsEnabled(3042);
			if (ent.isInvisible()) {
				return;
			}

			GL11.glPushAttrib(16384);
			GL11.glEnable(3042);
			GL11.glBlendFunc(770, 771);
			Set<? extends JSModelBase> models = this.modelManager.getModels();
			synchronized (models) {

				for (Object o : models) {

					JSModelBase model = (JSModelBase) o;
					if (this.modelManager.isWearingOrTryingOn(ent, model.ModelData.getID())) {
						GL11.glPushMatrix();
						switch (model.ModelData.getType()) {
							case Hat:
								this.PreRenderHat(ent);
								break;
							case Face:
								this.PreRenderFace(ent);
								break;
							case Back:
								this.PreRenderBack(ent);
								break;
							case Pet:
								this.PreRenderPet(ent);
						}

						float playerSpeed = ent.distanceWalkedModified - ent.prevDistanceWalkedModified;
						Minecraft mc = Minecraft.getMinecraft();
						boolean deobf = (boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
						NonNullList<ItemStack> armorInventory = mc.player.inventory.armorInventory;
						model.animate(time, playerSpeed, distance, mc, armorInventory, deobf);
						model.render(ent, distance, speed, time, yaw, pitch, scale);
						GL11.glPopMatrix();
					}
				}
			}

			GL11.glPopAttrib();
		}

	}

	void PreRenderHat(Entity ent) {
		if (this.modelPlayer.isSneak) {
			GL11.glTranslatef(0.0F, this.sneakDiffY, 0.0F);
		}

		this.setRotation(this.modelPlayer.bipedHead.rotateAngleX, this.modelPlayer.bipedHead.rotateAngleY, this.modelPlayer.bipedHead.rotateAngleZ);
		float headTopY;
		// if (((EntityPlayer)ent).getEquipmentAndArmor().getCurrentArmor(3) != null) {
		if (getCollectionFromIteralbe(ent.getArmorInventoryList()).get(3) != null) {
			headTopY = 1.2F;
			GL11.glScalef(1.2F, 1.2F, 1.2F);
		}

		headTopY = 0.5625F;
		GL11.glTranslatef(0.0F, -0.5625F, 0.0F);
	}

	void PreRenderFace(Entity ent) {
		if (this.modelPlayer.isSneak) {
			GL11.glTranslatef(0.0F, this.sneakDiffY, 0.0F);
		}

		float headCenterY;
		// if (((EntityPlayer)ent).getCurrentArmor(3) != null) {
		if (getCollectionFromIteralbe(ent.getArmorInventoryList()).get(3) != null) {
			headCenterY = 0.99F;
			GL11.glScalef(0.99F, 0.99F, 0.99F);
		}

		this.setRotation(this.modelPlayer.bipedHead.rotateAngleX, this.modelPlayer.bipedHead.rotateAngleY, this.modelPlayer.bipedHead.rotateAngleZ);
		headCenterY = 0.25F;
		float headCenterZ = 0.3125F;
		GL11.glTranslatef(0.0F, -0.25F, -0.3125F);
	}

	void PreRenderBack(Entity ent) {
		float backCenterZ;
		if (this.modelPlayer.isSneak) {
			backCenterZ = 0.0F;
			float bodySneakDiffY = 0.0F;
			GL11.glRotatef(28.647888F, 1.0F, 0.0F, 0.0F);
			GL11.glTranslatef(0.0F, 0.0F, -0.0F);
		}

		backCenterZ = 0.125F;
		// if (((EntityPlayer)ent).getArmorInventoryList(2) != null) {
		if (getCollectionFromIteralbe(ent.getArmorInventoryList()).get(2) != null) {
			backCenterZ += 0.0625F;
		}

		GL11.glTranslatef(0.0F, 0.0F, backCenterZ);
	}

	void PreRenderPet(Entity ent) {
		float shoulderDiff;
		float shoulderHeight;
		if (this.modelPlayer.isSneak) {
			shoulderDiff = 0.09375F;
			shoulderHeight = 0.175F;
			GL11.glTranslatef(0.0F, 0.175F, -0.09375F);
		}

		shoulderDiff = -0.4375F;
		shoulderHeight = 1.46875F;
		//  if (((EntityPlayer)ent).getCurrentArmor(2) != null) {
		if (getCollectionFromIteralbe(ent.getArmorInventoryList()).get(2) != null) {
			shoulderHeight += 0.09375F;
		}

		GL11.glTranslatef(-0.4375F, -shoulderHeight, 0.0F);
	}

	protected void setRotation(float x, float y, float z) {
		if (z != 0.0F) {
			GL11.glRotatef(z * 57.295776F, 0.0F, 0.0F, 1.0F);
		}

		if (y != 0.0F) {
			GL11.glRotatef(y * 57.295776F, 0.0F, 1.0F, 0.0F);
		}

		if (x != 0.0F) {
			GL11.glRotatef(x * 57.295776F, 1.0F, 0.0F, 0.0F);
		}

	}

}
