package ru.will0376.personalize.client;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import ru.will0376.personalize.Personalize;
import ru.will0376.personalize.common.network.packets.PacketType;
import ru.will0376.personalize.common.network.packets.ToServer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

//клиент
public class PlayerTracker {
	Minecraft mc = Minecraft.getMinecraft();
	double radius = Math.pow(100.0D, 2.0D);
	Set<EntityPlayer> requested = new HashSet<>();
	long lasttime = 0L;
	long interval = 1000L;

	public void Check() {
		try {
			requested.removeIf(ply -> !PlayerTracker.this.mc.world.playerEntities.contains(ply) || PlayerTracker.this.mc.player.getDistanceSq(ply) > PlayerTracker.this.radius);
			List<EntityPlayer> iter = this.mc.world.playerEntities;

			for (EntityPlayer ply : iter) {
				if (this.mc.player.getDistanceSq(ply) <= this.radius && !this.requested.contains(ply)) {
					this.requested.add(ply);
					this.RequestPlayerData(ply.getName());
				}
			}
		} catch (Exception e) {
			if (Personalize.debug) e.printStackTrace();
		}
	}

	@SubscribeEvent
	public void onClientTick(TickEvent.PlayerTickEvent ev) {
		long now = System.currentTimeMillis();
		if (now - this.lasttime >= this.interval) {
			this.Check();
			this.lasttime = now;
		}

	}

	private void RequestPlayerData(String player) {
		if (Personalize.debug) System.out.println("Requesting " + player);
		Personalize.network.sendToServer(new ToServer(player, PacketType.REQUEST_PLAYER_DATA));
	}
}
