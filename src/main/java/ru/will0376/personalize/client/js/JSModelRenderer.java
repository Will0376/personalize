package ru.will0376.personalize.client.js;

import net.minecraft.client.model.ModelRenderer;

//клиент
public class JSModelRenderer {
	ModelRenderer model = null;
	JSModelBase base;

	public JSModelRenderer(JSModelBase base, int u, int v) {
		this.base = base;
		(this.model = new ModelRenderer(base, u, v)).setTextureSize(base.textureWidth, base.textureHeight);
	}

	public void addBox(float offsetX, float offsetY, float offsetZ, int width, int height, int depth) {
		this.model.addBox(offsetX + this.base.offsetCorrection.x, offsetY + this.base.offsetCorrection.y, offsetZ + this.base.offsetCorrection.z, width, height, depth);
	}

	public void setRotationPoint(float x, float y, float z) {
		this.model.setRotationPoint(x + this.base.posCorrection.x, y + this.base.posCorrection.y, z + this.base.posCorrection.z);
	}

	public void setRotationDegrees(float x, float y, float z) {
		this.model.rotateAngleX = x / 57.295776F;
		this.model.rotateAngleY = y / 57.295776F;
		this.model.rotateAngleZ = z / 57.295776F;
	}

	public void setRotationRadians(float x, float y, float z) {
		this.model.rotateAngleX = x;
		this.model.rotateAngleY = y;
		this.model.rotateAngleZ = z;
	}

	public void setMirror(boolean mirror) {
		this.model.mirror = mirror;
	}

	public void setVisible(boolean visible) {
		this.model.showModel = visible;
	}

	public void render(float scale) {
		this.model.render(scale);
	}

	public ModelRenderer getModelRenderer() {
		return this.model;
	}
}
