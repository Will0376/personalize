package ru.will0376.personalize.client.js;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.FilenameUtils;
import org.lwjgl.util.vector.Vector3f;
import org.mozilla.javascript.*;
import ru.will0376.personalize.client.ModelManager;
import ru.will0376.personalize.common.ModelData;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//клиент
public class JSModelBase extends ModelBase {
	public final ru.will0376.personalize.common.ModelData ModelData;
	public Vector3f posCorrection;
	public Vector3f offsetCorrection;
	Minecraft mc;
	Scriptable scope;
	Function animateFunc;
	ResourceLocation texture;
	List shapes;
	String scriptDir;
	float allScale;

	public JSModelBase(JSModelBase instance) {
		this(instance.ModelData);
	}

	public JSModelBase(ModelData modelData) {
		this.shapes = new ArrayList();
		this.allScale = 1.0F;
		this.posCorrection = new Vector3f();
		this.offsetCorrection = new Vector3f();
		this.ModelData = modelData;
		this.mc = Minecraft.getMinecraft();
		this.scriptDir = FilenameUtils.getFullPath(this.ModelData.getScriptLocation());
		Context context = Context.enter();
		context.setOptimizationLevel(-1);
		this.scope = JavaScriptManager.createScope(context);
		this.setupScope(context);
		this.RunScriptByLocation(context, this.ModelData.getScriptLocation());
		Context.exit();
		Object anim = this.scope.get("animate", this.scope);
		if (anim instanceof Function) {
			this.animateFunc = (Function) anim;
		}

	}

	public boolean isPutOn() {
		return ModelManager.get().isWearing(this.mc.player, this.ModelData.getID());
	}

	public boolean isBought() {
		return ModelManager.get().isBought(this.ModelData.getID());
	}

	private void RunScriptByLocation(Context context, String location) {
		try {
			InputStream is = this.mc.getResourceManager().getResource(new ResourceLocation(location.replace(" ", "_"))).getInputStream();
			Throwable var4 = null;

			try {
				InputStreamReader isr = new InputStreamReader(is);
				Throwable var6 = null;

				try {
					context.evaluateReader(this.scope, isr, location, 1, null);
				} catch (Throwable var31) {
					var6 = var31;
					throw var31;
				} finally {
					if (var6 != null) {
						try {
							isr.close();
						} catch (Throwable var30) {
							var6.addSuppressed(var30);
						}
					} else {
						isr.close();
					}

				}
			} catch (Throwable var33) {
				var4 = var33;
				throw var33;
			} finally {
				if (is != null) {
					if (var4 != null) {
						try {
							is.close();
						} catch (Throwable var29) {
							var4.addSuppressed(var29);
						}
					} else {
						is.close();
					}
				}

			}
		} catch (Exception var35) {
			var35.printStackTrace();
		}

	}

	private void setupScope(Context context) {
		Object modelConstructor = Context.javaToJS(new BaseFunction() {
			public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
				JSModelRenderer model = new JSModelRenderer(JSModelBase.this, ((Double) args[0]).intValue(), ((Double) args[1]).intValue());
				synchronized (JSModelBase.this.shapes) {
					JSModelBase.this.shapes.add(model);
				}

				return Context.javaToJS(model, scope);
			}
		}, this.scope);
		ScriptableObject.putProperty(this.scope, "Model", modelConstructor);
		ScriptableObject.putProperty(this.scope, "base", Context.javaToJS(this, this.scope));
	}

	public void setTexture(int textureWidth, int textureHeight, String texture) {
		super.textureWidth = textureWidth;
		super.textureHeight = textureHeight;
		this.texture = new ResourceLocation(this.scriptDir.replace(" ", "_") + texture.replace(" ", "_"));
	}

	public void setPositionCorrection(float x, float y, float z) {
		this.posCorrection.x = x / this.allScale;
		this.posCorrection.y = y / this.allScale;
		this.posCorrection.z = z / this.allScale;
	}

	public void setOffsetCorrection(float x, float y, float z) {
		this.offsetCorrection.x = x / this.allScale;
		this.offsetCorrection.y = y / this.allScale;
		this.offsetCorrection.z = z / this.allScale;
	}

	public void setScale(float scale) {
		this.allScale = scale;
	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float scale) {
		this.renderShapes(scale);
	}

	public void renderShapes(float scale) {
		if (this.texture != null) {
			this.mc.renderEngine.bindTexture(this.texture);
		}

		synchronized (this.shapes) {
			Iterator var3 = this.shapes.iterator();

			while (var3.hasNext()) {
				JSModelRenderer shape = (JSModelRenderer) var3.next();
				shape.render(scale * this.allScale);
			}
		}
	}

	public void animate(Object... args) {
		if (this.animateFunc != null) {
			Context context = Context.enter();

			try {
				this.animateFunc.call(context, this.scope, this.scope, args);
			} catch (Exception var4) {
				//var4.printStackTrace();
				System.err.println(var4.getMessage());
			}

			Context.exit();
		}

	}

	public List getShapes() {
		return this.shapes;
	}

	public boolean equals(Object obj) {
		return obj != null && obj instanceof JSModelBase && ((JSModelBase) obj).ModelData.equals(this.ModelData);
	}
}
