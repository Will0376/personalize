package ru.will0376.personalize.client.js;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

//клиент
public class JavaScriptManager {
	private static Scriptable globalScope;

	public static Scriptable createScope(Context context) {
		Scriptable obj = context.newObject(getGlobalScope(context));
		obj.setParentScope(getGlobalScope(context));
		return obj;
	}

	public static Scriptable getGlobalScope(Context context) {
		if (globalScope == null) {
			globalScope = context.initStandardObjects();
		}

		return globalScope;
	}
}
