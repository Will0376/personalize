package ru.will0376.personalize.client.js;

//клиент
public class Rainbow {
	public static final int SIZE = 1536;
	public static final double[][] PALETTE = new double[1536][3];

	static {
		int index = 0;

		int blu;
		for (blu = 0; blu < 256; ++blu) {
			PALETTE[index][0] = 1.0D;
			PALETTE[index][1] = (double) blu / 255.0D;
			PALETTE[index][2] = 0.0D;
			++index;
		}

		index = 256;

		for (blu = 0; blu < 256; ++blu) {
			PALETTE[index][0] = 1.0D - (double) blu / 255.0D;
			PALETTE[index][1] = 1.0D;
			PALETTE[index][2] = 0.0D;
			++index;
		}

		index = 512;

		for (blu = 0; blu < 256; ++blu) {
			PALETTE[index][0] = 0.0D;
			PALETTE[index][1] = 1.0D;
			PALETTE[index][2] = (double) blu / 255.0D;
			++index;
		}

		index = 768;

		for (blu = 0; blu < 256; ++blu) {
			PALETTE[index][0] = 0.0D;
			PALETTE[index][1] = 1.0D - (double) blu / 255.0D;
			PALETTE[index][2] = 1.0D;
			++index;
		}

		index = 1024;

		for (blu = 0; blu < 256; ++blu) {
			PALETTE[index][0] = (double) blu / 255.0D;
			PALETTE[index][1] = 0.0D;
			PALETTE[index][2] = 1.0D;
			++index;
		}

		index = 1280;

		for (blu = 0; blu < 256; ++blu) {
			PALETTE[index][0] = 1.0D;
			PALETTE[index][1] = 0.0D;
			PALETTE[index][2] = 1.0D - (double) blu / 255.0D;
			++index;
		}

	}
}
