package ru.will0376.personalize.client.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;
import ru.will0376.personalize.client.Lang;
import ru.will0376.personalize.client.ModelManager;
import ru.will0376.personalize.client.js.JSModelBase;
import ru.will0376.personalize.common.ModelType;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

//клиент
public class GuiModelList extends GuiScrollListFixed {
	List<JSModelBase> localModels = new ArrayList<>();
	int selected = -1;
	float selectionRotation = 0.0F;

	public void onUpdate() {

	}

	public JSModelBase getSelected() {
		return this.selected < 0 ? null : this.localModels.get(this.selected);
	}

	public void setModelType(ModelType type) {
		this.selected = -1;
		this.localModels.clear();
		ModelManager.get().getModels().forEach(model -> {
			if (model.ModelData.getType() == type) {
				this.localModels.add(new JSModelBase(model));
			}
		});
		this.localModels.sort(Comparator.comparingInt(o -> (int) o.ModelData.getPrice()));
	}

	protected void drawBackground() {
		Gui.drawRect(super.left, super.top, super.right, super.bottom, -7237231);
	}

	public int getSelectionColor() {
		return -6750208;
	}

	public int getUnselectedColor() {
		return -3355444;
	}

	protected void drawSlot(int id, int right, int y, int itemHeight, Tessellator tess) {
		JSModelBase model = this.localModels.get(id);
		int srcY = y;
		int imgX = this.left + 2;
		int imgSize = itemHeight - 2;
		if (this.selected != id)
			Gui.drawRect(this.left + 1, y - 1, right - 1, y + itemHeight + 1, this.getUnselectedColor());
		Gui.drawRect(this.left + 2, y + 2 - 2, right - 2, y + itemHeight - 2 + 2, -13421773);
		Gui.drawRect(imgX, y + 2 - 2, imgX + imgSize, y + imgSize + 2, -8947849);
		GL11.glPushMatrix();
		GL11.glPushAttrib(532481);
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 0.95f);
		GL11.glEnable(3042);
		GL11.glDisable(2884);
		GL11.glEnable(3089);
		GL11.glTranslatef((float) (imgX + imgSize / 2), (float) (y + imgSize / 2), 50.0f);
		GL11.glRotatef(25.0f, -1.0f, 0.0f, 0.0f);
		GL11.glRotatef(155.0f, 0.0f, 1.0f, 0.0f);
		if (this.isSelected(id)) {
			float selectionRotation = this.selectionRotation;
			this.selectionRotation = selectionRotation + 1.0f;
			GL11.glRotatef(selectionRotation, 0.0f, 1.0f, 0.0f);
		}
		switch (model.ModelData.getType()) {
			case Back: {
				GL11.glTranslatef(0.0f, -10.0f, 0.0f);
				break;
			}
			case Pet: {
				GL11.glTranslated(0.0, -40.0, 0.0);
				break;
			}
		}
		model.renderShapes(2.3f);
		GL11.glPopAttrib();
		GL11.glPopMatrix();
		FontRenderer fontRenderer = PersonalizeGui.unicodeFont;
		int textLeft = imgX + imgSize + 2;
		++y;
		fontRenderer.drawString(TextFormatting.YELLOW + model.ModelData.getName(), textLeft, y, -1);
		y += fontRenderer.FONT_HEIGHT - 1;
		if (!model.isBought() && model.ModelData.getPrice() >= 0) {
			String priceStr;
			if (model.ModelData.getPrice() == 0) {
				priceStr = TextFormatting.GREEN.toString() + Lang.ModelFree.Text;
			} else {
				priceStr = TextFormatting.RED + Lang.ModelPrice.Text + TextFormatting.WHITE + model.ModelData.getPrice();
			}
			fontRenderer.drawString(priceStr, textLeft, y, -1);
			y += fontRenderer.FONT_HEIGHT - 1;
		} else if (!model.isBought()) {
			String priceStr = TextFormatting.RED.toString() + Lang.ModelUnavailable.Text;
			fontRenderer.drawString(priceStr, textLeft, y, -1);
			y += fontRenderer.FONT_HEIGHT - 1;
		}
		PersonalizeGui.unicodeFont.drawString(model.ModelData.getDescription(), textLeft, y, -1, right - textLeft, -1);
		String[] tags = {Lang.ModelBought.Text, Lang.ModelPutOn.Text};
		boolean[] shown = {model.isBought(), model.isPutOn()};
		int[] tagColors = {-3377289, -8934793};
		for (int i = 0; i < tags.length; ++i) {
			if (shown[i]) {
				int tagX = right - 50 * (i + 1);
				int tagY = srcY + itemHeight - 10;
				Gui.drawRect(tagX, tagY, tagX + 40, tagY + 10, tagColors[i]);
				PersonalizeGui.unicodeFont.drawCenteredString(tags[i], tagX + 20 - 1, tagY + 5 - 1, -1);
			}
		}
	}

	private void cropImage(int x, int y, int w, int h) {
		int bottomCutOff = y + h - super.bottom;
		if (bottomCutOff > 0) {
			h -= bottomCutOff;
		}

		int topCutOff = super.top - y;
		if (topCutOff > 0) {
			y += topCutOff;
		}

		float scaleW = (float) (super.mc.displayWidth / super.screenWidth);
		float scaleH = (float) (super.mc.displayHeight / super.screenHeight);
		int scX = (int) ((float) x * scaleW);
		int scY = (int) ((float) (super.screenHeight - y - h) * scaleH);
		int scW = (int) ((float) w * scaleW);
		int scH = (int) ((float) h * scaleH);
		GL11.glScissor(scX, scY, scW, scH);
	}

	protected void elementClicked(int index, boolean doubleClick) {
		if (this.selected != index) {
			this.selectionRotation = 0.0F;
			this.selected = index;
		}

	}

	protected int getSize() {
		return this.localModels.size();
	}

	protected boolean isSelected(int index) {
		return this.selected == index;
	}
}
