package ru.will0376.personalize.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import java.awt.*;
import java.util.HashMap;

//клиент
public class UnicodeFontRenderer extends FontRenderer {
	private final UnicodeFont font;
	private HashMap<Character, org.newdawn.slick.Color> colors = new HashMap<>();

	public UnicodeFontRenderer(Font awtFont) {
		super(Minecraft.getMinecraft().gameSettings, new ResourceLocation("textures/font/ascii.png"), Minecraft.getMinecraft().getTextureManager(), false);
		(this.font = new UnicodeFont(awtFont)).addAsciiGlyphs();
		this.font.addGlyphs("\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044b\u044c\u044d\u044e\u044f\u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042b\u042c\u042d\u042e\u042f");
		this.font.getEffects().add(new ColorEffect(Color.WHITE));

		try {
			this.font.loadGlyphs();
		} catch (SlickException var3) {
			throw new RuntimeException(var3);
		}

		String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
		super.FONT_HEIGHT = this.font.getHeight("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789") / 2;
		this.colors.put('0', new org.newdawn.slick.Color(0));
		this.colors.put('1', new org.newdawn.slick.Color(170));
		this.colors.put('2', new org.newdawn.slick.Color(43520));
		this.colors.put('3', new org.newdawn.slick.Color(43690));
		this.colors.put('4', new org.newdawn.slick.Color(11141120));
		this.colors.put('5', new org.newdawn.slick.Color(11141290));
		this.colors.put('6', new org.newdawn.slick.Color(16755200));
		this.colors.put('7', new org.newdawn.slick.Color(11184810));
		this.colors.put('8', new org.newdawn.slick.Color(5592405));
		this.colors.put('9', new org.newdawn.slick.Color(5592575));
		this.colors.put('a', new org.newdawn.slick.Color(5635925));
		this.colors.put('b', new org.newdawn.slick.Color(5636095));
		this.colors.put('c', new org.newdawn.slick.Color(16733525));
		this.colors.put('d', new org.newdawn.slick.Color(16733695));
		this.colors.put('e', new org.newdawn.slick.Color(16777045));
		this.colors.put('f', new org.newdawn.slick.Color(16777215));
	}

	public int drawString(String string, int x, int y, int color) {
		return this.drawString(string, x, y, color, 0, 0);
	}

	public int drawString(String string, int x, int y, int color, int lineWidth, int lineSpacing) {
		if (string == null) {
			return 0;
		} else {
			GL11.glPushMatrix();
			GL11.glScaled(0.5D, 0.5D, 0.5D);
			boolean blend = GL11.glIsEnabled(3042);
			boolean lighting = GL11.glIsEnabled(2896);
			boolean texture = GL11.glIsEnabled(3553);
			if (!blend) {
				GL11.glEnable(3042);
			}

			if (lighting) {
				GL11.glDisable(2896);
			}

			if (texture) {
				GL11.glDisable(3553);
			}

			x *= 2;
			y *= 2;
			int newX = x;
			int spaceWidth = this.font.getWidth(" ");
			int fontHeight = this.font.getHeight(string.replace('\n', ' '));
			lineWidth *= 2;
			boolean first = true;
			String delimiter = "\u00a7";
			org.newdawn.slick.Color col = new org.newdawn.slick.Color(color);
			String[] var16 = string.split("\u00a7");
			int var17 = var16.length;

			for (int var18 = 0; var18 < var17; ++var18) {
				String s = var16[var18];
				if (s.length() > 0) {
					if (!first) {
						Character key = s.charAt(0);
						if (this.colors.containsKey(key)) {
							s = s.substring(1);
							col = this.colors.get(key);
						} else {
							s = "\u00a7" + s;
						}
					}

					if (lineWidth <= 0) {
						this.font.drawString((float) newX, (float) y, s, col);
						newX += this.font.getWidth(s);
					} else {
						String[] var29 = s.split("\n");
						int var21 = var29.length;

						for (int var22 = 0; var22 < var21; ++var22) {
							String line = var29[var22];
							String[] var24 = line.split(" ");
							int var25 = var24.length;

							for (int var26 = 0; var26 < var25; ++var26) {
								String word = var24[var26];
								int wordWidth = this.font.getWidth(word);
								if (newX + wordWidth > x + lineWidth) {
									newX = x;
									y += fontHeight + lineSpacing;
								}

								this.font.drawString((float) newX, (float) y, word + " ", col);
								newX += wordWidth + spaceWidth;
							}

							if (newX != x) {
								newX = x;
								y += fontHeight + lineSpacing;
							}
						}
					}
				}

				first = false;
			}

			if (texture) {
				GL11.glEnable(3553);
			}

			if (lighting) {
				GL11.glEnable(2896);
			}

			if (!blend) {
				GL11.glDisable(3042);
			}

			GL11.glPopMatrix();
			return x;
		}
	}

	public int drawCenteredString(String str, int x, int y, int color) {
		return this.drawString(str, x - this.getStringWidth(str) / 2, y - this.getStringHeight(str) / 2, color);
	}

	public int getCharWidth(char c) {
		return this.getStringWidth(Character.toString(c));
	}

	public int getStringWidth(String string) {
		return this.font.getWidth(string) / 2;
	}

	public int getStringHeight(String string) {
		return this.font.getHeight(string) / 2;
	}
}
