package ru.will0376.personalize.client.gui;

//клиент
enum MovermentKey {
	Up,
	Down,
	Left,
	Right,
	Reset,
	ZoomIn,
	ZoomOut
}
