package ru.will0376.personalize.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

//клиент
public abstract class GuiScrollListFixed extends GuiScreen {
	final Minecraft mc = Minecraft.getMinecraft();
	int listWidth;
	int screenWidth;
	int screenHeight;
	int top;
	int bottom;
	int right;
	int left;
	int slotHeight;
	int mouseX;
	int mouseY;
	int selectedIndex = -1;
	private int scrollUpActionId;
	private int scrollDownActionId;
	private float initialMouseClickY = -2.0F;
	private float scrollFactor;
	private float scrollDistance;
	private long lastClickTime = 0L;
	/*public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.mouseX = mouseX;
		this.mouseY = mouseY;
		this.drawBackground();
		boolean isHovering = mouseX >= this.left && mouseX <= this.left + this.listWidth && mouseY >= this.top && mouseY <= this.bottom;
		int listLength = this.getSize();
		int scrollBarRight = this.left + this.listWidth;
		int scrollBarLeft = scrollBarRight - 6;
		int entryLeft = this.left;
		int entryRight = scrollBarLeft - 1;
		int viewHeight = this.bottom - this.top;
		int scroll;
		if (Mouse.isButtonDown(0)) {
			if (this.initialMouseClickY == -1.0F) {
				if (isHovering) {
					scroll = mouseY - this.top + (int) this.scrollDistance - 4;
					int slotIndex = scroll / this.slotHeight;
					if (mouseX >= entryLeft && mouseX <= entryRight && slotIndex >= 0 && scroll >= 0 && slotIndex < listLength) {
						this.elementClicked(slotIndex, slotIndex == this.selectedIndex && System.currentTimeMillis() - this.lastClickTime < 250L);
						this.selectedIndex = slotIndex;
						this.lastClickTime = System.currentTimeMillis();
					}

					if (mouseX >= scrollBarLeft && mouseX <= scrollBarRight) {
						this.scrollFactor = -1.0F;
						int scrollHeight = this.getContentHeight() - viewHeight - 4;
						if (scrollHeight < 1) {
							scrollHeight = 1;
						}

						int var13 = viewHeight * viewHeight / this.getContentHeight();
						if (var13 < 32) {
							var13 = 32;
						}

						if (var13 > viewHeight - 8) {
							var13 = viewHeight - 8;
						}

						this.scrollFactor /= (float) (this.bottom - this.top - var13) / (float) scrollHeight;
					} else {
						this.scrollFactor = 1.0F;
					}

					this.initialMouseClickY = (float) mouseY;
				} else {
					this.initialMouseClickY = -2.0F;
				}
			} else if (this.initialMouseClickY >= 0.0F) {
				this.scrollDistance -= ((float) mouseY - this.initialMouseClickY) * this.scrollFactor;
				this.initialMouseClickY = (float) mouseY;
			}
		} else {
			while (isHovering && Mouse.next()) {
				scroll = Mouse.getEventDWheel();
				if (scroll != 0) {
					if (scroll > 0) {
						scroll = -1;
					} else if (scroll < 0) {
						scroll = 1;
					}

					this.scrollDistance += (float) (scroll * this.slotHeight / 2);
				}
			}

			this.initialMouseClickY = -1.0F;
		}

		this.applyScrollLimits();
		Tessellator tess = Tessellator.getInstance();
		BufferBuilder bb = tess.getBuffer();
		ScaledResolution res = new ScaledResolution(mc);
		double scaleW = (double) this.mc.displayWidth / res.getScaledWidth_double();
		double scaleH = (double) this.mc.displayHeight / res.getScaledHeight_double();
		GL11.glEnable(3089);
		GL11.glScissor((int) ((double) this.left * scaleW), (int) ((double) this.mc.displayHeight - (double) this.bottom * scaleH), (int) ((double) this.listWidth * scaleW), (int) ((double) viewHeight * scaleH));
		int baseY = this.top + 4 - (int) this.scrollDistance;

		int extraHeight;
		int height;
		int barTop;
		try {

			for (extraHeight = 0; extraHeight < listLength; ++extraHeight) {
				height = baseY + extraHeight * this.slotHeight;
				barTop = this.slotHeight - 4;
				if (height <= this.bottom && height + barTop >= this.top) {
					if (this.highlightSelected && this.isSelected(extraHeight)) {
						int color = this.getSelectionColor();
						int a = color >> 24 & 255;
						int r = color >> 16 & 255;
						int g = color >> 8 & 255;
						int b = color >> 0 & 255;
						int min = this.left;
						GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
						GL11.glDisable(3553);
						//tess.startDrawingQuads(); //TODO: проверить
						bb.begin(7, DefaultVertexFormats.ITEM);
						bb.addVertexData(addAll(min, height + barTop + 2, 0.0D, 0.0D, 1.0D));
						bb.addVertexData(addAll(entryRight, height + barTop + 2, 0.0D, 1.0D, 1.0D));
						bb.addVertexData(addAll(entryRight, height - 2, 0.0D, 1.0D, 0.0D));
						bb.addVertexData(addAll(min, height - 2, 0.0D, 0.0D, 0.0D));
						bb.endVertex();
						tess.draw();
						GL11.glEnable(3553);

					}

					this.drawSlot(extraHeight, entryRight, height, barTop, tess);
				}
			}

			GL11.glDisable(2929);
			extraHeight = this.getContentHeight() - viewHeight - 4;
			if (extraHeight > 0) {
				height = viewHeight * viewHeight / this.getContentHeight();
				if (height < 32) {
					height = 32;
				}

				if (height > viewHeight - 8) {
					height = viewHeight - 8;
				}

				barTop = (int) this.scrollDistance * (viewHeight - height) / extraHeight + this.top;
				if (barTop < this.top) {
					barTop = this.top;
				}

				GL11.glDisable(3553);
				GlStateManager.pushMatrix();
				bb.begin(7, DefaultVertexFormats.ITEM);
				bb.putColorRGBA(0, 255, 255, 255);
				bb.addVertexData(addAll(scrollBarLeft, this.bottom, 0.0D, 0.0D, 1.0D));
				bb.addVertexData(addAll(scrollBarRight, this.bottom, 0.0D, 1.0D, 1.0D));
				bb.addVertexData(addAll(scrollBarRight, this.top, 0.0D, 1.0D, 0.0D));
				bb.addVertexData(addAll(scrollBarLeft, this.top, 0.0D, 0.0D, 0.0D));
				bb.endVertex();
				tess.draw();
				GlStateManager.popMatrix();
				GlStateManager.pushMatrix();
				bb.begin(7, DefaultVertexFormats.ITEM);
				bb.putColorRGBA(1, 255, 255, 255);
				bb.addVertexData(addAll(scrollBarLeft, barTop + height, 0.0D, 0.0D, 1.0D));
				bb.addVertexData(addAll(scrollBarRight, barTop + height, 0.0D, 1.0D, 1.0D));
				bb.addVertexData(addAll(scrollBarRight, barTop, 0.0D, 1.0D, 0.0D));
				bb.addVertexData(addAll(scrollBarLeft, barTop, 0.0D, 0.0D, 0.0D));
				bb.endVertex();
				tess.draw();
				GlStateManager.popMatrix();
				GlStateManager.pushMatrix();
				bb.begin(7, DefaultVertexFormats.ITEM);
				bb.putColorRGBA(2, 255, 255, 255);
				bb.addVertexData(addAll(scrollBarLeft, barTop + height - 1, 0.0D, 0.0D, 1.0D));
				bb.addVertexData(addAll(scrollBarRight - 1, barTop + height - 1, 0.0D, 1.0D, 1.0D));
				bb.addVertexData(addAll(scrollBarRight - 1, barTop, 0.0D, 1.0D, 0.0D));
				bb.addVertexData(addAll(scrollBarLeft, barTop, 0.0D, 0.0D, 0.0D));
				bb.endVertex();
				tess.draw();
				GlStateManager.popMatrix();
			}

			GL11.glEnable(3553);
			GL11.glShadeModel(7424);
			GL11.glEnable(3008);
			GL11.glDisable(3042);
			GL11.glDisable(3089);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	public int slotTop;

	public void setPositionAndBounds(int x, int y, int w, int h, int elementHeight, int screenW, int screenH) {
		this.top = y;
		this.left = x;
		this.right = x + w;
		this.bottom = y + h;
		this.listWidth = w;
		this.slotHeight = elementHeight;
		this.screenWidth = screenW;
		this.screenHeight = screenH;
	}

	ArrayList<Slot> slots = new ArrayList<>();

	abstract int getSize();

	abstract void elementClicked(int var1, boolean var2);

	abstract boolean isSelected(int var1);

	abstract void drawBackground();

	abstract void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5);

	public abstract int getSelectionColor();

	int getContentHeight() {
		return this.getSize() * this.slotHeight;
	}

	public void registerScrollButtons(List buttons, int upActionID, int downActionID) {
		this.scrollUpActionId = upActionID;
		this.scrollDownActionId = downActionID;
	}

	private void applyScrollLimits() {
		int listHeight = this.getContentHeight() - (this.bottom - this.top - 4);
		if (listHeight < 0) {
			listHeight /= 2;
		}

		if (this.scrollDistance < 0.0F) {
			this.scrollDistance = 0.0F;
		}

		if (this.scrollDistance > (float) listHeight) {
			this.scrollDistance = (float) listHeight;
		}

	}

	public void actionPerformed(GuiButton button) {
		if (button.enabled) {
			if (button.id == this.scrollUpActionId) {
				this.scrollDistance -= (float) (this.slotHeight * 2 / 3);
				this.initialMouseClickY = -2.0F;
				this.applyScrollLimits();
			} else if (button.id == this.scrollDownActionId) {
				this.scrollDistance += (float) (this.slotHeight * 2 / 3);
				this.initialMouseClickY = -2.0F;
				this.applyScrollLimits();
			}
		}

	}

	public GuiScrollListFixed() {
		slots.clear();
	}

	public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
		this.mouseX = mouseX;
		this.mouseY = mouseY;
		this.drawBackground();
		final boolean isHovering = mouseX >= this.left && mouseX <= this.left + this.listWidth && mouseY >= this.top && mouseY <= this.bottom;
		final int listLength = this.getSize();
		final int scrollBarWidth = 4;
		final int scrollBarRight = this.left + this.listWidth - 1;
		final int scrollBarLeft = scrollBarRight - scrollBarWidth;
		final int entryLeft = this.left;
		final int entryRight = scrollBarLeft - 1;
		final int viewHeight = this.bottom - this.top;
		final int border = 4;
		if (Mouse.isButtonDown(0)) {
			if (this.initialMouseClickY == -1.0f) {
				if (isHovering) {
					final int mouseListY = mouseY - this.top + (int) this.scrollDistance - border;
					final int slotIndex = mouseListY / this.slotHeight;
					if (mouseX >= entryLeft && mouseX <= entryRight && slotIndex >= 0 && mouseListY >= 0 && slotIndex < listLength) {
						this.elementClicked(slotIndex, slotIndex == this.selectedIndex && System.currentTimeMillis() - this.lastClickTime < 250L);
						this.selectedIndex = slotIndex;
						this.lastClickTime = System.currentTimeMillis();
					}
					if (mouseX >= scrollBarLeft && mouseX <= scrollBarRight) {
						this.scrollFactor = -1.0f;
						int scrollHeight = this.getContentHeight() - viewHeight - border;
						if (scrollHeight < 1) {
							scrollHeight = 1;
						}
						int var13 = viewHeight * viewHeight / this.getContentHeight();

						if (var13 > viewHeight - border * 2) {
							var13 = viewHeight - border * 2;
						}

						this.scrollFactor = -1.0f * var13 / 3;
					} else {
						this.scrollFactor = 1.0f;
					}
					this.initialMouseClickY = mouseY;
				} else {
					this.initialMouseClickY = -2.0f;
				}
			} else if (this.initialMouseClickY >= 0.0f) {
				this.scrollDistance -= (mouseY - this.initialMouseClickY) * this.scrollFactor;
				this.initialMouseClickY = mouseY;
			}
		} else {
			while (isHovering && Mouse.next()) {
				int scroll = Mouse.getEventDWheel();
				if (scroll != 0) {
					if (scroll > 0) {
						scroll = -1;
					} else if (scroll < 0) {
						scroll = 1;
					}
					this.scrollDistance += scroll * this.slotHeight;
				}
			}
			this.initialMouseClickY = -1.0f;
		}
		this.applyScrollLimits();
		final Tessellator tess = Tessellator.getInstance();
		final BufferBuilder buffer = tess.getBuffer();
		final ScaledResolution res = new ScaledResolution(this.mc);
		final double scaleW = this.mc.displayWidth / res.getScaledWidth_double();
		final double scaleH = this.mc.displayHeight / res.getScaledHeight_double();
		GL11.glEnable(3089);
		GL11.glScissor((int) (this.left * scaleW), (int) (this.mc.displayHeight - this.bottom * scaleH), (int) (this.listWidth * scaleW), (int) (viewHeight * scaleH));
		final int baseY = this.top + border - (int) this.scrollDistance;
		for (int slotIdx = 0; slotIdx < listLength; ++slotIdx) {
			slotTop = baseY + slotIdx * this.slotHeight;
			final int slotBuffer = this.slotHeight - border;
			if (slotTop <= this.bottom && slotTop + slotBuffer >= this.top) {
				this.drawSlot(slotIdx, entryRight, slotTop, slotBuffer, tess);
				Slot s = new Slot(slotIdx, entryRight, slotTop);
				if (!slots.contains(s)) {
					slots.add(s);
				}
			}
		}
		GlStateManager.disableDepth();
		final int extraHeight = this.getContentHeight() - viewHeight - border;
		if (extraHeight > 0) {
			int height = viewHeight * viewHeight / this.getContentHeight();
			if (height < 32) {
				height = 32;
			}
			if (height > viewHeight - border * 2) {
				height = viewHeight - border * 2;
			}
			int barTop = (int) this.scrollDistance * (viewHeight - height) / extraHeight + this.top + 3;
			if (barTop < this.top) {
				barTop = this.top;
			}
			GlStateManager.disableTexture2D();
			buffer.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
			buffer.pos(scrollBarLeft, barTop + height, 0.0).tex(0.0, 1.0).color(89, 228, 74, 255).endVertex();
			buffer.pos(scrollBarRight, barTop + height, 0.0).tex(1.0, 1.0).color(89, 228, 74, 255).endVertex();
			buffer.pos(scrollBarRight, barTop, 0.0).tex(1.0, 0.0).color(89, 228, 74, 255).endVertex();
			buffer.pos(scrollBarLeft, barTop, 0.0).tex(0.0, 0.0).color(89, 228, 74, 255).endVertex();
			tess.draw();
		}
		GlStateManager.enableTexture2D();
		GlStateManager.shadeModel(7424);
		GlStateManager.enableAlpha();
		GlStateManager.disableBlend();
		GL11.glDisable(3089);
	}

	private int[] addAll(double x, double y, double z, double a, double b) {
		System.out.println("x = " + x + ", y = " + y + ", z = " + z + ", a = " + a + ", b = " + b);
		return new int[]{(int) x, (int) y, (int) z, (int) a, (int) b};
	}
}
