package ru.will0376.personalize.client.gui;

public class Slot {

	private final int posX;
	private final int posY;
	private final int index;

	public Slot(int id, int x, int y) {
		index = id;
		posX = x;
		posY = y;
	}

	public int getId() {
		return index;
	}

	public int getX() {
		return posX;
	}

	public int getY() {
		return posY;
	}

}
