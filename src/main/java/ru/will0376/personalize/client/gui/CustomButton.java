package ru.will0376.personalize.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

//клиент
public class CustomButton extends GuiButton {
	public CustomButton(int i, int j, int k, String s) {
		this(i, j, k, 120, 20, s);
	}

	public CustomButton(int i, int j, int k, int l, int i1, String s) {
		super(i, j, k, l, i1, s);
	}

	public static void drawRect(int left, int top, int right, int bottom, int color) {
		if (left < right) {
			int i = left;
			left = right;
			right = i;
		}

		if (top < bottom) {
			int j = top;
			top = bottom;
			bottom = j;
		}

		float f3 = (float) (color >> 24 & 255) / 255.0F;
		float f = (float) (color >> 16 & 255) / 255.0F;
		float f1 = (float) (color >> 8 & 255) / 255.0F;
		float f2 = (float) (color & 255) / 255.0F;
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		//bufferbuilder.isDrawing = false;
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.color(f, f1, f2, f3);
		try {
			bufferbuilder.begin(7, DefaultVertexFormats.POSITION);
		} catch (Exception ignore) {
		}

		bufferbuilder.pos(left, bottom, 0.0D).endVertex();
		bufferbuilder.pos(right, bottom, 0.0D).endVertex();
		bufferbuilder.pos(right, top, 0.0D).endVertex();
		bufferbuilder.pos(left, top, 0.0D).endVertex();
		tessellator.draw();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
	}

	public int getHoverState(boolean flag) {
		byte byte0 = 1;
		if (!super.enabled) {
			byte0 = 0;
		} else if (flag) {
			byte0 = 2;
		}

		return byte0;
	}

	public void drawButton(Minecraft mc, int mx, int my, float partialTicks) {
		FontRenderer fontrenderer = mc.fontRenderer;
		boolean flag = mx >= super.x && my >= super.y && mx < super.x + super.width && my < super.y + super.height;
		if (flag) {
			this.drawBorderedRect(super.x, super.y, super.x + super.width, super.y + super.height, 1, -1862270977, Integer.MIN_VALUE);
			this.drawCenteredString(fontrenderer, super.displayString, super.x + super.width / 2, super.y + (super.height - 8) / 2, -1);
		} else {
			this.drawBorderedRect(super.x, super.y, super.x + super.width, super.y + super.height, 1, -1862270977, 1610612736);
			this.drawCenteredString(fontrenderer, super.displayString, super.x + super.width / 2, super.y + (super.height - 8) / 2, -3355444);
		}

	}

	public void drawBorderedRect(int x, int y, int x1, int y1, int size, int borderC, int insideC) {
		try {
			drawRect(x + size, y + size, x1 - size, y1 - size, insideC);
			drawRect(x + size, y + size, x1, y, borderC);
			drawRect(x, y, x + size, y1, borderC);
			drawRect(x1, y1, x1 - size, y + size, borderC);
			drawRect(x, y1 - size, x1, y1, borderC);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
