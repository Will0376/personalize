package ru.will0376.personalize.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Pre;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.will0376.personalize.Personalize;
import ru.will0376.personalize.client.Lang;
import ru.will0376.personalize.client.ModelManager;
import ru.will0376.personalize.client.js.JSModelBase;
import ru.will0376.personalize.common.ModelData;
import ru.will0376.personalize.common.ModelType;
import ru.will0376.personalize.common.network.packets.PacketType;
import ru.will0376.personalize.common.network.packets.ToServer;

import java.awt.*;
import java.io.IOException;
import java.util.concurrent.Callable;

//клиент
public class PersonalizeGui extends GuiScreen {
	public static UnicodeFontRenderer unicodeFont;
	int prevView;
	Minecraft mc = Minecraft.getMinecraft();
	float yaw = 0.0F;
	float pitch = 0.0F;
	float zoom = 0.0F;
	boolean guiOpened = false;
	GuiModelList modelList = new GuiModelList();
	GuiButton[] tabs;
	int activeTab = 0;
	float viewHeight = 0.0F;
	GuiButton tryonButton;
	GuiButton buyButton;
	GuiButton putonButton;
	int oldX;
	int oldY;
	boolean pressed = false;
	float tabScaleX = 1.0F;
	float tabScaleY = 1.0F;
	int btnId = 92;

	public PersonalizeGui() {
		buttonList.clear();
		ModelType[] types = ModelType.values();
		this.tabs = new GuiButton[types.length];

		for (int i = 0; i < this.tabs.length; ++i) {
			this.tabs[i] = new CustomButton(0, 0, 0, this.getCategoryName(types[i]));
		}

		if (unicodeFont == null) {
			unicodeFont = new UnicodeFontRenderer(new Font("Arial", Font.PLAIN, 17));
		}

	}

	private void UpdateList() {
		ModelType tab = ModelType.values()[this.activeTab];
		this.modelList.setModelType(tab);
		switch (tab) {
			case Hat:
				this.ShowFront();
				this.viewHeight = 0.6F;
				break;
			case Face:
				this.ShowFront();
				this.viewHeight = 0.3F;
				break;
			case Back:
				this.ShowBack();
				this.viewHeight = -0.2F;
				break;
			case Pet:
				this.ShowFront();
				this.viewHeight = 0.1F;
		}

	}

	public void initGui() {
		super.initGui();
		this.prevView = this.mc.gameSettings.thirdPersonView;
		this.UpdateList();
		super.buttonList.add(this.tryonButton = new CustomButton(0, 0, 0, Lang.MenuTryOn.Text));
		super.buttonList.add(this.buyButton = new CustomButton(0, 0, 0, Lang.MenuBuy.Text));
		super.buttonList.add(this.putonButton = new CustomButton(0, 0, 0, Lang.MenuPutOn.Text));
		EntityPlayerSP player = this.mc.player;
		EntityPlayerSP player2 = this.mc.player;
		EntityPlayerSP player3 = this.mc.player;
		float rotationYaw = this.mc.player.rotationYaw;
		player3.renderYawOffset = rotationYaw;
		player2.rotationYawHead = rotationYaw;
		player.prevRotationYawHead = rotationYaw;
		this.mc.player.rotationPitch = 0.0F;
		this.mc.gameSettings.thirdPersonView = 2;
		this.guiOpened = true;
	}

	void Move(MovermentKey key, float move) {
		switch (key) {
			case Up:
				this.pitch += move;
				break;
			case Down:
				this.pitch -= move;
				break;
			case Right:
				this.yaw -= move;
				break;
			case Left:
				this.yaw += move;
				break;
			case ZoomIn:
				this.zoom += move;
				break;
			case ZoomOut:
				this.zoom -= move;
				break;
			case Reset:
				this.pitch = 0.0F;
				this.yaw = 0.0F;
				this.zoom = 0.0F;
		}

		if (this.pitch > 90.0F) {
			this.pitch = 90.0F;
		} else if (this.pitch < -90.0F) {
			this.pitch = -90.0F;
		}

		if (this.zoom > 1.0F) {
			this.zoom = 1.0F;
		} else if (this.zoom < -1.0F) {
			this.zoom = -1.0F;
		}

	}

	void ShowFront() {
		this.Move(MovermentKey.Reset, 0.0F);
		this.mc.gameSettings.thirdPersonView = 2;
	}

	void ShowBack() {
		this.Move(MovermentKey.Reset, 0.0F);
		this.mc.gameSettings.thirdPersonView = 1;
		this.yaw = 0.0F;
	}

	public void checkKeys() {
		float move = 1.0F;
		if (Keyboard.isKeyDown(17)) {
			this.Move(MovermentKey.Up, 1.0F);
		}

		if (Keyboard.isKeyDown(31)) {
			this.Move(MovermentKey.Down, 1.0F);
		}

		if (Keyboard.isKeyDown(30)) {
			this.Move(MovermentKey.Left, 1.0F);
		}

		if (Keyboard.isKeyDown(32)) {
			this.Move(MovermentKey.Right, 1.0F);
		}

		if (Keyboard.isKeyDown(57)) {
			this.Move(MovermentKey.Reset, 1.0F);
		}

		if (Keyboard.isKeyDown(18)) {
			this.Move(MovermentKey.ZoomIn, 0.03F);
		}

		if (Keyboard.isKeyDown(16)) {
			this.Move(MovermentKey.ZoomOut, 0.03F);
		}

	}

	private void mouseEvent(int x, int y) {
		boolean mouseState = Mouse.isButtonDown(0) || Mouse.isButtonDown(1);
		float wheelDelta;
		if (mouseState != this.pressed) {
			this.pressed = mouseState;
			this.oldX = x;
			this.oldY = y;
		} else if (this.pressed) {
			wheelDelta = (float) (x - this.oldX);
			float deltaY = (float) (y - this.oldY);
			this.Move(MovermentKey.Left, wheelDelta * 1.0F);
			this.Move(MovermentKey.Up, deltaY * 1.0F);
			this.oldX = x;
			this.oldY = y;
		}

		wheelDelta = (float) Mouse.getDWheel() / 120.0F;
		if (wheelDelta != 0.0F) {
			this.Move(MovermentKey.ZoomIn, wheelDelta * 0.1F);
		}

	}

	@SubscribeEvent
	public void onRenderTick(TickEvent.RenderTickEvent event) {
		if (this.guiOpened) {
			this.checkKeys();
			GL11.glTranslatef(0.5F + (1.0F - this.zoom) / 2.0F * 1.0F, 0.25F - this.viewHeight, 2.0F + this.zoom);
			this.mc.player.rotationPitch = this.pitch;
			this.mc.player.rotationYaw = this.yaw;
		}

	}

	public void onGuiClosed() {
		this.mc.gameSettings.thirdPersonView = this.prevView;
		ModelManager.get().StopTryingOn();
		this.guiOpened = false;
	}

	public void onResize(Minecraft mcIn, int p1, int p2) {
		this.onGuiClosed();
		super.setWorldAndResolution(mcIn, p1, p2);
	}

	protected void actionPerformed(GuiButton button) {
		if (button == this.tryonButton) {
			ModelManager.get().TryOn(this.modelList.getSelected().ModelData);
		} else if (button == this.buyButton) {
			ModelManager.get().StopTryingOn();
			ModelData model = this.modelList.getSelected().ModelData;
			this.mc.displayGuiScreen(new GuiYesNo((result, id) -> {
				if (result) {
					//TODO: SEND PacketBuyModel TO SERVER
					Personalize.network.sendToServer(new ToServer(model.getID(), PacketType.BUY_MODEL));
				}

				PersonalizeGui.this.mc.displayGuiScreen(PersonalizeGui.this);
			}, Lang.ModelBuyLine1.Text, String.format(Lang.ModelBuyLine2.Text, TextFormatting.YELLOW + model.getName()
					+ TextFormatting.WHITE, TextFormatting.RED + String.valueOf(model.getPrice()) + TextFormatting.WHITE), 0));
		} else if (button == this.putonButton) {
			ModelManager.get().StopTryingOn();
			//TODO: SEND PacketPutOnModel TO SERVER
			Personalize.network.sendToServer(new ToServer(this.modelList.getSelected().ModelData.getID(), PacketType.PUT_ON_MODEL));
		}

	}

	public void updateScreen() {
		this.modelList.onUpdate();
	}

	private void drawBackground(int x, int y, int w, int h) {
		drawRect(x - 3, y - 3, x + w + 3, y + h + 3, -16777216);
		drawRect(x - 2, y - 2, x + w, y + h, -1);
		drawRect(x, y, x + w + 2, y + h + 2, -11184811);
		drawRect(x, y, x + w, y + h, -3750202);
	}

	private String getCategoryName(ModelType type) {
		switch (type) {
			case Hat:
				return Lang.ModelHat.Text;
			case Face:
				return Lang.ModelFace.Text;
			case Back:
				return Lang.ModelBack.Text;
			case Pet:
				return Lang.ModelPet.Text;
			default:
				return "";
		}
	}

	private void drawCenteredStringNoShadow(FontRenderer fontRenderer, String text, int x, int y, int color) {
		fontRenderer.drawString(text, x - fontRenderer.getStringWidth(text) / 2, y, color, false);
	}

	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		int x = super.width / 18;
		int y = super.height / 14;
		int w = super.width / 2 - x * 2;
		int h = super.height - y * 2;

		if (w < 174) {
			w = 174;
			x = super.width / 4 - 87;
		}

		if (w < 200) {
			this.tabScaleX = 0.9F;
			this.tabScaleY = 1.0F;
		} else {
			this.tabScaleX = 1.0F;
			this.tabScaleY = 1.0F;
		}

		if (mouseX > x && mouseX < x + w && mouseY > y && mouseY < y + h) {
			this.pressed = false;
		} else {
			this.mouseEvent(mouseX, mouseY);
		}

		int hOffset = y + 2;
		int paddedWidth = w - 4;
		int paddedX = x + 2;
		this.drawBackground(x, y, w, h);
		this.drawCenteredStringNoShadow(fontRenderer, Lang.MenuTitle.Text, x + w / 2, hOffset, -3407872);
		hOffset += fontRenderer.FONT_HEIGHT + 2;
		GL11.glPushMatrix();
		GL11.glScalef(this.tabScaleX, this.tabScaleY, 1.0F);
		int tabWidth = (w - 2) / this.tabs.length;

		for (int i = 0; i < this.tabs.length; ++i) {
			GuiButton tabBtn = this.tabs[i];
			tabBtn.x = (int) ((float) (paddedX + tabWidth * i) / this.tabScaleX);
			tabBtn.y = (int) ((float) hOffset / this.tabScaleY);
			tabBtn.width = (int) ((float) tabWidth / this.tabScaleX);
			tabBtn.height = (int) (20.0F / this.tabScaleY);
			tabBtn.enabled = true;
			tabBtn.enabled = i == this.activeTab || tabBtn.mousePressed(this.mc, (int) ((float) mouseX / this.tabScaleX), (int) ((float) mouseY / this.tabScaleY));
			tabBtn.drawButton(this.mc, i == this.activeTab ? tabBtn.x : 0, i == this.activeTab ? tabBtn.y : 0, partialTicks);
		}

		hOffset += 22;
		GL11.glPopMatrix();
		int listHeight = h - hOffset + y - 24;
		this.modelList.setPositionAndBounds(paddedX, hOffset, paddedWidth, listHeight, 50, super.width, super.height);
		this.modelList.drawScreen(mouseX, mouseY, partialTicks);
		hOffset += listHeight + 2;
		GuiButton[] downButtons = new GuiButton[]{this.tryonButton, this.buyButton, this.putonButton};
		int btnWidth = w / downButtons.length;

		GuiButton tryonButton;
		for (int j = 0; j < downButtons.length; ++j) {
			tryonButton = downButtons[j];
			tryonButton.x = paddedX + btnWidth * j;
			tryonButton.y = hOffset;
			tryonButton.width = btnWidth - 1;
			tryonButton.height = 20;
		}

		if (w < 200) {
			tryonButton = this.tryonButton;
			tryonButton.width += 4;
			GuiButton buyButton = this.buyButton;
			buyButton.x += 6;
			buyButton.width -= 14;
			buyButton = this.putonButton;
			buyButton.x -= 6;
			buyButton.width += 2;

		}

		JSModelBase selected = this.modelList.getSelected();
		this.tryonButton.enabled = selected != null && !selected.isBought();
		this.buyButton.enabled = selected != null && selected.ModelData.getPrice() >= 0 && !selected.isBought();
		this.putonButton.enabled = selected != null && selected.isBought();
		this.putonButton.displayString = selected != null && selected.isPutOn() && this.putonButton.enabled ? Lang.MenuTakeOff.Text : Lang.MenuPutOn.Text;
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);
		if (mouseButton == 0) {
			for (int i = 0; i < this.tabs.length; ++i) {
				int finalI = i;
				this.checkPress(this.tabs[i], (int) ((float) mouseX / this.tabScaleX), (int) ((float) mouseY / this.tabScaleY), () -> {
					PersonalizeGui.this.activeTab = finalI;
					PersonalizeGui.this.UpdateList();
					return null;
				});
			}
		}

	}

	private void checkPress(GuiButton btn, int x, int y, Callable action) {
		if (btn.mousePressed(this.mc, x, y)) {
			btn.playPressSound(this.mc.getSoundHandler());

			try {
				action.call();
			} catch (Exception var6) {
				var6.printStackTrace();
			}
		}
	}

	@SubscribeEvent
	public void OnRenderGameOverlay(Pre ev) {
		if (this.guiOpened && ev.getType() != ElementType.ALL) {
			ev.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void keybindadd(InputEvent.KeyInputEvent event) {
		if (Personalize.KeyTest.isPressed())
			Minecraft.getMinecraft().displayGuiScreen(this);
	}
}
