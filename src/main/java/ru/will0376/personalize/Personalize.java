package ru.will0376.personalize;

import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.personalize.common.ModelData;
import ru.will0376.personalize.common.PlayerWearingData;
import ru.will0376.personalize.common.network.packets.PacketType;
import ru.will0376.personalize.common.network.packets.ToClient;
import ru.will0376.personalize.common.network.packets.ToServer;
import ru.will0376.personalize.server.*;
import ru.will0376.personalize.server.economy.IEco;
import ru.will0376.personalize.server.economy.SetEco;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Mod(
		modid = Personalize.MOD_ID,
		name = Personalize.MOD_NAME,
		version = Personalize.VERSION
)
public class Personalize {

	public static final String MOD_ID = "personalize";
	public static final String MOD_NAME = "Personalize";
	public static final String VERSION = "2.3";
	public static boolean debug = true;

	@SidedProxy(
			serverSide = "ru.will0376.personalize.BaseProxy",
			clientSide = "ru.will0376.personalize.client.ClientProxy"
	)
	public static BaseProxy Proxy;
	@Mod.Instance(MOD_ID)
	public static Personalize instance;
	public static SimpleNetworkWrapper network;
	@SideOnly(Side.CLIENT)
	public static KeyBinding KeyTest;
	@GradleSideOnly(GradleSide.SERVER)
	public static IEco eco;
	@GradleSideOnly(GradleSide.SERVER)
	public static Config config;

	@GradleSideOnly(GradleSide.SERVER)
	public ModelManagerServer managerServer;

	@GradleSideOnly(GradleSide.SERVER)
	private FileManager fileManager;

	@GradleSideOnly(GradleSide.SERVER)
	private File folder;

	public static Personalize get() {
		return instance;
	}

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (debug) debug = (boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
		if (event.getSide().isServer()) Invoke.server(() ->
		{
			config = new Config(event.getSuggestedConfigurationFile());
			config.launch();
			new SetEco();
			serverPreInt(event);
		});
	}

	@GradleSideOnly(GradleSide.SERVER)

	private void serverPreInt(FMLPreInitializationEvent event) {
		this.folder = new File(event.getModConfigurationDirectory(), "Personalization");
		if (!this.folder.exists())
			this.folder.mkdir();
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		if (event.getSide().isClient()) Proxy.Init(this);
		network = NetworkRegistry.INSTANCE.newSimpleChannel(MOD_ID);
		network.registerMessage(new ToServer(), ToServer.class, 1, Side.SERVER);
		network.registerMessage(new ToClient(), ToClient.class, 2, Side.CLIENT);

	}

	@Mod.EventHandler
	@GradleSideOnly(GradleSide.CLIENT)
	@SideOnly(Side.CLIENT)
	public void postInit(FMLPostInitializationEvent e) {
		KeyTest = new KeyBinding("Open Gui", Keyboard.KEY_K, "Personalize");
		ClientRegistry.registerKeyBinding(KeyTest);
	}

	@GradleSideOnly(GradleSide.SERVER)
	@Mod.EventHandler
	public void serverInit(FMLInitializationEvent event) {
		if (event.getSide().isServer()) {
			this.fileManager = new FileManager(this.folder);
			this.managerServer = new ModelManagerServer(this, this.fileManager);
			this.managerServer.LoadModels();
			//FMLCommonHandler.instance().bus().register(this);
			MinecraftForge.EVENT_BUS.register(this);
		}
	}


	@GradleSideOnly(GradleSide.SERVER)
	@SubscribeEvent
	public void onPlayerJoined(PlayerEvent.PlayerLoggedInEvent ev) {
		EntityPlayerMP ply = (EntityPlayerMP) ev.player;
		network.sendTo(new ToClient(null, PacketType.PLAYER_RESET_WEARING), ply);
		network.sendTo(new ToClient(null, PacketType.MODEL_RESET), ply);
		network.sendTo(new ToClient(null, PacketType.PLAYER_RESET_BOUGHT), ply);
		this.sendModelList(ply);
		PlayerModelData data = managerServer.ReloadPlayerData(ply);
		this.sendBought(ply);
		this.sendWearings(data, ply);
	}

	@GradleSideOnly(GradleSide.SERVER)
	@Mod.EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new PersonalizationCommand());
	}

	@GradleSideOnly(GradleSide.SERVER)

	public void sendWearings(PlayerModelData wearings, EntityPlayerMP whom) {
		PlayerWearingData data = new PlayerWearingData();
		data.PlayerName = whom.getName();
		data.Wearing = wearings.Wearing;
		network.sendTo(new ToClient(data, PacketType.PLAYER_WEARING), whom);
		List<EntityPlayerMP> playerList = whom.world.getEntitiesWithinAABB(EntityPlayerMP.class,
				new AxisAlignedBB(whom.posX - 100.0D, whom.posY - 100.0D, whom.posZ - 100.0D, whom.posX + 100.0D, whom.posY + 100.0D, whom.posZ + 100.0D));

		for (Entity ent : playerList) {
			if (ent instanceof EntityPlayerMP) {
				network.sendTo(new ToClient(data, PacketType.PLAYER_WEARING), (EntityPlayerMP) ent);
			}
		}

	}

	@GradleSideOnly(GradleSide.SERVER)

	public void sendOtherWearing(EntityPlayerMP requester, EntityPlayerMP player) {
		if (player != null) {
			PlayerWearingData data = new PlayerWearingData();
			data.PlayerName = player.getName();
			data.Wearing = this.managerServer.GetPlayerData(player).Wearing;
			network.sendTo(new ToClient(data, PacketType.PLAYER_WEARING), requester);
		}

	}

	@GradleSideOnly(GradleSide.SERVER)

	public void sendBought(EntityPlayerMP player) {
		this.managerServer.GetModels().forEach(e -> {
			if (this.isBoughtBy(player, e)) {
				network.sendTo(new ToClient(e.getID(), PacketType.PLAYER_BOUGHT), player);
			}
		});
	}

	@GradleSideOnly(GradleSide.SERVER)

	public void sendModelList(EntityPlayerMP player) {
		managerServer.GetModels().forEach(e -> network.sendTo(new ToClient(e, PacketType.MODEL_SET), player));
	}

	@GradleSideOnly(GradleSide.SERVER)

	public void putOnModel(EntityPlayerMP player, String modelID) {
		PlayerModelData playerData = this.managerServer.GetPlayerData(player);
		if (playerData.Wearing.contains(modelID)) {
			playerData.Wearing.remove(modelID);
		} else {
			Optional<ModelData> omodelData = this.managerServer.GetModelData(modelID);
			omodelData.ifPresent(modelData -> {
				if (!this.isBoughtBy(player, modelData)) {
					player.sendMessage(new TextComponentString("Нет доступа!"));
					return;
				}

				playerData.Wearing.removeIf((Predicate<? super String>) wearingModel -> {
					Optional<ModelData> optionalModelData = Personalize.this.managerServer.GetModelData(wearingModel);
					return optionalModelData.filter(data -> data.getType() == modelData.getType()).isPresent();
				});
				playerData.Wearing.add(modelID);
			});

		}

		this.managerServer.UpdatePlayerData(playerData, player);
		this.sendWearings(playerData, player);
	}

	@GradleSideOnly(GradleSide.SERVER)

	public void buyModel(EntityPlayerMP player, String modelID) {
		if (debug) System.out.println(String.format("player %s trying buy model: %s", player.getName(), modelID));
		Optional<ModelData> optionalModelData = this.managerServer.GetModelData(modelID);
		optionalModelData.ifPresent(modelData -> {
			if (!this.isBoughtBy(player, modelData) && modelData.getPrice() >= 0) {
				if (modelData.getPrice() > 0)
					if (!eco.canBuy(player, modelData.getPrice())) {
						player.sendMessage(new TextComponentString(TextFormatting.DARK_RED + "Ошибка покупки: Недостаточно средств!"));
						return;
					} else
						eco.changeBalance(player, -modelData.getPrice());
				PlayerModelData playerData = this.managerServer.GetPlayerData(player);
				playerData.Bought.add(modelID);
				this.managerServer.UpdatePlayerData(playerData, player);
				this.sendBought(player);
				player.sendMessage(new TextComponentString("Спасибо за покупку!"));
			}
		});
	}

	@GradleSideOnly(GradleSide.SERVER)
	public boolean isBoughtBy(EntityPlayerMP player, ModelData model) {
		if (debug) {
			assert managerServer != null;
			assert managerServer.GetPlayerData(player) != null;
			assert model.getID() != null;
		}
		return this.managerServer.GetPlayerData(player).Bought.contains(model.getID()) || this.hasPermission(player, model.getPermission());
	}


	@GradleSideOnly(GradleSide.SERVER)
	private boolean hasPermission(EntityPlayerMP player, String perm) {
		return perm != null && perm.length() > 0 && player.canUseCommand(4, perm);
	}
}
