# Основное
Порт мода Personalize с 1.7.10 на 1.12.2.
## Библиотеки
Библиотеки используются только на стороне клиента. Кладутся в mods.
* RenderPlayerApi(1.0), можно скачать https://gitlab.com/Will0376/personalize/-/blob/master/lib/RenderPlayerAPI_1.12.2_1.0.jar
* javascript(1.7.2), http://www.jabylon.org/maven/org/mozilla/javascript/1.7.2/javascript-1.7.2.jar
* slick2d-core(1.0.2) https://repo1.maven.org/maven2/org/slick2d/slick2d-core/1.0.2/slick2d-core-1.0.2.jar
## Файлы
Конфигурационные файлы находятся в корне репозитория в 7z'е.
**ТОЛЬКО НА СЕРВЕРЕ!** Правильный путь до папки конфига мода - /config/**Personalization**
## Известные баги
* Не работают радужные крылья.(нет метода, и тупо впадлу его искать)